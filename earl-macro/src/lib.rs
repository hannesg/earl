extern crate proc_macro;
use proc_macro::TokenStream;
use earl_codegen::generate;
use syn::parse_macro_input;

#[proc_macro]
pub fn graphql_schema(input: TokenStream) -> TokenStream {
    let lit = parse_macro_input!( input as syn::Lit );
    match lit {
        syn::Lit::Str( schema ) => {
            let s = generate(&schema.value()).unwrap();
//            println!["{}", s];
            return s.parse().unwrap();
        },
        _e => println!["No"]
    };

    /*let trees = input.into_iter().collect::<Vec<_>>();
    if trees.len() == 1 {
        match trees.get(0) {
            Some( TokenTree::Literal(lit) ) => {
                println!["{:?}",lit];
                let s = generate(&format!("{}", lit)).unwrap();
                return s.parse().unwrap();
            },
            _ => {}
        }
    }*/
    panic!["No idea what to do with this input"];
}
