use serde::{Serialize, Serializer};
use std::collections::hash_map::HashMap;
use serde_json::Value;

pub trait Extension {
    fn key() -> &'static str;
    fn encode(self) -> serde_json::Value; 
}

pub struct Extensions {
    inner: HashMap<&'static str, Value>
}

pub struct VariableValue ( pub crate::internal::graphql_parser::query::Value<'static,String> );

fn graphql_to_serde(value: crate::internal::graphql_parser::query::Value<'static,String>) -> serde_json::Value {
    use serde_json::Number;
    match value {
        crate::internal::graphql_parser::query::Value::Boolean(b) => {
            Value::Bool(b)
        },
        crate::internal::graphql_parser::query::Value::Enum(name) => {
            Value::String(name)
        },
        crate::internal::graphql_parser::query::Value::String(name) => {
            Value::String(name)
        },
        crate::internal::graphql_parser::query::Value::Float(f) => {
            Value::Number(Number::from_f64(f).unwrap())
        },
        crate::internal::graphql_parser::query::Value::Int(n) => {
            Value::Number(Number::from(n.as_i64().unwrap()))
        },
        crate::internal::graphql_parser::query::Value::List(list) => {
            Value::Array(list.into_iter().map(graphql_to_serde).collect())
        },
        crate::internal::graphql_parser::query::Value::Object(map) => {
            Value::Object(map.into_iter().map(|(k,v)| (k, graphql_to_serde(v) ) ).collect())
        },
        crate::internal::graphql_parser::query::Value::Null => {
            Value::Null
        },
        crate::internal::graphql_parser::query::Value::Variable(_) => {
            Value::Null
        }
    }
}

impl Extension for VariableValue {
    fn key() -> &'static str {
        "value"
    }
    fn encode(self) -> serde_json::Value {
        graphql_to_serde(self.0)
    }
}

pub struct Problems {
    
}

impl Extension for Problems {
    fn key() -> &'static str {
        "problems"
    }
    fn encode(self) -> serde_json::Value {
        unimplemented![]
    }
}

impl Extensions {
    pub fn new() -> Self {
        Self{ inner: HashMap::new() }
    }
    pub fn is_empty(&self) -> bool {
        self.inner.is_empty()
    }
    pub fn set<E: Extension>(&mut self, v: E) {
        self.inner.insert(E::key(), v.encode());
    }
    pub fn insert_with<E: Extension, F: FnOnce() -> E>(&mut self, f: F) {
        self.inner.entry(E::key()).or_insert_with(|| E::encode(f()) );
    }
}

impl std::fmt::Debug for Extensions {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        let mut map = f.debug_map();
        for (k,v) in self.inner.iter() {
            map.entry(k, v);
        }
        map.finish()
    }
}

impl Serialize for Extensions {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer {
        use serde::ser::SerializeMap;
        let mut map = serializer.serialize_map(None)?;
        for (k,v) in self.inner.iter() {
            map.serialize_entry(k, v)?;
        }
        map.end()
    }
}
