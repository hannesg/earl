use std::sync::Arc;
use std::sync::atomic::AtomicBool;
use std::sync::atomic::Ordering;
use std::cell::UnsafeCell;
use crate::PathElement;

#[derive(Debug,Eq,PartialEq,Clone,Copy,Hash)]
enum PathElementInternal {
    Field{offset: usize},
    Index{index: usize}
}

struct Inner {
    open: AtomicBool,
    content: UnsafeCell<Vec<u8>>,
    forks: UnsafeCell<Vec<(usize, Arc<Inner>)>>,
    errors: UnsafeCell<Vec<crate::Error>>
}

impl Inner {
    fn new() -> Self {
        Inner{
            open: AtomicBool::new(true),
            content: UnsafeCell::new(Vec::with_capacity(1024)),
            forks: UnsafeCell::new(Vec::new()),
            errors: UnsafeCell::new(Vec::new())
        }
    }

    fn content_mut(&self) -> Option<&mut Vec<u8>> {
        if self.open.load(Ordering::Acquire) {
            unsafe{ self.content.get().as_mut() }
        } else {
            None
        }
    }

    fn forks_mut(&self) -> Option<&mut Vec<(usize, Arc<Inner>)>> {
        if self.open.load(Ordering::Acquire) {
            unsafe{ self.forks.get().as_mut() }
        } else {
            None
        }
    }

    fn errors_mut(&self) -> Option<&mut Vec<crate::Error>> {
        if self.open.load(Ordering::Acquire) {
            unsafe{ self.errors.get().as_mut() }
        } else {
            None
        }
    }

    fn is_done(&self) -> bool {
        if self.open.load(Ordering::Acquire) {
            false
        } else {
            let forks = unsafe{ self.forks.get().as_ref() }.unwrap();
            forks.iter().all(|(_,fork)| fork.is_done() )
        }
    }

    fn close(&self) {
        self.open.store(false,Ordering::Release)
    }

    fn len(&self) -> (usize, Option<usize>) {
        if self.open.load(Ordering::Acquire) {
            (0, None)
        } else {
            let forks = unsafe{ self.forks.get().as_ref() }.unwrap();
            let mut min = unsafe{ self.content.get().as_ref() }.unwrap().len();
            let mut max = Some(min);
            for (_,fork) in forks.iter() {
                let (fmin, fmax) = fork.len();
                min += fmin;
                max = match (max, fmax) {
                    (Some(a), Some(b)) => Some(a+b),
                    _ => None
                }
            }
            (min, max)
        }
    }

    fn content(&self, buf: &mut Vec<u8>) -> Result<(),()> {
        if self.open.load(Ordering::Acquire) {
            Err(())
        } else {
            let mut i = 0;
            let content = unsafe{ self.content.get().as_ref() }.unwrap();
            let forks = unsafe{ self.forks.get().as_ref() }.unwrap();
            for (o, fork) in forks.iter() {
                if i != *o {
                    buf.extend_from_slice(&content[i..*o]);
                    i = *o;
                }
                fork.content(buf)?
            }
            buf.extend_from_slice(&content[i..]);
            Ok(())
        }
    }

    fn errors(&self, buf: &mut Vec<crate::Error>) -> Result<(),()> {
        if self.open.load(Ordering::Acquire) {
            Err(())
        } else {
            let mut errs = Vec::new();
            unsafe{ std::mem::swap(&mut errs, self.errors.get().as_mut().unwrap()) }
            buf.extend( errs );
            let forks = unsafe{ self.forks.get().as_ref() }.unwrap();
            for (_, fork) in forks.iter() {
                fork.errors(buf)?
            }
            Ok(())
        }
    }
}

pub struct ForkBuf {
    inner: Arc<Inner>,
    content: &'static mut Vec<u8>,
    path: Vec<PathElementInternal>,
    parent_path: Vec<PathElement>
}

impl ForkBuf {
    pub fn put(&mut self, bytes: &[u8]) {
        self.content.extend(bytes);
    }
    pub fn push(&mut self, bytes: &[u8]) {
        self.content.extend(bytes);
    }

    pub fn start_object(&mut self) {
        self.content.push(b'{');
    }

    pub fn field(&mut self, name: &str) {
        if let Some(c) = self.content.last() {
            if *c != b'{' {
                self.path.pop();
                self.content.push(b',');
            }
        }
        self.path.push(PathElementInternal::Field{offset: self.content.len()});
        self.content.push(b'"');
        self.content.extend(name.as_bytes());
        self.content.push(b'"');
        self.content.push(b':');
    }

    pub fn ensure_field_is_closed(&mut self) {
        if let Some(c) = self.content.last() {
            if *c == b':' {
                self.content.extend(&[b'n',b'u',b'l',b'l'])
            }
        } else {
            self.content.extend(&[b'n',b'u',b'l',b'l'])
        }
    }

    pub fn end_object(&mut self) {
        self.path.pop();
        self.content.push(b'}');
    }

    pub fn start_array(&mut self) {
        self.content.push(b'[');
        self.path.push(PathElementInternal::Index{index: 0})
    }

    pub fn next_element(&mut self) {
        if let Some(PathElementInternal::Index{index}) = self.path.pop() {
            self.content.push(b',');
            self.path.push(PathElementInternal::Index{index: index+1});
        } else {
            panic!();
        }
    }

    pub fn end_array(&mut self) {
        self.path.pop();
        self.content.push(b']');
    }

    pub fn add_error(&mut self, err: crate::Error) {
        let mut path = self.where_am_i();
        path.reverse();
        self.inner.errors_mut().map(|errs| errs.push(err.with_path(path)) );
    }

    pub fn fork(&mut self) -> ForkBuf {
        let arc = Arc::new(Inner::new());
        let content = self.inner.content_mut().unwrap();
        self.inner.forks_mut().unwrap().push((content.len(), arc.clone()));
        let inner_content = unsafe{ std::mem::transmute( arc.content_mut().unwrap() ) };
        return ForkBuf{ inner: arc, content: inner_content, path: Vec::with_capacity(16), parent_path: self.where_am_i() }
    }

    pub fn new() -> Self {
        let inner = Arc::new(Inner::new());
        let inner_content = unsafe{ std::mem::transmute( inner.content_mut().unwrap() ) };
        ForkBuf{ inner, content: inner_content, path: Vec::with_capacity(16), parent_path: Vec::new() }
    }

    pub fn is_done(&mut self) -> bool {
        self.inner.is_done()
    }

    pub fn close(self) -> ForkBufReader {
        ForkBufReader{ inner: self.inner.clone() }
    }

    pub fn len(&mut self) -> (usize, Option<usize>) {
        self.inner.len()
    }

    pub fn where_am_i(&self) -> Vec<PathElement> {
        let mut base = self.parent_path.clone();
        base.extend(self.path.iter().map(|elem|{
            match elem {
                PathElementInternal::Field{offset} => {
                    // Poor mans js string parser
                    let mut i = offset + 1;
                    while i < self.content.len() {
                        if self.content[i] == b'"' {
                            break
                        }
                        i+= 1;
                    }
                    PathElement::Field(std::str::from_utf8(&self.content[offset+1..i]).unwrap().to_string())
                },
                PathElementInternal::Index{index} => {
                    PathElement::Index(*index)
                }
            }
        }));
        base
    }

}

unsafe impl Send for ForkBuf {
}

pub struct ForkBufReader {
    inner: Arc<Inner>
}

unsafe impl Send for ForkBufReader {
}

impl ForkBufReader {

    pub fn is_done(&self) -> bool {
        self.inner.is_done()
    }

    pub fn len(&self) -> (usize, Option<usize>) {
        self.inner.len()
    }

    pub fn linearize(&self) -> Result<Vec<u8>,()>{
        if !self.is_done() {
            return Err(())
        }
        let mut buf = Vec::new();
        self.inner.content(&mut buf).expect("Calling content failed on a finished ForkBufReader");
        return Ok(buf)
    }

    pub fn eat(self) -> (Vec<u8>,Vec<crate::Error>) {
        if !self.is_done() {
            unimplemented![]
        }
        let mut buf = Vec::new();
        let mut err = Vec::new();
        self.inner.content(&mut buf).expect("Calling content failed on a finished ForkBufReader");
        self.inner.errors(&mut err).expect("Calling content failed on a finished ForkBufReader");
        return (buf,err)
    }

}

impl std::ops::Drop for ForkBuf {

    fn drop(&mut self) {
        self.inner.close();
    }

}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn it_works() {
        let mut fb = ForkBuf::new();
        fb.push(&[b'a']);
        let mut spoon = fb.fork();
        fb.push(&[b'd']);
        spoon.push(&[b'b']);
        spoon.push(&[b'c']);
        fb.push(&[b'e']);
        assert_eq!(fb.is_done(), false);
        assert_eq!(spoon.is_done(), false);
        let spoon = spoon.close();
        assert_eq!(spoon.is_done(), true);
        assert_eq!(spoon.len(), (2, Some(2)));
        assert_eq!(fb.is_done(), false);
        assert_eq!(fb.len(), (0, None));
        let fb = fb.close();
        assert_eq!(fb.len(), (5, Some(5)));
        assert_eq!(fb.linearize(), Ok(vec![b'a',b'b',b'c',b'd',b'e']));
    }

    #[test]
    fn it_works_with_double_forks() {
        let mut fb = ForkBuf::new();
        fb.push(&[b'a']);
        {
            let mut spoon1 = fb.fork();
            let mut spoon2 = fb.fork();
            fb.push(&[b'd']);
            spoon1.push(&[b'b']);
            spoon2.push(&[b'c']);
        }
        fb.push(&[b'e']);
        let fb = fb.close();
        assert_eq!(fb.len(), (5, Some(5)));
        assert_eq!(fb.linearize(), Ok(vec![b'a',b'b',b'c',b'd',b'e']));
    }

    #[test]
    fn it_works_with_recursive_forks() {
        let mut fb = ForkBuf::new();
        fb.push(&[b'a']);
        {
            let mut spoon1 = fb.fork();
            spoon1.push(&[b'b']);
            let mut spoon2 = spoon1.fork();
            fb.push(&[b'd']);
            spoon2.push(&[b'c']);
        }
        fb.push(&[b'e']);
        let fb = fb.close();
        assert_eq!(fb.len(), (5, Some(5)));
        assert_eq!(fb.linearize(), Ok(vec![b'a',b'b',b'c',b'd',b'e']));
    }

    #[test]
    fn it_records_the_path() {
        let mut fb = ForkBuf::new();
        fb.start_object();
        assert_eq!(fb.where_am_i(), vec![]);
        fb.field("hello");
        assert_eq!(fb.where_am_i(), vec![PathElement::Field("hello".to_string())]);
        fb.start_array();
        assert_eq!(fb.where_am_i(), vec![
                PathElement::Field("hello".to_string()),
                PathElement::Index(0)
        ]);
        fb.push("1".as_bytes());
        fb.next_element();
        assert_eq!(fb.where_am_i(), vec![
                PathElement::Field("hello".to_string()),
                PathElement::Index(1)
        ]);
        fb.push("2".as_bytes());
        fb.end_array();
        assert_eq!(fb.where_am_i(), vec![PathElement::Field("hello".to_string())]);
        fb.field("world");
        assert_eq!(fb.where_am_i(), vec![PathElement::Field("world".to_string())]);
        fb.start_object();
        assert_eq!(fb.where_am_i(), vec![PathElement::Field("world".to_string())]);
        fb.field("foo");
        assert_eq!(fb.where_am_i(), vec![
                PathElement::Field("world".to_string()),
                PathElement::Field("foo".to_string())
        ]);
        fb.push("null".as_bytes());
        fb.end_object();
        fb.end_object();
        let fb = fb.close();
        assert_eq!(
            fb.linearize().map(|v| String::from_utf8(v).unwrap() ),
            Ok("{\"hello\":[1,2],\"world\":{\"foo\":null}}".to_string())
        );
    }

    #[test]
    fn it_records_the_path_across_forks_1() {
        let mut fb = ForkBuf::new();
        fb.start_object();
        assert_eq!(fb.where_am_i(), vec![]);
        fb.field("hello");
        let mut hello = fb.fork();

        assert_eq!(hello.where_am_i(), vec![PathElement::Field("hello".to_string())]);
        hello.start_array();
        assert_eq!(hello.where_am_i(), vec![
                PathElement::Field("hello".to_string()),
                PathElement::Index(0)
        ]);
        assert_eq!(fb.where_am_i(), vec![PathElement::Field("hello".to_string())]);
        hello.push("1".as_bytes());
        hello.next_element();
        assert_eq!(hello.where_am_i(), vec![
                PathElement::Field("hello".to_string()),
                PathElement::Index(1)
        ]);
        hello.push("2".as_bytes());
        hello.end_array();
        hello.close();

        assert_eq!(fb.where_am_i(), vec![PathElement::Field("hello".to_string())]);

        fb.field("world");
        let mut world = fb.fork();
        assert_eq!(world.where_am_i(), vec![PathElement::Field("world".to_string())]);
        assert_eq!(fb.where_am_i(), vec![PathElement::Field("world".to_string())]);
        world.start_object();
        assert_eq!(world.where_am_i(), vec![PathElement::Field("world".to_string())]);
        world.field("foo");
        assert_eq!(world.where_am_i(), vec![
                PathElement::Field("world".to_string()),
                PathElement::Field("foo".to_string())
        ]);
        world.push("null".as_bytes());
        world.end_object();
        world.close();

        fb.end_object();
        let fb = fb.close();
        assert_eq!(
            fb.linearize().map(|v| String::from_utf8(v).unwrap() ),
            Ok("{\"hello\":[1,2],\"world\":{\"foo\":null}}".to_string())
        );
    }

    #[test]
    fn it_records_the_path_across_forks_2() {
        let mut fb = ForkBuf::new();
        fb.start_object();
        assert_eq!(fb.where_am_i(), vec![]);
        fb.field("hello");
        let mut hello = fb.fork();
        fb.field("world");
        let mut world = fb.fork();
        fb.end_object();

        assert_eq!(world.where_am_i(), vec![PathElement::Field("world".to_string())]);
        world.start_object();
        assert_eq!(world.where_am_i(), vec![PathElement::Field("world".to_string())]);
        world.field("foo");
        assert_eq!(world.where_am_i(), vec![
                PathElement::Field("world".to_string()),
                PathElement::Field("foo".to_string())
        ]);
        world.push("null".as_bytes());
        world.end_object();
        world.close();

        assert_eq!(hello.where_am_i(), vec![PathElement::Field("hello".to_string())]);
        hello.start_array();
        assert_eq!(hello.where_am_i(), vec![
                PathElement::Field("hello".to_string()),
                PathElement::Index(0)
        ]);
        hello.push("1".as_bytes());
        hello.next_element();
        assert_eq!(hello.where_am_i(), vec![
                PathElement::Field("hello".to_string()),
                PathElement::Index(1)
        ]);
        hello.push("2".as_bytes());
        hello.end_array();
        hello.close();

        let fb = fb.close();
        assert_eq!(
            fb.linearize().map(|v| String::from_utf8(v).unwrap() ),
            Ok("{\"hello\":[1,2],\"world\":{\"foo\":null}}".to_string())
        );
    }

    #[test]
    fn it_records_errors() {
        let err = crate::Error::new(crate::ErrorKind::Internal);
        let mut fb = ForkBuf::new();
        fb.add_error(err);
        let (_, errs) = fb.close().eat();
        assert_eq!(errs.len(), 1);
        assert_eq!(errs[0].kind() , crate::ErrorKind::Internal);
    }


    #[test]
    fn it_records_child_errors() {
        let err = crate::Error::new(crate::ErrorKind::Internal);
        let mut fb = ForkBuf::new();
        {
            let mut fb2 = fb.fork();
            fb2.add_error(err);
        }
        let (_, errs) = fb.close().eat();
        assert_eq!(errs.len(), 1);
        assert_eq!(errs[0].kind() , crate::ErrorKind::Internal);
    }
}
