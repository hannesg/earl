use std::collections::hash_map::HashMap;
use hyper;
use serde::Deserialize;
use serde_json::Value;

#[derive(Debug,Deserialize)]
struct ReqBody {
    #[serde(rename = "operationName")]
    operation_name: Option<String>,
    query: String,
    variables: Option<HashMap<String,Value>>
}

/**
 *
 */
pub async fn call<S,C>(c: C, req: hyper::Request<hyper::Body>) -> hyper::Response<hyper::Body> where
    S: crate::Schema,
    S::Query: crate::MakeRoot<C,()>,
    S::Subscription: crate::MakeRoot<C,()>,
    S::Mutation: crate::MakeRoot<C,()>,
    C: Send + Clone + 'static
{
    call2::<S,C>(c,req).await.unwrap_or_else(|e|{
        let content = format!("{{\"errors\":[{}]}}", serde_json::to_string(&e).unwrap());
        hyper::Response::builder().status(200).body(hyper::Body::from(content)).unwrap()
    })
}

pub async fn call2<S,C>(c: C, req: hyper::Request<hyper::Body>) -> Result<hyper::Response<hyper::Body>,crate::Error> where
    S: crate::Schema,
    S::Query: crate::MakeRoot<C,()>,
    S::Subscription: crate::MakeRoot<C,()>,
    S::Mutation: crate::MakeRoot<C,()>,
    C: Send + Clone + 'static
{
    use hyper::body::HttpBody;

    let (_head, mut body) = req.into_parts();
    // Body
    let mut buf : Vec<u8> = vec![];
    'body: loop {
        let body_fn = futures_util::future::poll_fn(|ctx| std::pin::Pin::new(&mut body).poll_data(ctx) );
        match body_fn.await {
            Some(Ok(data)) => buf.extend(data),
            Some(Err(e)) => {
                return Err(crate::Error::new(crate::ErrorKind::Request)
                 .with_source(Box::new(crate::RequestError::BufferIncomplete{
                    source: Box::new(e)
                 })))},
            None => break 'body
        }
    }

    let mut body : ReqBody = serde_json::from_slice(&buf)
        .map_err(|e|
            crate::Error::new(crate::ErrorKind::Request)
                .with_source(Box::new(crate::RequestError::InvalidJson{
                    source: Box::new(e)
                }))
            )?;

    let values = body.variables.take().unwrap_or(HashMap::new()).into_iter()
        .map(|(k,v)|{
           (k,serde_to_graphql(v))
        }).collect::<HashMap<_,_>>();

    let res_body = S::query(&body.query)
        .operation(body.operation_name.as_ref().map(|s| s.as_str()))
        .context(c)
        .values(values)
        .run(&()).await?;
    let mut final_body : Vec<u8> = "{\"data\":".as_bytes().to_vec();
    final_body.extend(res_body.data_as_bytes());
    if !res_body.errors().is_empty() {
        final_body.extend(",\"errors\":".as_bytes());
        final_body.extend(serde_json::to_string(&res_body.errors()).unwrap().as_bytes());
    }
    final_body.extend("}".as_bytes());
    Ok(hyper::Response::builder().status(200).body(hyper::Body::from(final_body)).unwrap())
}

struct OurOwnNumber(i64);

fn serde_to_graphql(value: Value) -> crate::internal::graphql_parser::query::Value<'static,String> {
    match value {
        Value::Array(v) => {
            crate::internal::graphql_parser::query::Value::List(v.into_iter().map(serde_to_graphql).collect())
        },
        Value::Bool(v) => {
            crate::internal::graphql_parser::query::Value::Boolean(v)
        },
        Value::Null => {
            crate::internal::graphql_parser::query::Value::Null
        },
        Value::Number(v) => {
            if let Some(i) = v.as_i64() {
                if i > std::i32::MAX.into() || i < std::i32::MIN.into() {
                    // Cry a little inside
                    let r = unsafe{ std::mem::transmute::<_, crate::internal::graphql_parser::query::Number>(OurOwnNumber(i)) };
                    debug_assert_eq!(r.as_i64(), Some(i));
                    return crate::internal::graphql_parser::query::Value::Int(r)
                } else {
                    return crate::internal::graphql_parser::query::Value::Int(crate::internal::graphql_parser::query::Number::from(i as i32))
                }
            }
            if let Some(f) = v.as_f64() {
                return crate::internal::graphql_parser::query::Value::Float(f)
            }
            unreachable![]
        },
        Value::Object(map) => {
            crate::internal::graphql_parser::query::Value::Object(
                map.into_iter().map(|(k,v)| (k, serde_to_graphql(v)) ).collect()
            )
        },
        Value::String(v) => {
            crate::internal::graphql_parser::query::Value::String(v)
        }
    }
}
