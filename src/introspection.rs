use std::collections::HashMap;
use graphql_parser::{self, schema, Pos};

#[doc(hidden)]
pub mod introspection {
    #![allow(non_snake_case)]
    #![allow(non_camel_case_types)]
    #![allow(dead_code)]
    include!(concat!(env!("OUT_DIR"), "/introspection_schema.rs"));
}

fn is_deprecated(directives: &[graphql_parser::schema::Directive<String>]) -> bool {
    directives.iter().any(|dir| dir.name == "deprecated" )
}

#[derive(Debug,PartialEq)]
pub struct Introspector {
    types: HashMap<String, schema::TypeDefinition<'static,String>>,
    query_type: schema::Type<'static,String>,
    mutation_type: Option<schema::Type<'static,String>>
}

#[derive(Clone,Debug,PartialEq)]
struct TypeRef(String);

impl Introspector {

    pub fn from_source(doc: &str) -> Self {
        let mut types : HashMap<String, schema::TypeDefinition<'static,String>> = HashMap::new();
        types.insert(
            "String".to_string(),
            schema::TypeDefinition::Scalar(
                schema::ScalarType{
                    name: "String".to_string(),
                    description: None,
                    directives: vec![],
                    position: Pos{column: 0, line: 0}
                }
            )
        );
        types.insert(
            "ID".to_string(),
            schema::TypeDefinition::Scalar(
                schema::ScalarType{
                    name: "ID".to_string(),
                    description: None,
                    directives: vec![],
                    position: Pos{column: 0, line: 0}
                }
            )
        );
        types.insert(
            "Boolean".to_string(),
            schema::TypeDefinition::Scalar(
                schema::ScalarType{
                    name: "Boolean".to_string(),
                    description: None,
                    directives: vec![],
                    position: Pos{column: 0, line: 0}
                }
            )
        );
        types.insert(
            "Int".to_string(),
            schema::TypeDefinition::Scalar(
                schema::ScalarType{
                    name: "Int".to_string(),
                    description: None,
                    directives: vec![],
                    position: Pos{column: 0, line: 0}
                }
            )
        );
        types.insert(
            "Float".to_string(),
            schema::TypeDefinition::Scalar(
                schema::ScalarType{
                    name: "Float".to_string(),
                    description: None,
                    directives: vec![],
                    position: Pos{column: 0, line: 0}
                }
            )
        );
        let mut query_type : Option<schema::Type<'static,String>> = None;
        let mut mutation_type : Option<schema::Type<'static,String>> = None;
        let doc = graphql_parser::parse_schema::<'static,String>(unsafe{ std::mem::transmute::<&str, &'static str>(doc) } ).unwrap();
        for def in doc.definitions.iter() {
            match def {
                schema::Definition::SchemaDefinition(sd) => {
                    query_type = sd.query.as_ref().map(|name| schema::Type::NamedType(name.clone()));
                    mutation_type = sd.mutation.as_ref().map(|name| schema::Type::NamedType(name.clone()));
                },
                schema::Definition::TypeDefinition(td) => {
                    match td {
                        schema::TypeDefinition::Object(ot) => {
                            types.insert(ot.name.clone(), td.clone());
                        },
                        schema::TypeDefinition::Enum(ot) => {
                            types.insert(ot.name.clone(), td.clone());
                        },
                        schema::TypeDefinition::InputObject(ot) => {
                            types.insert(ot.name.clone(), td.clone());
                        },
                        schema::TypeDefinition::Scalar(ot) => {
                            types.insert(ot.name.clone(), td.clone());
                        },
                        schema::TypeDefinition::Union(ot) => {
                            types.insert(ot.name.clone(), td.clone());
                        },
                        schema::TypeDefinition::Interface(ot) => {
                            types.insert(ot.name.clone(), td.clone());
                        },
                    }
                }
                _ => {}
            }
        }

        Introspector{
            types: types,
            query_type: query_type.unwrap(),
            mutation_type: mutation_type
        }
    }
}

#[derive(Clone,Debug,PartialEq)]
struct EnumValue {
    name: String,
    description: String,
    is_deprecated: bool,
    deprecation_reason: Option<String>,
}

impl introspection::___enum_value::Instance<&'static Introspector> for EnumValue {

    fn name(&self, responder: crate::string::Responder<&'static Introspector>) -> crate::Ack {
        responder.ok(&self.name)
    }

    fn description(&self, responder: crate::string::OptionalResponder<&'static Introspector>) -> crate::Ack {
        responder.ok(Some(&self.description))
    }

    fn isDeprecated(&self, responder: crate::boolean::Responder<&'static Introspector>) -> crate::Ack {
        responder.ok(self.is_deprecated)
    }

    fn deprecationReason(&self, responder: crate::string::OptionalResponder<&'static Introspector>) -> crate::Ack {
        responder.ok(self.deprecation_reason.as_ref())
    }

}

impl introspection::___input_value::Instance<&'static Introspector> for schema::InputValue<'static,String> {

    fn name(&self, responder: crate::string::Responder<&'static Introspector> ) -> crate::Ack {
        responder.ok(&self.name)
    }

    fn r#type(&self, responder: introspection::___type::Responder<&'static Introspector> ) -> crate::Ack {
        responder.ok(&self.value_type)
    }

    fn description(&self, responder: crate::string::OptionalResponder<&'static Introspector> ) -> crate::Ack {
        responder.ok(self.description.as_ref())
    }

    fn defaultValue(&self, responder: crate::string::OptionalResponder<&'static Introspector> ) -> crate::Ack {
        let val = self.default_value.as_ref().map(|val| val.to_string() );
        responder.ok(val.as_ref())
    }

}

impl introspection::___field::Instance<&'static Introspector> for schema::Field<'static,String> {
    
    fn name(&self, responder: crate::string::Responder<&'static Introspector>) -> crate::Ack {
        responder.ok(&self.name)
    }

    fn description(&self, responder: crate::string::OptionalResponder<&'static Introspector>) -> crate::Ack {
        responder.ok(self.description.as_ref())
    }

    fn args(&self, responder: introspection::___input_value::ListResponder<&'static Introspector> ) -> crate::Ack {
        responder.ok(&self.arguments)
    }

    fn isDeprecated(&self, responder: crate::boolean::Responder<&'static Introspector> ) -> crate::Ack {
        responder.ok(false)
    }

    fn deprecationReason(&self, responder: crate::string::OptionalResponder<&'static Introspector> ) -> crate::Ack {
        responder.none()
    }

    fn r#type(&self, responder: introspection::___type::Responder<&'static Introspector>) -> crate::Ack {
        responder.ok(&self.field_type)
    }
}

impl introspection::___field::Instance<&'static Introspector> for schema::InputValue<'static,String> {
    
    fn name(&self, responder: crate::string::Responder<&'static Introspector>) -> crate::Ack {
        responder.ok(&self.name)
    }

    fn description(&self, responder: crate::string::OptionalResponder<&'static Introspector>) -> crate::Ack {
        responder.ok(self.description.as_ref())
    }

    fn args(&self, responder: introspection::___input_value::ListResponder<&'static Introspector> ) -> crate::Ack {
        responder.empty()
    }

    fn isDeprecated(&self, responder: crate::boolean::Responder<&'static Introspector> ) -> crate::Ack {
        responder.ok(false)
    }

    fn deprecationReason(&self, responder: crate::string::OptionalResponder<&'static Introspector> ) -> crate::Ack {
        responder.none()
    }

    fn r#type(&self, responder: introspection::___type::Responder<&'static Introspector>) -> crate::Ack {
        responder.ok(&self.value_type)
    }
}

impl introspection::___type::Instance<&'static Introspector> for schema::Type<'static,String> {
    fn r#kind(&self,
      responder: introspection::___type_kind::Responder<&'static Introspector> ) -> crate::Ack {
        match self {
            schema::Type::NamedType(name) => {
                responder.context().clone().types.get(name).expect(name).kind(responder)
            },
            schema::Type::ListType(_) =>
                responder.ok(&introspection::__TypeKind::LIST),
            schema::Type::NonNullType(_) => 
                responder.ok(&introspection::__TypeKind::NON_NULL)
        }
    }
    fn r#name(&self,
      responder: crate::string::OptionalResponder<&'static Introspector> ) -> crate::Ack {
        match self {
            schema::Type::NamedType(name) => {
                responder.some(name)
            },
            _ => responder.none()
        }
    }
    fn r#description(&self,
      responder: crate::string::OptionalResponder<&'static Introspector> ) -> crate::Ack {
        responder.none()
    }
    fn r#fields(&self,
      include_deprecated: Option<bool>,
      responder: introspection::___field::OptionalListResponder<&'static Introspector> ) -> crate::Ack {
        match self {
            schema::Type::NamedType(name) => {
                responder.context().clone().types[name].fields(include_deprecated, responder)
            },
            _ => responder.none()
        }
    }
    fn r#interfaces(&self,
      responder: introspection::___type::OptionalListResponder<&'static Introspector> ) -> crate::Ack {
        match self {
            schema::Type::NamedType(name) => {
                responder.context().clone().types.get(name).expect(name).interfaces(responder)
            },
            _ => responder.none()
        }
    }
    fn r#possibleTypes(&self,
      responder: introspection::___type::OptionalListResponder<&'static Introspector> ) -> crate::Ack {
        responder.none()
    }
    fn r#enumValues(&self,
      include_deprecated: Option<bool>,
      responder: introspection::___enum_value::OptionalListResponder<&'static Introspector> ) -> crate::Ack {
        match self {
            schema::Type::NamedType(name) => {
                responder.context().clone().types[name].enumValues(include_deprecated, responder)
            },
            _ => responder.none()
        }
    }
    fn r#inputFields(&self,
      responder: introspection::___input_value::OptionalListResponder<&'static Introspector> ) -> crate::Ack{
        responder.empty()
    }
    fn r#ofType(&self,
      responder: introspection::___type::OptionalResponder<&'static Introspector> ) -> crate::Ack {
        match self {
            schema::Type::ListType(of) => responder.some(&**of),
            schema::Type::NonNullType(of) => responder.some(&**of),
            _ => responder.none()
        }
    }
}

impl introspection::___type::Instance<&'static Introspector> for schema::TypeDefinition<'static,String> {
    fn r#kind(&self,
      responder: introspection::___type_kind::Responder<&'static Introspector> ) -> crate::Ack {
        match self {
            schema::TypeDefinition::Enum(..) =>
                responder.ok(&introspection::__TypeKind::ENUM),
            schema::TypeDefinition::Object(..) =>
                responder.ok(&introspection::__TypeKind::OBJECT),
            schema::TypeDefinition::InputObject(..) =>
                responder.ok(&introspection::__TypeKind::INPUT_OBJECT),
            schema::TypeDefinition::Scalar(..) =>
                responder.ok(&introspection::__TypeKind::SCALAR),
            schema::TypeDefinition::Union(..) =>
                responder.ok(&introspection::__TypeKind::UNION),
            schema::TypeDefinition::Interface(..) =>
                responder.ok(&introspection::__TypeKind::INTERFACE)
        }
    }
    fn r#name(&self,
      responder: crate::string::OptionalResponder<&'static Introspector> ) -> crate::Ack {
        responder.ok(match self {
            schema::TypeDefinition::Enum(e) => Some(&e.name),
            schema::TypeDefinition::Object(e) => Some(&e.name),
            schema::TypeDefinition::InputObject(e) => Some(&e.name),
            schema::TypeDefinition::Interface(e) => Some(&e.name),
            schema::TypeDefinition::Scalar(e) => Some(&e.name),
            schema::TypeDefinition::Union(e) => Some(&e.name)
        })
    }
    fn r#description(&self,
      responder: crate::string::OptionalResponder<&'static Introspector> ) -> crate::Ack {
        responder.ok(match self {
            schema::TypeDefinition::Enum(e) => e.description.as_ref(),
            schema::TypeDefinition::Object(e) => e.description.as_ref(),
            schema::TypeDefinition::InputObject(e) => e.description.as_ref(),
            schema::TypeDefinition::Interface(e) => e.description.as_ref(),
            schema::TypeDefinition::Scalar(e) => e.description.as_ref(),
            schema::TypeDefinition::Union(e) => e.description.as_ref()
        })
    }
    fn r#fields(&self,
      include_deprecated: Option<bool>,
      responder: introspection::___field::OptionalListResponder<&'static Introspector> ) -> crate::Ack {
        match self {
            schema::TypeDefinition::Object(obj) => {
                if include_deprecated.unwrap_or(false) {
                    responder.some(&obj.fields)
                } else {
                    responder.some(&obj.fields.iter().filter(|field| !is_deprecated(&*field.directives)).collect::<Vec<_>>())
                }
            },
            schema::TypeDefinition::Interface(ifs) => {
                if include_deprecated.unwrap_or(false) {
                    responder.some(&ifs.fields)
                } else {
                    responder.some(&ifs.fields.iter().filter(|field| !is_deprecated(&*field.directives)).collect::<Vec<_>>())
                }
            },
            _ => responder.none()
        }
    }
    fn r#interfaces(&self,
      responder: introspection::___type::OptionalListResponder<&'static Introspector> ) -> crate::Ack {
        match self {
            schema::TypeDefinition::Object(ot) => {
                let ctx = responder.context().clone();
                let ifs : Vec<_> = ot.implements_interfaces.iter().map(|named| &ctx.types[named] ).collect();
                responder.ok(Some(&ifs))
            },
            _ => responder.none()
        }
    }
    fn r#possibleTypes(&self,
      responder: introspection::___type::OptionalListResponder<&'static Introspector> ) -> crate::Ack {
        responder.none()
    }
    fn r#enumValues(&self,
      include_deprecated: Option<bool>,
      responder: introspection::___enum_value::OptionalListResponder<&'static Introspector> ) -> crate::Ack {
        match self {
            schema::TypeDefinition::Enum(enom) => {
                if include_deprecated.unwrap_or(false) {
                    responder.some(&enom.values)
                } else {
                    responder.some(&enom.values.iter().filter(|val| !is_deprecated(&*val.directives) ).collect::<Vec<_>>())
                }
            }
            _ => responder.none()
        }
    }
    fn r#inputFields(&self,
      responder: introspection::___input_value::OptionalListResponder<&'static Introspector> ) -> crate::Ack{
        match self {
            schema::TypeDefinition::InputObject(obj) => {
                responder.some(&obj.fields)
            },
            _ => responder.none()
        }
    }
    fn r#ofType(&self,
      responder: introspection::___type::OptionalResponder<&'static Introspector> ) -> crate::Ack {
        responder.none()
    }
}

impl introspection::___enum_value::Instance<&'static Introspector> for schema::EnumValue<'static,String> {
    
    fn r#name(&self,
                responder: crate::string::Responder<&'static Introspector> ) -> crate::Ack {
        responder.ok(&self.name)
    }

    fn r#description(&self,
                responder: crate::string::OptionalResponder<&'static Introspector> ) -> crate::Ack {
        responder.ok(self.description.as_ref())
    }

    fn r#isDeprecated(&self,
                responder: crate::boolean::Responder<&'static Introspector> ) -> crate::Ack {
        responder.ok(false)
    }
    
    fn r#deprecationReason(&self,
                responder: crate::string::OptionalResponder<&'static Introspector> ) -> crate::Ack {
        responder.none()
    }
}

impl introspection::___schema::Instance<&'static Introspector> for &'static Introspector {
    fn types(&self, responder: introspection::___type::ListResponder<&'static Introspector>) -> crate::Ack {
        let types : Vec<_> = self.types.values().collect();
        responder.ok(&types)
        }

    fn queryType(&self, responder: introspection::___type::Responder<&'static Introspector>) -> crate::Ack {
        responder.ok(&self.query_type)
    }

    fn mutationType(&self, responder: introspection::___type::OptionalResponder<&'static Introspector>) -> crate::Ack {
        responder.ok(self.mutation_type.as_ref())
    }

    fn subscriptionType(&self, responder: introspection::___type::OptionalResponder<&'static Introspector>) -> crate::Ack {
        responder.none()
    }

    fn directives(&self, responder: introspection::___directive::ListResponder<&'static Introspector>)
        -> crate::Ack {
        responder.empty()
    }
}

impl introspection::query::Instance<&'static Introspector> for () {
    fn __schema(&self, responder: introspection::___schema::OptionalResponder<&'static Introspector>) -> crate::Ack {
        let ctx : &'static Introspector = *responder.context();
        responder.some(&ctx)
    }
}
