mod fork_buf;
pub mod hyper;
pub mod introspection;
pub mod variables;
pub mod extensions;
#[doc(hidden)]
pub use graphql_parser;
#[doc(hidden)]
pub use futures_util::FutureExt;
use serde::{Serialize, Serializer};
use std::collections::hash_map::HashMap;
use std::marker::PhantomData;
pub use crate::variables::{Variables, ValueProvider};

#[derive(Debug,PartialEq,Eq,Clone,Hash,Serialize)]
pub struct Location {
    pub line: usize,
    pub column: usize
}

#[derive(Debug)]
pub enum QueryError {
    NoQuery,
    NotImplemented,
    UnknownField{
        name: String,
        on_type: &'static str
    },
    MissingArgumentForField{
        name: &'static str,
        field: &'static str
    },
    MissingArgumentForInputObject{
        name: &'static str,
        on_type: &'static str
    },
    IncompatibledArgumentForField{
        name: &'static str,
        field: &'static str,
        expected_type: &'static str
    },
    IncompatibledArgumentForInputObject{
        name: &'static str,
        on_type: &'static str,
        expected_type: &'static str
    },
    IncompatibleVariableDefinition{
        variable: String,
        defined_type: String,
        expected_type: String
    },
    InvalidVariableValue{
        variable: String,
        defined_type: String
    },
    UnknownVariable{
        variable: String
    },
    InvalidVariableType{
        variable: String,
        defined_type: String
    },
    InvalidArgument{
        message: String
    }
}

impl std::fmt::Display for QueryError {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        match self {
            QueryError::NoQuery => write![f, "No query found"],
            QueryError::NotImplemented => write![f, "Not implemented"],
            QueryError::UnknownField{ name, on_type } => write![f, "Field '{}' doesn't exist on type '{}'.", name, on_type],
            QueryError::MissingArgumentForInputObject{ name, on_type } =>
                write![f, "Argument '{}' on InputObject '{}' is required.", name, on_type],
            QueryError::MissingArgumentForField{ name, field } =>
                write![f, "Argument '{}' on Field '{}' is required.", name, field],
            QueryError::IncompatibledArgumentForField{ name, field, expected_type } =>
                write![f, "Argument '{}' on Field '{}' has an invalid value. Expected type '{}'.", name, field, expected_type ],
            QueryError::IncompatibledArgumentForInputObject{ name, on_type, expected_type } =>
                write![f, "Argument '{}' on InputObject '{}' has an invalid value. Expected type '{}'.", name, on_type, expected_type ],
            QueryError::IncompatibleVariableDefinition{ variable, defined_type, expected_type } => {
                write![f, "Type mismatch on variable ${} and argument key ({} / {})", variable, defined_type, expected_type ]
            },
            QueryError::InvalidVariableValue{ variable, defined_type } => {
                write![f, "Variable {} of type {} was provided invalid value", variable, defined_type ]
            },
            QueryError::UnknownVariable{ variable } => {
                write![f, "Unknown variable {}", variable ]
            },
            QueryError::InvalidVariableType{ variable, defined_type } => {
                write![f, "{} isn't a defined input type (on ${})", defined_type, variable ]
            },
            QueryError::InvalidArgument{ message } => {
                write![f, "Argument is invalid: {}", message ]
            }
        }
    }
}

impl std::error::Error for QueryError {
    
}

#[derive(Debug)]
pub enum RequestError {
    BufferIncomplete{
        source: Box<dyn std::error::Error + Send + Sync + 'static>
    },
    InvalidEncoding{
        source: Box<dyn std::error::Error + Send + Sync + 'static>
    },
    InvalidJson{
        source: Box<dyn std::error::Error + Send + Sync + 'static>
    }
}

impl std::fmt::Display for RequestError {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        match self {
            RequestError::BufferIncomplete{ source } => write![f, "Buffer incomplete `{}`", source],
            RequestError::InvalidEncoding{ source } => write![f,  "Invalide encoding `{}`", source],
            RequestError::InvalidJson{ source } => write![f, "Invalide json `{}`", source]
        }
    }
}

impl std::error::Error for RequestError {
    
}

#[derive(Debug,Eq,PartialEq,Clone,Hash)]
pub enum PathElement {
    Field(String),
    Index(usize)
}

impl Serialize for PathElement {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer {
        match self {
            PathElement::Field(s) => serializer.serialize_str(&*s),
            PathElement::Index(u) => serializer.serialize_u64(*u as u64)
        }
    }
}


#[derive(Debug,PartialEq,Eq,Clone,Hash,Serialize)]
pub enum ExecutionError {
    Generic(String)
}

impl std::fmt::Display for ExecutionError {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        match self {
            ExecutionError::Generic(s) => f.write_str(s)
        }
    }
}

impl std::error::Error for ExecutionError {

}


#[derive(Debug,PartialEq,Eq,Copy,Clone,Hash,Serialize)]
pub enum ErrorKind {
    Request,
    Parse,
    Query,
    Execution,
    Internal
}

#[derive(Debug)]
pub struct Error {
    kind: ErrorKind,
    fatal: bool,
    locations: Vec<Location>,
    path: Vec<PathElement>,
    source: Option<Box<dyn std::error::Error + Send + Sync + 'static>>,
    extensions: extensions::Extensions
}

impl Error {
    pub fn new(kind: ErrorKind) -> Self {
        Error {
            kind,
            fatal: false,
            locations: Vec::new(),
            path: Vec::new(),
            source: None,
            extensions: extensions::Extensions::new()
        }
    }

    pub fn invalid_argument(s: String) -> Self {
        Self::new(ErrorKind::Query).with_source(Box::new(QueryError::InvalidArgument{message: s}))
    }

    pub fn with_message(s: &str) -> Self {
        Self::new(ErrorKind::Execution).with_source(Box::new(ExecutionError::Generic(s.to_string())))
    }

    pub fn with_source(self, source: Box<dyn std::error::Error + Send + Sync + 'static>) -> Self {
        Self{
            source: Some(source), ..self
        }
    }

    pub fn or_with_source<F: FnOnce() -> Box<dyn std::error::Error + Send + Sync + 'static>>(mut self, f: F) -> Self {
        self.source.get_or_insert_with(f);
        self
    }
/*
    pub fn map_source<T: Any + dyn std::error::Error + Send + Sync + 'static>(self, f: FnOnce(T) -> T) -> Self {
        let source = self.source.take();
        self.source = source.map(f);
        self
    }
*/
    pub fn with_pos(mut self, pos: &graphql_parser::Pos) -> Self {
        self.locations.push(Location{ line: pos.line, column: pos.column });
        self
    }

    pub fn with_path(mut self, mut path: Vec<PathElement>) -> Self {
        self.path.append(&mut path);
        self
    }

    pub fn at_field<T: Into<String>>(mut self, s: T) -> Self {
        self.path.push(PathElement::Field(s.into()));
        self
    }

    pub fn at_index(mut self, i: usize) -> Self {
        self.path.push(PathElement::Index(i));
        self
    }

    pub fn kind(&self) -> ErrorKind {
        self.kind.clone()
    }

    pub fn with_fatal(mut self) -> Self {
        self.fatal = true;
        self
    }

    pub fn is_fatal(&self) -> bool {
        self.fatal
    }

    pub fn with_extension<E: extensions::Extension>(mut self, e: E) -> Self {
        self.extensions.set(e);
        self
    }

    pub fn or_with_extension<E: extensions::Extension, F: FnOnce() -> E>(mut self,f: F) -> Self {
        self.extensions.insert_with(f);
        self
    }
}

impl std::fmt::Display for Error {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        if let Some(s) = &self.source {
            std::fmt::Display::fmt(s, f)
            } else {
            write![f, "error"]
        }
    }
}

impl<T> From<T> for Error where T : std::error::Error + Send + Sync + 'static {
    fn from(err: T) -> Self {
        crate::Error::new(crate::ErrorKind::Execution).with_source(Box::new(err))
    }
}

struct Compat<'a> {
    error: &'a Error
}

impl<'a> std::fmt::Debug for Compat<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&*self.error, f)
    }
}
impl<'a> std::fmt::Display for Compat<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Display::fmt(&*self.error, f)
    }
}

impl<'a> std::error::Error for Compat<'a> {
    fn source(&self) -> Option<&(dyn std::error::Error + 'static)> {
        self.error.source.as_ref().map(|source| &**source as &(dyn std::error::Error + 'static) )
    }
}

impl Serialize for Error {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer {
        use serde::ser::SerializeMap;
        let mut map = serializer.serialize_map(None)?;
        map.serialize_entry("message", &self.to_string())?;
        if !self.locations.is_empty() {
            map.serialize_entry("locations", &self.locations)?;
        }
        if !self.path.is_empty() {
            map.serialize_entry("path", &self.path.iter().rev().collect::<Vec<_>>())?;
        }
        if !self.extensions.is_empty() {
            map.serialize_entry("extensions", &self.extensions)?;
        }
        map.end()
    }
}

pub trait FromSelectionSet : Sized {

    fn from_selection_set(
        set: &internal::graphql_parser::query::SelectionSet<'static,String>, doc: &internal::graphql_parser::query::Document<'static,String>, variables: &Variables) -> Result<Self, Error>;

}

pub trait MakeRoot<Ctx,T> {
    fn make_root(self, ctx: Ctx, t: &T) -> internal::Root;
}

impl<Ctx,T> MakeRoot<Ctx,T> for NotImplemented {
    fn make_root(self, _ctx: Ctx, _t: &T) -> internal::Root {
        unreachable![]
    }
}

static INTROSPECTION_CACHE_ONCE: std::sync::Once = std::sync::Once::new();
// TOOO: switch to MaybeUninit when all methods are stable.
static mut INTROSPECTION_CACHE: Option<
    std::sync::Mutex<std::collections::HashMap<*const str, crate::introspection::Introspector>>
> = None;

pub trait Schema: 'static + Send + Sync {
    type Query : FromSelectionSet + Send;
    type Mutation : FromSelectionSet + Send;
    type Subscription : FromSelectionSet + Send;

    fn source() -> &'static str {
        unimplemented![];
    }

    fn introspection() -> &'static crate::introspection::Introspector {
        INTROSPECTION_CACHE_ONCE.call_once(||{
            unsafe{
                INTROSPECTION_CACHE = Some(std::sync::Mutex::new(HashMap::with_capacity(1)));
            }
        });
        let mut guard = unsafe{ INTROSPECTION_CACHE.as_ref() }.unwrap().lock().unwrap() ;
        let s = Self::source();
        let v = guard.entry(s as *const _).or_insert_with(||{
            crate::introspection::Introspector::from_source(s)
        });
        unsafe{ std::mem::transmute(v) }
    }

    fn operation(qry: &graphql_parser::query::Document<'static,String>,raw: RawOperation, values: &mut dyn ValueProvider) -> Result<Operation<Self::Query,Self::Mutation,Self::Subscription>,crate::Error> {
        match raw {
            RawOperation::Query(ss, var_defs) => Self::Query::from_selection_set(ss, qry, &Variables::new::<Self>(var_defs, values)?).map(Operation::Query),
            RawOperation::Mutation(ss, var_defs) => Self::Mutation::from_selection_set(ss, qry, &Variables::new::<Self>(var_defs, values)? ).map(Operation::Mutation),
            RawOperation::Subscription(ss, var_defs) => Self::Subscription::from_selection_set(ss, qry, &Variables::new::<Self>(var_defs, values)?).map(Operation::Subscription)
        }
    }

    fn input_type(typ: &internal::graphql_parser::query::Type<'static,String>) -> Option<&'static dyn Fn(&internal::graphql_parser::query::Value<'static,String>) -> Result<(),Error>>;

    fn query(query: &str) -> Builder<Self,variables::NoValues,()> {
        Builder{
            query: graphql_parser::parse_query::<'static,String>(unsafe{ std::mem::transmute::<&str, &'static str>(query) }).map_err(|err|{
                use failure::Fail;
                crate::Error::new(crate::ErrorKind::Query)
                    .with_source(Box::new(err.compat()))
            }),
            values: variables::NoValues(),
            operation_name: None,
            context: (),
            _phantom: PhantomData
        }
    }
}

pub struct Builder<'opname, S: Schema + ?Sized, V: ValueProvider, Ctx> {
    query: Result<graphql_parser::query::Document<'static,String>, Error>,
    values: V,
    operation_name: Option<&'opname str>,
    context: Ctx,
    _phantom: PhantomData<S>
}

impl<'query, S, V, Ctx> Builder<'query, S, V, Ctx>
    where 
        S: Schema + ?Sized,
        V: ValueProvider
{
    pub fn run<T>(self, t: &T) -> internal::Root
        where
        S::Query: MakeRoot<Ctx,T>,
        S::Subscription: MakeRoot<Ctx,T>,
        S::Mutation: MakeRoot<Ctx,T> {
        self.run2(t).unwrap_or_else(|e| internal::Root::failed(e) )
    }

    pub fn run2<T>(self, t: &T) -> Result<internal::Root,Error>
        where
        S::Query: MakeRoot<Ctx,T>,
        S::Subscription: MakeRoot<Ctx,T>,
        S::Mutation: MakeRoot<Ctx,T> 
    {
        let Builder{ query, mut values, operation_name, context, .. } = self;
        let qry = query?;
        let raw = crate::RawOperation::get(&qry, operation_name)?
            .ok_or_else(|| crate::Error::new(crate::ErrorKind::Query).with_source(Box::new(crate::QueryError::NoQuery) ) )?;

        if raw.is_introspection() {
            Ok(crate::introspection::introspection::Schema::operation(&qry, raw, &mut values)?.make_root(S::introspection(),&()))
        } else {
            Ok(S::operation(&qry, raw, &mut values)?.make_root(context,t))
        }
    }

    pub fn operation<'opname2>(self, operation_name: Option<&'opname2 str>) -> Builder<'opname2,S,V,Ctx> {
        let Builder{ query, values, context, .. } = self;
        Builder{
            query,
            values,
            context,
            operation_name,
            _phantom: PhantomData
        }
    }

    pub fn context<Ctx2>(self, context: Ctx2) -> Builder<'query,S,V,Ctx2> {
        let Builder{ query, operation_name, values, .. } = self;
        Builder{
            query,
            operation_name,
            context,
            values,
            _phantom: PhantomData
        }
    }

    pub fn values<V2: ValueProvider>(self, values: V2) -> Builder<'query,S,V2,Ctx> {
        let Builder{ query, operation_name, context, .. } = self;
        Builder{
            query,
            operation_name,
            context,
            values,
            _phantom: PhantomData
        }
    }
}

pub enum RawOperation<'a>{
    Query(&'a graphql_parser::query::SelectionSet<'static,String>, &'a [graphql_parser::query::VariableDefinition<'static,String>]),
    Mutation(&'a graphql_parser::query::SelectionSet<'static,String>, &'a [graphql_parser::query::VariableDefinition<'static,String>]),
    Subscription(&'a graphql_parser::query::SelectionSet<'static,String>, &'a [graphql_parser::query::VariableDefinition<'static,String>])
}

static EMPTY_VAR_DEF: [graphql_parser::query::VariableDefinition<'static,String>; 0] = [];

impl<'a> RawOperation<'a> {
    fn get(qry: &'a graphql_parser::query::Document<'static,String>, operation_name: Option<&str>) -> Result<Option<Self>,crate::Error> {
        if let Some(op_name) = operation_name {
            for def in qry.definitions.iter() {
                match def {
                    graphql_parser::query::Definition::Operation(op) => {
                        match op {
                            graphql_parser::query::OperationDefinition::Query(q) => {
                                if q.name.as_ref().map(|name| name == op_name).unwrap_or(false) {
                                    return Ok(Some(RawOperation::Query(&q.selection_set, &*q.variable_definitions)))
                                }
                            },
                            graphql_parser::query::OperationDefinition::Mutation(q) => {
                                if q.name.as_ref().map(|name| name == op_name).unwrap_or(false) {
                                    return Ok(Some(RawOperation::Mutation(&q.selection_set, &*q.variable_definitions)))
                                }
                            },
                            graphql_parser::query::OperationDefinition::SelectionSet(_) => {},
                            _ => {}
                        }
                    },
                    _ => {}
                }
            }
            return Ok(None)
        } else {
            for def in qry.definitions.iter() {
                match def {
                    graphql_parser::query::Definition::Operation(op) => {
                        match op {
                            graphql_parser::query::OperationDefinition::SelectionSet(ss) => {
                                return Ok(Some(RawOperation::Query(&ss, &EMPTY_VAR_DEF)))
                            },
                            _ => {}
                        }
                    },
                    _ => {}
                }
            }
        }
        return Ok(None)
    }

    fn selection_set(&self) -> &'a graphql_parser::query::SelectionSet<'static,String> {
        match self {
            RawOperation::Query(ss,_) => ss,
            RawOperation::Mutation(ss,_) => ss,
            RawOperation::Subscription(ss,_) => ss
        }
    }


    fn is_introspection(&self) -> bool {
        self.selection_set().items.iter().any(|selection|{
            match selection {
                graphql_parser::query::Selection::Field(field) => {
                    field.name == "__schema"
                },
                _ => false
            }
        })
    }
}

pub enum Operation<Query: FromSelectionSet, Mutation: FromSelectionSet, Subscription: FromSelectionSet> {
    Query(Query),
    Mutation(Mutation),
    Subscription(Subscription)
}

impl<Query: FromSelectionSet, Mutation: FromSelectionSet, Subscription: FromSelectionSet>
    Operation<Query, Mutation, Subscription>
{
    fn make_root<Ctx, T>(self, ctx: Ctx, t: &T) -> internal::Root 
        where Query: MakeRoot<Ctx,T>,
              Mutation: MakeRoot<Ctx,T>,
              Subscription: MakeRoot<Ctx,T>
    {
        match self {
            Operation::Query(q) => MakeRoot::make_root(q, ctx, t),
            Operation::Subscription(s) => MakeRoot::make_root(s,ctx,t),
            Operation::Mutation(m) => MakeRoot::make_root(m,ctx,t)
        }
    }
}

pub enum NotImplemented {}

impl FromSelectionSet for NotImplemented {
    fn from_selection_set(
        _set: &internal::graphql_parser::query::SelectionSet<'static,String>,
        _doc: &internal::graphql_parser::query::Document<'static,String>,
        _variables: &Variables) -> Result<Self, Error> {
        Err(Error::new(ErrorKind::Query).with_source(Box::new(QueryError::NotImplemented)))
    }
}

use std::hash::Hash;

pub trait Symbol : Eq + Hash + Copy + Send + Sync + std::fmt::Debug + 'static{
    fn parse(name: &str) -> Option<Self>;
    fn interfaces(&self) -> &'static [Self];
    fn implementations(&self) -> &'static [Self];
}

pub fn normalize_object<'doc, T: Symbol>(
    selection_set: &'doc graphql_parser::query::SelectionSet<'static,String>,
    doc: &'doc graphql_parser::query::Document<'static,String>,
    typ: T
    ) -> Result<Vec<&'doc graphql_parser::query::Field<'static,String>>,Error> {
    // TODO: duplicates
    let fragments : HashMap<&'doc String,&graphql_parser::query::FragmentDefinition<'static,String>> = doc.definitions.iter().flat_map(|def|
        if let graphql_parser::query::Definition::Fragment(frag) = def {
            Some(frag)
        } else {
            None
        }).map(|frag| (&frag.name, frag) ).collect();
    let mut result : Vec<&'doc graphql_parser::query::Field<'static,String>> = Vec::new();
    fn recurse<'doc, T: Symbol>(
        selection_set: &'doc graphql_parser::query::SelectionSet<'static,String>,
        fragments: &HashMap<&'doc String,&'doc graphql_parser::query::FragmentDefinition<'static,String>>,
        typ: T,
        result: &mut Vec<&'doc graphql_parser::query::Field<'static,String>>
        ) -> Result<(),Error> {
        for selection in selection_set.items.iter() {
            match selection {
                graphql_parser::query::Selection::Field(field) => result.push(field),
                graphql_parser::query::Selection::FragmentSpread(spread) => {
                    let definition = fragments[&spread.fragment_name];
                    match &definition.type_condition {
                        graphql_parser::query::TypeCondition::On(name) => {
                            let t : T = Symbol::parse(name.as_str()).unwrap();
                            if Symbol::interfaces(&typ).contains(&t) {
                                recurse( &definition.selection_set, fragments, typ, result )?
                            }
                        }
                    };
                },
                graphql_parser::query::Selection::InlineFragment(definition) => {
                    match &definition.type_condition {
                        Some(graphql_parser::query::TypeCondition::On(name)) => {
                            let t : T = Symbol::parse(name.as_str()).unwrap();
                            if Symbol::interfaces(&typ).contains(&t) {
                                recurse( &definition.selection_set, fragments, typ, result )?
                            }
                        },
                        None => {
                            recurse( &definition.selection_set, fragments, typ, result )?
                        }
                    }
                }
            }
        }
        Ok(())
    }
    recurse(selection_set, &fragments, typ, &mut result)?;
    return Ok(result)
}

pub fn normalize_interface<'doc, T: Symbol>(
    selection_set: &'doc graphql_parser::query::SelectionSet<'static,String>,
    doc: &'doc graphql_parser::query::Document<'static,String>,
    typ: T
    ) -> Result<HashMap<T,Vec<&'doc graphql_parser::query::Field<'static,String>>>,Error> {
    // TODO: duplicates
    let fragments : HashMap<&String,&graphql_parser::query::FragmentDefinition<'static,String>> = doc.definitions.iter().flat_map(|def|
        if let graphql_parser::query::Definition::Fragment(frag) = def {
            Some(frag)
        } else {
            None
        }).map(|frag| (&frag.name, frag) ).collect();
    let mut result : HashMap<T, Vec<&'doc graphql_parser::query::Field<'static,String>>> = HashMap::with_capacity(Symbol::implementations(&typ).len());
    for key in Symbol::implementations(&typ).iter() {
        result.insert(*key,Vec::new());
    }
    fn recurse<'doc, T: Symbol>(
        doc: &'doc graphql_parser::query::Document<'static,String>,
        selection_set: &'doc graphql_parser::query::SelectionSet<'static,String>,
        fragments: &HashMap<&'doc String,&'doc graphql_parser::query::FragmentDefinition<'static,String>>,
        typ: T,
        result: &mut HashMap<T, Vec<&'doc graphql_parser::query::Field<'static,String>>>
        ) -> Result<(),Error> {
        for selection in selection_set.items.iter() {
            match selection {
                graphql_parser::query::Selection::Field(field) => {
                    for fs in result.values_mut() {
                        fs.push(field)
                    }
                },
                graphql_parser::query::Selection::FragmentSpread(spread) => {
                    let definition = fragments[&spread.fragment_name];
                    match &definition.type_condition {
                        graphql_parser::query::TypeCondition::On(name) => {
                            let t : T = Symbol::parse(name.as_str()).unwrap();
                            for imp in Symbol::implementations(&typ).iter() {
                                if Symbol::interfaces(imp).contains(&t) {
                                    result.get_mut(&t).unwrap().extend( normalize_object(&definition.selection_set, doc, *imp)? );
                                }
                            }
                        }
                    };
                },
                graphql_parser::query::Selection::InlineFragment(definition) => {
                    match &definition.type_condition {
                        Some(graphql_parser::query::TypeCondition::On(name)) => {
                            let t : T = Symbol::parse(name).unwrap();
                            for imp in Symbol::implementations(&typ).iter() {
                                if Symbol::interfaces(imp).contains(&t) {
                                    result.get_mut(&t).unwrap().extend( normalize_object(&definition.selection_set, doc, *imp)? );
                                }
                            }
                        },
                        None => {}
                    }
                }
            }
        }
        Ok(())
    }

    recurse(doc, selection_set, &fragments, typ, &mut result)?;
    return Ok(result)
}

pub struct Response {
    data: Vec<u8>,
    errors: Vec<crate::Error>
}

impl Response {
    pub fn data_as_bytes(&self) -> &[u8] {
        self.data.as_ref()
    }

    pub fn data_as_string(&self) -> &str {
        std::str::from_utf8(self.data.as_ref()).unwrap()
    }

    pub fn errors(&self) -> &[crate::Error] {
        self.errors.as_ref()
    }

    fn from(buf_data: (Vec<u8>,Vec<crate::Error>)) -> Result<Self,Error> {
        let fatal = buf_data.1.iter().any(|e| e.is_fatal() );
        if fatal {
            Err(buf_data.1.into_iter().filter(|e| e.is_fatal() ).next().unwrap())
        } else {
            Ok(Self{
                data: buf_data.0,
                errors: buf_data.1
            })
        }
    }
}

pub mod internal {
    use std::future::Future;
    use std::task::{Context, Poll};
    use std::pin::Pin;
    pub use graphql_parser;
    pub use crate::fork_buf::ForkBuf;

    pub trait Projection {
        type Responder;
    }

    pub struct Root {
        futures: futures_util::stream::FuturesUnordered<super::AckFuture>,
        buf: Option<crate::fork_buf::ForkBufReader>,
    }

    impl Root {
        pub fn from(ack: super::Ack, mut buf: ForkBuf) -> Root {
            match ack {
                Ok(inner) => {
                    let futures = futures_util::stream::FuturesUnordered::new();
                    for future in inner.futures {
                        futures.push(future);
                    }
                    Root{ futures, buf: Some(buf.close()) }
                },
                Err(error) => {
                    buf.add_error(error);
                    let futures : futures_util::stream::FuturesUnordered<super::AckFuture>= futures_util::stream::FuturesUnordered::new();
                    Root{ futures, buf: Some(buf.close()) }
                }
            }
        }

        pub(crate) fn failed(error: crate::Error) -> Root {
            let mut buf = ForkBuf::new();
            buf.add_error(error);
            let futures : futures_util::stream::FuturesUnordered<super::AckFuture>= futures_util::stream::FuturesUnordered::new();
            Root{ futures, buf: Some(buf.close()) }
        }
    }

    impl Future for Root {

        type Output = Result<crate::Response, crate::Error>;

        fn poll(self: Pin<&mut Self>, ctx: &mut Context) -> Poll<Self::Output> {
            let mut_self = self.get_mut();
            loop {
                let poll = futures_util::stream::Stream::poll_next(Pin::new(&mut mut_self.futures), ctx);
                match poll {
                    Poll::Pending => return Poll::Pending,
                    Poll::Ready(Some(ack)) => {
                                for future in ack.futures {
                                    mut_self.futures.push(future);
                                }
                    },
                    Poll::Ready(None) =>
                        
                        return Poll::Ready(crate::Response::from(mut_self.buf.take().unwrap().eat()))
                }
            }
        }

    }

}

type AckFuture = futures_util::future::BoxFuture<'static, AckInner>;

/**
 * Type for acknowledging that you've yielded a value.
 *
 * You never construct this value but retrieve it from a responder.
 */
pub type Ack = Result<AckInner, Error>;

pub struct AckInner {
    futures: Vec<AckFuture>
}

impl std::fmt::Debug for AckInner {
    fn fmt(&self, w: &mut std::fmt::Formatter) -> std::fmt::Result {
        write![w, "Ack({})", self.futures.len()]
    }
}

#[doc(hidden)]
pub mod ack {
    pub fn new() -> crate::Ack {
        crate::Ack::Ok(crate::AckInner{ futures: vec![] } )
    }
    pub fn new_inner() -> crate::AckInner {
        crate::AckInner{ futures: vec![] }
    }
    pub fn of(future: crate::AckFuture) -> crate::Ack {
        crate::Ack::Ok(crate::AckInner{ futures: vec![future] } )
    }

    pub fn is_empty(ack: &crate::Ack) -> bool {
        match ack {
            crate::Ack::Ok(inner) => inner.futures.is_empty(),
            _ => false
        }
    }

    pub fn merge(a: &mut crate::Ack, b: crate::Ack) {
        take_mut::take(a, |a|
            match (a,b) {
                ( crate::Ack::Ok(mut ai), crate::Ack::Ok(bi) ) => {
                    ai.futures.extend(bi.futures);
                    crate::Ack::Ok(crate::AckInner{futures: ai.futures})
                },
                ( crate::Ack::Err(ae), _ ) => {
                    crate::Ack::Err(ae)
                },
                ( _, crate::Ack::Err(be) ) => {
                    crate::Ack::Err(be)
                }
            }
        );
    }

    pub fn into_inner(ack: crate::Ack, buf: &mut crate::fork_buf::ForkBuf) -> crate::AckInner {
        match ack {
            Ok(inner) => inner,
            Err(err) => {
                buf.add_error(err);
                new_inner()
            }
        }
    }

    #[inline]
    pub fn is_fatal(ack: &crate::Ack) -> bool {
        match ack {
            Err(e) => e.fatal,
            _ => false
        }
    }

    #[inline]
    pub fn is_err(ack: &crate::Ack) -> bool {
        match ack {
            Err(_) => true,
            _ => false
        }
    }


}

pub(crate) fn type_to_string(typ: &internal::graphql_parser::query::Type<'static,String>) -> String {
    match typ {
        internal::graphql_parser::query::Type::NamedType(name) => name.to_string(),
        internal::graphql_parser::query::Type::ListType(inner) => {
            "[".to_string() + &*type_to_string(inner) + "]"
        },
        internal::graphql_parser::query::Type::NonNullType(inner) => {
            type_to_string(inner) + "!"
        }
    }
}

/**
 * Trait for creating rust values from GraphQL values.
 */
pub trait FromValue : Sized {
    /**
     * Creates a new rust value from a GraphQL value.
     */
    fn from_value(value: &internal::graphql_parser::query::Value<'static,String>, variables: &Variables) -> Result<Self, Error>;

    /**
     * Checks whether a GraphQL value can be turned into a rust value.
     *
     * It's only necessary to implement this method for allocating types.
     */
    fn accepts(value: &internal::graphql_parser::query::Value<'static,String>) -> Result<(),Error> {
        Self::from_value(value, &Variables::empty()).map(|_| ())
    }

    /**
     * Checks whether a certain GraphQL type is compatible with a rust value.
     *
     * This method mainly coerces nullability.
     *
     * ```
     * use earl::FromValue;
     * // A type is assigneable from itself ...
     * assert_eq!(<i64 as FromValue>::is_assignable_from(&<i64 as FromValue>::graphql_type()), true);
     * // ... but not from its nullable version.
     * assert_eq!(<i64 as FromValue>::is_assignable_from(&<Option<i64> as FromValue>::graphql_type()), false);
     * // The other way round works thought.
     * assert_eq!(<Option<i64> as FromValue>::is_assignable_from(&<i64 as FromValue>::graphql_type()), true);
     * ```
     */
    fn is_assignable_from(typ: &internal::graphql_parser::query::Type<'static,String>) -> bool;

    /**
     * The canonic GraphQL type of this rust type.
     *
     * ```
     * use earl::FromValue;
     * assert_eq!(<i64 as FromValue>::graphql_type().to_string(), "Int!");
     * assert_eq!(<Option<i64> as FromValue>::graphql_type().to_string(), "Int");
     * assert_eq!(<Vec<i64> as FromValue>::graphql_type().to_string(), "[Int!]!");
     * ```
     */
    fn graphql_type() -> internal::graphql_parser::query::Type<'static,String>;
}

pub trait Scalar : Sized {
    fn read_value(value: &graphql_parser::query::Value<'static,String>) -> Result<Self, Error>;

    fn write_value<'a>(&self, writer: ScalarWriter<'a>) -> Result<(), Error>;
}

pub struct ScalarWriter<'a> {
    buf: &'a mut internal::ForkBuf
}

impl<'a> ScalarWriter<'a> {
    
    pub fn new(buf: &'a mut internal::ForkBuf) -> Self{
        ScalarWriter{ buf }
    }

    pub fn write_str(self, value: &str) -> Result<(), Error>{
        self.buf.put(&[b'"']);
        for b in value.as_bytes().iter() {
            match b {
                b'\\' | b'"' => self.buf.put(&[b'\\', *b]),
                b'\n' => self.buf.put(&[b'\\', b'n']),
                _ => self.buf.put(&[*b])
            }
        }
        self.buf.put(&[b'"']);
        Ok(())
    }
}

impl FromValue for String {
    fn from_value(value: &internal::graphql_parser::query::Value<'static,String>, variables: &Variables) -> Result<Self, Error> {
        match variables.resolve::<Self>(value)? {
            internal::graphql_parser::query::Value::String(s) => Ok(s.to_string()),
            elze => Err(Error::new(ErrorKind::Query).with_extension(extensions::VariableValue(elze.clone())))
        }
    }
    
    fn is_assignable_from(typ: &internal::graphql_parser::query::Type<'static,String>) -> bool {
        match typ {
            internal::graphql_parser::query::Type::NonNullType(inner) => {
                match &**inner {
                    internal::graphql_parser::query::Type::NamedType(name) => {
                        name == "String"
                    },
                    _ => false
                }
            },
            _ => false
        }
    }

    fn graphql_type() -> internal::graphql_parser::query::Type<'static,String> {
        internal::graphql_parser::query::Type::NonNullType(
            Box::new(internal::graphql_parser::query::Type::NamedType("String".to_string()))
        )
    }
}

impl FromValue for i64 {
    fn from_value(value: &internal::graphql_parser::query::Value<'static,String>, variables: &Variables) -> Result<Self, Error> {
        match variables.resolve::<Self>(value)? {
            internal::graphql_parser::query::Value::Int(i) => 
                Ok(i.as_i64().unwrap()),
            elze => Err(Error::new(ErrorKind::Query).with_extension(extensions::VariableValue(elze.clone())))
        }
    }

    fn is_assignable_from(typ: &internal::graphql_parser::query::Type<'static,String>) -> bool {
        match typ {
            internal::graphql_parser::query::Type::NonNullType(inner) => {
                match &**inner {
                    internal::graphql_parser::query::Type::NamedType(name) => {
                        name == "Int"
                    },
                    _ => false
                }
            },
            _ => false
        }
    }

    fn graphql_type() -> internal::graphql_parser::query::Type<'static,String> {
        internal::graphql_parser::query::Type::NonNullType(
            Box::new(internal::graphql_parser::query::Type::NamedType("Int".to_string()))
        )
    }
}
impl FromValue for f64 {
    fn from_value(value: &internal::graphql_parser::query::Value<'static,String>, variables: &Variables) -> Result<Self, Error> {
        match variables.resolve::<Self>(value)? {
            internal::graphql_parser::query::Value::Float(i) => Ok(*i),
            elze => Err(Error::new(ErrorKind::Query).with_extension(extensions::VariableValue(elze.clone())))
        }
    }

    fn is_assignable_from(typ: &internal::graphql_parser::query::Type<'static,String>) -> bool {
        match typ {
            internal::graphql_parser::query::Type::NonNullType(inner) => {
                match &**inner {
                    internal::graphql_parser::query::Type::NamedType(name) => {
                        name == "Float"
                    },
                    _ => false
                }
            },
            _ => false
        }
    }

    fn graphql_type() -> internal::graphql_parser::query::Type<'static,String> {
        internal::graphql_parser::query::Type::NonNullType(
            Box::new(internal::graphql_parser::query::Type::NamedType("Float".to_string()))
        )
    }
}

impl FromValue for bool {
    fn from_value(value: &internal::graphql_parser::query::Value<'static,String>, variables: &Variables) -> Result<Self, Error> {
        match variables.resolve::<Self>(value)? {
            internal::graphql_parser::query::Value::Boolean(i) => Ok(*i),
            elze => Err(Error::new(ErrorKind::Query).with_extension(extensions::VariableValue(elze.clone())))
        }
    }

    fn is_assignable_from(typ: &internal::graphql_parser::query::Type<'static,String>) -> bool {
        match typ {
            internal::graphql_parser::query::Type::NonNullType(inner) => {
                match &**inner {
                    internal::graphql_parser::query::Type::NamedType(name) => {
                        name == "Boolean"
                    },
                    _ => false
                }
            },
            _ => false
        }
    }

    fn graphql_type() -> internal::graphql_parser::query::Type<'static,String> {
        internal::graphql_parser::query::Type::NonNullType(
            Box::new(internal::graphql_parser::query::Type::NamedType("String".to_string()))
        )
    }
}

impl<T: FromValue> FromValue for Option<T> {
    fn from_value(value: &internal::graphql_parser::query::Value<'static,String>, variables: &Variables) -> Result<Self, Error> {
        match variables.resolve::<Self>(value)? {
            internal::graphql_parser::query::Value::Null => Ok(None),
            _ => <T as FromValue>::from_value(value, variables).map(Some)
        }
    }

    fn is_assignable_from(typ: &internal::graphql_parser::query::Type<'static,String>) -> bool {
        match typ {
            internal::graphql_parser::query::Type::NonNullType(_) => {
                T::is_assignable_from(typ)
            },
            _ => {
                //TODO: get rid of this allocation
                T::is_assignable_from(
                    &internal::graphql_parser::query::Type::NonNullType(Box::new(typ.clone()))
                )
            }
        }
    }

    fn graphql_type() -> internal::graphql_parser::query::Type<'static,String> {
        match <T as FromValue>::graphql_type() {
            internal::graphql_parser::query::Type::NonNullType(inner) => *inner,
            other => {
                panic!("FromValue::graphql_type is already nullable: {}", other);
            }
        }
    }
}

impl<T: FromValue> FromValue for Vec<T> {
    fn from_value(value: &internal::graphql_parser::query::Value<'static,String>, variables: &Variables) -> Result<Self, Error> {
        match variables.resolve::<Self>(value)? {
            internal::graphql_parser::query::Value::List(list) => {
                list.iter()
                    .enumerate()
                    .map(|(i,elem)|
                        <T as FromValue>::from_value(elem, variables).map_err(|e| e.at_index(i).or_with_extension(|| extensions::VariableValue(elem.clone())) )
                    )
                    .collect::<Result<Vec<T>,Error>>()
            },
            elze => Err(Error::new(ErrorKind::Query).with_extension(extensions::VariableValue(elze.clone())))
        }
    }

    fn is_assignable_from(typ: &internal::graphql_parser::query::Type<'static,String>) -> bool {
        match typ {
            internal::graphql_parser::query::Type::NonNullType(inner) => {
                match &**inner {
                    internal::graphql_parser::query::Type::ListType(list_inner) => {
                        T::is_assignable_from(list_inner)
                    },
                    _ => false
                }
            },
            _ => false
        }
    }

    fn graphql_type() -> internal::graphql_parser::query::Type<'static,String> {
        internal::graphql_parser::query::Type::NonNullType(
            Box::new(
                internal::graphql_parser::query::Type::ListType(Box::new(<T as FromValue>::graphql_type()))
            )
        )
    }
}

macro_rules! responders {
    (|$buf:ident, $value:ident: &$typ:ty| $write:expr) => {
        responders!{...}

        impl<'req, Ctx> Responder<'req, Ctx> {
            pub fn ok<T>(self, value: &T) -> crate::Ack
                where T: std::borrow::Borrow<$typ>
            {
                let $buf = self.buf;
                let $value = value.borrow();
                $write;
                crate::ack::new()
            }
        }

        impl<'req, Ctx> Responder<'req, Ctx>
            where Ctx: Send + Clone + 'static
            {
            pub fn lazy<F, T>(self, f: F) -> crate::Ack
                where F: ::std::future::Future<Output=Result<T,crate::Error>> + Send + 'static,
                      T: std::borrow::Borrow<$typ>
                {
                use crate::FutureExt;
                let Responder{ buf, context } = self;
                let mut buf = buf.fork();
                let context = context.clone();
                crate::ack::of(f.map(move |result|{
                    match result {
                        Ok(value) => {
                            let responder = Responder{ buf: &mut buf, context: &context };
                            let r = responder.ok(&value.borrow());
                            crate::ack::into_inner(r, &mut buf)
                        },
                        Err(err) => {
                            buf.ensure_field_is_closed();
                            buf.add_error(err);
                            crate::ack::new_inner()
                        }
                    }
                }).boxed())
            }
        }

        impl<'req, Ctx> OptionalResponder<'req, Ctx> {
            pub fn ok<T>(self, value: Option<&T>) -> crate::Ack 
                where T: std::borrow::Borrow<$typ>
                {
                if let Some(inner) = value {
                    let $buf = self.buf;
                    let $value = inner.borrow();
                    $write;
                } else {
                    self.buf.put("null".as_bytes());
                }
                crate::ack::new()
            }
            pub fn some<T>(self, value: &T) -> crate::Ack
                where T: std::borrow::Borrow<$typ>
            {
                self.ok(Some(value))
            }
        }

        impl<'req, Ctx> ListResponder<'req, Ctx> {
            pub fn ok<T>(self, value: &[T]) -> crate::Ack
                where T: std::borrow::Borrow<$typ>
            {
                let ListResponder{ $buf, .. } = self;
                let mut first = true;
                $buf.start_array();
                for inner in value.iter() {
                    if !first {
                        $buf.next_element();
                    } else {
                        first = false;
                    }
                    let $value = inner.borrow();
                    $write;
                }
                $buf.end_array();
                crate::ack::new()
            }
        }
    };

    (|$buf:ident, $value:ident: $typ:ty| $write:expr) => {
        responders!{...}

        impl<'req, Ctx> Responder<'req, Ctx> {
            pub fn ok(self, value: $typ) -> crate::Ack {
                let $buf = self.buf;
                let $value = value;
                $write;
                crate::ack::new()
            }
        }

        impl<'req, Ctx> Responder<'req, Ctx>
            where Ctx: Send + Clone + 'static
            {
            pub fn lazy<F>(self, f: F) -> crate::Ack
                where F: ::std::future::Future<Output=Result<$typ,crate::Error>> + Send + 'static
                {
                use crate::FutureExt;
                let Responder{ buf, context } = self;
                let mut buf = buf.fork();
                let context = context.clone();
                crate::ack::of(f.map(move |result|{
                    match result {
                        Ok(value) => {
                            let responder = Responder{ buf: &mut buf, context: &context };
                            let r = responder.ok(value);
                            crate::ack::into_inner(r, &mut buf)
                        },
                        Err(err) => {
                            buf.add_error(err.with_fatal());
                            crate::ack::new_inner()
                        }
                    }
                }).boxed())
            }
        }

        impl<'req, Ctx> OptionalResponder<'req, Ctx> {
            pub fn ok(self, value: Option<$typ>) -> crate::Ack {
                if let Some($value) = value {
                    let $buf = self.buf;
                    $write;
                } else {
                    self.buf.put("null".as_bytes());
                }
                crate::ack::new()
            }
            pub fn some(self, value: $typ) -> crate::Ack {
                self.ok(Some(value))
            }
        }
        impl<'req, Ctx> OptionalResponder<'req, Ctx>
            where Ctx: Send + Clone + 'static
            {
            pub fn lazy<F>(self, f: F) -> crate::Ack
                where F: ::std::future::Future<Output=Result<Option<$typ>,crate::Error>> + Send + 'static
                {
                use crate::FutureExt;
                let OptionalResponder{ buf, context } = self;
                let mut buf = buf.fork();
                let context = context.clone();
                crate::ack::of(f.map(move |result|{
                    match result {
                        Ok(value) => {
                            let responder = OptionalResponder{ buf: &mut buf, context: &context };
                            let r = responder.ok(value);
                            crate::ack::into_inner(r, &mut buf)
                        },
                        Err(err) => {
                            buf.ensure_field_is_closed();
                            buf.add_error(err);
                            crate::ack::new_inner()
                        }
                    }
                }).boxed())
            }
        }

        impl<'req, Ctx> ListResponder<'req, Ctx> {
            pub fn ok(self, value: &[$typ]) -> crate::Ack {
                let ListResponder{ $buf, .. } = self;
                let mut first = true;
                $buf.start_array();
                for &$value in value.iter() {
                    if !first {
                        $buf.next_element();
                    } else {
                        first = false;
                    }
                    $write;
                }
                $buf.end_array();
                crate::ack::new()
            }
        }
    };

    (...) => {
        /**
         * Reponder for simple values.
         */
        pub struct Responder<'req, Ctx> {
            #[doc(hidden)]
            pub buf: &'req mut super::fork_buf::ForkBuf,
            #[doc(hidden)]
            pub context: &'req Ctx
        }

        impl<'req, Ctx> Responder<'req, Ctx> {
            /**
             * Provides the context of this GraphQL request.
             */
            pub fn context(&self) -> &Ctx {
                self.context
            }
        }

        pub struct OptionalResponder<'req, Ctx> {
            #[doc(hidden)]
            pub buf: &'req mut super::fork_buf::ForkBuf,
            #[doc(hidden)]
            pub context: &'req Ctx
        }

        impl<'req, Ctx> OptionalResponder<'req, Ctx> {
            pub fn none(self) -> crate::Ack {
                self.buf.put("null".as_bytes());
                crate::ack::new()
            }

            pub fn context(&self) -> &Ctx {
                self.context
            }

            pub fn err<T: Into<crate::Error>>(self, err: T) -> crate::Ack {
                crate::Ack::Err(err.into())
            }
        }

        pub struct ListResponder<'req, Ctx> {
            #[doc(hidden)]
            pub buf: &'req mut super::fork_buf::ForkBuf,
            #[doc(hidden)]
            pub context: &'req Ctx
        }

        impl<'req, Ctx> ListResponder<'req, Ctx> {
            pub fn context(&self) -> &Ctx {
                self.context
            }
        }
    };
}

pub mod string {
    responders!{|buf,value: &str|
        {
            buf.put(&[b'"']);
            for b in value.as_bytes().iter() {
                match b {
                    b'\\' | b'"' => buf.put(&[b'\\', *b]),
                    b'\n' => buf.put(&[b'\\', b'n']),
                    _ => buf.put(&[*b])
                }
            }
            buf.put(&[b'"']);
        }
    }
}

pub mod int {
    responders!{|buf,value: i64|
        buf.put(&value.to_string().as_bytes())
    }

}

pub mod float {
    responders!{|buf,value: f64|
        buf.put(&value.to_string().as_bytes())
    }
}

pub mod boolean {
    responders!{|buf,value: bool|
        if value {
            buf.put(&[b't',b'r',b'u',b'e']);
        } else {
            buf.put(&[b'f',b'a',b'l',b's',b'e']);
        }
    }
}

/**
 * Special type for GraphQL IDs.
 *
 * An ID can be constructed from a String or &str.
 * ```
 * let id = earl::ID::from("A123");
 * assert_eq!( id, earl::ID::from(String::from("A123")) );
 * assert_eq!( id.as_ref(), "A123" );
 * ```
 *
 * An ID can be used as a value in GrapQL queries.
 * ```
 * use earl::{FromValue, Schema};
 * assert_eq!( earl::ID::graphql_type().to_string(), "ID!" );
 *
 * mod schema {
 *    earl_macro::graphql_schema!(r#"
 *    type Query {
 *      echo_id(id: ID!): ID!
 *    }
 *    schema {
 *      query: Query
 *    }
 *    "#);
 * }
 * impl schema::query::Instance<()> for () {
 *   fn echo_id(&self, id: earl::ID, responder: earl::id::Responder<()>) -> earl::Ack {
 *     responder.ok(&id)
 *   }
 * }
 * let run = schema::Schema::query(r#"{
 *   echo_id(id: "A123")
 * }"#).run(&());
 * assert_eq!(
 *   futures_executor::block_on(run).unwrap().data_as_string(),
 *   "{\"echo_id\":\"A123\"}"
 * );
 * 
 * ```
 */
#[derive(Clone,PartialEq,Eq,Debug,Hash)]
pub struct ID(String);

impl FromValue for ID {
    fn from_value(value: &internal::graphql_parser::query::Value<'static,String>, variables: &Variables) -> Result<Self, Error> {
        match variables.resolve::<Self>(value)? {
            internal::graphql_parser::query::Value::String(s) => Ok(ID(s.to_string())),
            _ => Err(Error::new(ErrorKind::Query))
        }
    }

    fn is_assignable_from(typ: &internal::graphql_parser::query::Type<'static,String>) -> bool {
        match typ {
            internal::graphql_parser::query::Type::NonNullType(inner) => {
                match &**inner {
                    internal::graphql_parser::query::Type::NamedType(name) => {
                        name == "ID"
                    },
                    _ => false
                }
            },
            _ => false
        }
    }

    fn graphql_type() -> internal::graphql_parser::query::Type<'static,String> {
        internal::graphql_parser::query::Type::NonNullType(
            Box::new(internal::graphql_parser::query::Type::NamedType("ID".to_string()))
        )
    }
}

impl From<String> for ID {
    fn from(s : String) -> Self {
        ID(s)
    }
}

impl From<&str> for ID {
    fn from(s : &str) -> Self {
        ID(s.into())
    }
}

impl AsRef<str> for ID {
    fn as_ref(&self) -> &str {
        self.0.as_ref()
    }
}

pub mod id {
    responders!{|buf,value: &crate::ID|
        {
            buf.put(&[b'"']);
            for b in value.0.as_bytes().iter() {
                match b {
                    b'\\' | b'"' => buf.put(&[b'\\', *b]),
                    b'\n' => buf.put(&[b'\\', b'n']),
                    _ => buf.put(&[*b])
                }
            }
            buf.put(&[b'"']);
        }
    }
}



#[cfg(test)]
mod tests {
    use std::collections::hash_map::HashMap;
    use graphql_parser;
    use graphql_parser::query;
    use super::*;

    use indoc::indoc;
    use pretty_assertions::{assert_eq};
    fn first_op<'a>(qry: &'a query::Document<'static,String>) -> &'a graphql_parser::query::SelectionSet<'static,String> {
        if let query::Definition::Operation(op) = &qry.definitions[0] {
            if let query::OperationDefinition::SelectionSet(ss) = op {
                return ss
            }
        }
        panic!("no op")
    }

    fn all_fields<'doc>(qry: &'doc query::Document<'static,String>) -> Vec<&'doc query::Field<'static,String>> {
        let mut result : Vec<&'doc query::Field<'static,String>> = vec![];
        fn collector<'a>(ss: &'a query::SelectionSet<'static,String>, result : &mut Vec<&'a query::Field<'static,String>>){
            for item in ss.items.iter() {
                match item {
                    query::Selection::Field(field) => result.push(field),
                    query::Selection::InlineFragment(frag) => collector(&frag.selection_set, result),
                    _ => {}
                }
            }
        };
        for def in qry.definitions.iter() {
            match def {
                query::Definition::Operation(query::OperationDefinition::SelectionSet(ss)) => {
                    collector(&ss, &mut result);
                },
                query::Definition::Fragment(frag) => {
                    collector(&frag.selection_set, &mut result);
                },
                _ => unimplemented![]
            }
        }
        return result
    }

    #[derive(Clone,Copy,PartialEq,Eq,Hash,Debug)]
    enum FooBar {
        Foo,
        Bar,
        Baz
    }

    impl super::Symbol for FooBar {
        fn parse(s: &str) -> Option<Self> {
            match s {
                "Bar" => Some(FooBar::Bar),
                "Foo" => Some(FooBar::Foo),
                _ => None
            }
        }
        fn interfaces(&self) -> &'static [Self] {
            match self {
                FooBar::Foo => &[FooBar::Foo, FooBar::Baz],
                FooBar::Bar => &[FooBar::Bar, FooBar::Baz],
                FooBar::Baz => &[FooBar::Baz]
            }
        }
        fn implementations(&self) -> &'static [Self] {
            match self {
                FooBar::Baz => &[FooBar::Foo, FooBar::Bar],
                _ => &[]
            }
        }
    }

    #[test]
    fn it_normalizes_selection_sets() {
        let doc = graphql_parser::parse_query(indoc!("
            {
                foo
            }")).unwrap();
        let fields = all_fields(&doc);
        let norm = normalize_object(first_op(&doc), &doc, FooBar::Bar).unwrap();
        let result = vec![
            fields[0]
        ];
        assert_eq!( norm, result );
    }

    #[test]
    fn it_normalizes_selection_sets_with_trivial_fragments() {
        let doc = graphql_parser::parse_query(indoc!("
            {
                foo
                ... Foo
            }
            fragment Foo on Bar {
                bar
            }
            ")).unwrap();
        let fields = all_fields(&doc);
        let norm = normalize_object(first_op(&doc), &doc, FooBar::Bar).unwrap();
        assert_eq!(norm, vec![
            fields[0],
            fields[1]
        ])
    }

    #[test]
    fn it_normalizes_inline_fragments() {
        let doc = graphql_parser::parse_query(indoc!("
            {
                foo
                ... on Bar {
                    bar
                }
            }
            ")).unwrap();
        let fields = all_fields(&doc);
        let norm = normalize_object(first_op(&doc), &doc, FooBar::Bar).unwrap();
        assert_eq!(norm, vec![
            fields[0],
            fields[1]
        ])
    }

    #[test]
    fn it_normalizes_inline_fragments_without_type_condition() {
        let doc = graphql_parser::parse_query(indoc!("
            {
                foo
                ... {
                    bar
                }
            }
            ")).unwrap();
        let fields = all_fields(&doc);
        let norm = normalize_object(first_op(&doc), &doc, FooBar::Bar).unwrap();
        assert_eq!(norm, vec![
            fields[0],
            fields[1]
        ])
    }

    #[test]
    fn it_ignores_fragments_for_wrong_types() {
        let doc = graphql_parser::parse_query(indoc!("
            {
                foo
                ... on Foo {
                    bar
                }
            }
            ")).unwrap();
        let fields = all_fields(&doc);
        let norm = normalize_object(first_op(&doc), &doc, FooBar::Bar).unwrap();
        assert_eq!(norm, vec![
            fields[0]
        ])
    }

    #[test]
    fn it_adds_fields_for_all_implementations_for_inline_fragments() {
        let doc = graphql_parser::parse_query(indoc!("
            {
                foo
                ... on Foo {
                    bar
                }
            }
            ")).unwrap();
        let fields = all_fields(&doc);
        let norm = normalize_interface(first_op(&doc), &doc, FooBar::Baz).unwrap();
        let mut result = HashMap::new();
        result.insert(FooBar::Bar, vec![fields[0]]);
        result.insert(FooBar::Foo, vec![fields[0], fields[1]]);
        assert_eq!(norm, result)
    }

    #[test]
    fn it_adds_fields_for_all_implementations_for_named_fragments() {
        let doc = graphql_parser::parse_query(indoc!("
            {
                foo
                ... Foo
            }
            fragment Foo on Bar {
                bar
            }
            ")).unwrap();
        let fields = all_fields(&doc);
        let norm = normalize_interface(first_op(&doc), &doc,FooBar::Baz).unwrap();
        let mut result = HashMap::new();
        result.insert(FooBar::Bar, vec![fields[0], fields[1]]);
        result.insert(FooBar::Foo, vec![fields[0]]);
        assert_eq!(norm, result)
    }

    #[test]
    fn it_prints_simple_types() {
        assert_eq!(type_to_string(
                &graphql_parser::query::Type::NamedType("String".into())
                ), "String");
    }

    #[test]
    fn string_has_the_correct_type() {
        assert_eq!(type_to_string(&<String as FromValue>::graphql_type()), "String!");
    }

    #[test]
    fn optional_string_has_the_correct_type() {
        assert_eq!(type_to_string(&<Option<String> as FromValue>::graphql_type()), "String");
    }

    #[test]
    fn optional_string_list_has_the_correct_type() {
        assert_eq!(type_to_string(&<Option<Vec<String>> as FromValue>::graphql_type()), "[String!]");
    }

    #[test]
    fn optional_string_optional_list_has_the_correct_type() {
        assert_eq!(type_to_string(&<Option<Vec<Option<String>>> as FromValue>::graphql_type()), "[String]");
    }
}
