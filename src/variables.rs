use std::collections::hash_map::HashMap;
use crate::internal;
use crate::{Error, ErrorKind, QueryError};
use crate::{FromValue, type_to_string};
use crate::Schema;

#[derive(Debug)]
struct Variable {
    used: std::sync::atomic::AtomicBool,
    definition: internal::graphql_parser::query::VariableDefinition<'static,String>,
    value: internal::graphql_parser::query::Value<'static,String>
}

impl Variable {
    fn mark(&self) {
        self.used.store(true, std::sync::atomic::Ordering::Relaxed)
    }
}

/**
 * A ValueProvider provides values for a given set of variables.
 *
 * ```
 * use earl::variables::ValueProvider;
 * struct Id{}
 * 
 * impl ValueProvider for Id {
 *  fn provide(&mut self, variable: &str, typ: &earl::graphql_parser::schema::Type<'static,String>) ->
 *  Result<Option<earl::graphql_parser::query::Value<'static,String>>,earl::Error> {
 *    Ok(Some(earl::graphql_parser::query::Value::String( variable.to_string() )))
 *  }
 * }
 * 
 * let mut id = Id{};
 * assert_eq!(
 *    id.provide( "name", &earl::graphql_parser::schema::Type::NamedType( "string".into() )
 *    ).unwrap(),
 *    Some(earl::graphql_parser::query::Value::String( "name".to_string() ))
 * )
 * ```
 */
pub trait ValueProvider {
    fn provide(&mut self, variable: &str, typ: &internal::graphql_parser::schema::Type<'static, String>) ->
        Result<Option<internal::graphql_parser::query::Value<'static, String>>,crate::Error>;
}

/**
 * Empty Value provider.
 *
 * ```
 * use earl::variables::{ValueProvider, NoValues};
 * 
 * let mut p = NoValues();
 * assert_eq!(
 *    p.provide( "name", &earl::graphql_parser::schema::Type::NamedType( "string".into() )
 *    ).unwrap(),
 *    None
 * )
 * ```
 *
 */
#[derive(Debug)]
pub struct NoValues();

impl ValueProvider for NoValues {
    fn provide(&mut self, _variable: &str, _typ: &internal::graphql_parser::schema::Type<'static,String>) -> Result<Option<internal::graphql_parser::query::Value<'static,String>>,crate::Error> {
        Ok(None)
    }
}

impl ValueProvider for HashMap<String,internal::graphql_parser::query::Value<'static, String>> {
    fn provide(&mut self, variable: &str, _typ: &internal::graphql_parser::schema::Type<'static, String>) -> Result<Option<internal::graphql_parser::query::Value<'static,String>>,crate::Error> {
        Ok(self.remove(variable))
    }
}

/**
 * Set of variables with values.
 *
 * ```
 * use earl::variables::Variables;
 *
 * let mut v = Variables::empty();
 * v.insert::<i64>( "a".into(), earl::graphql_parser::query::Value::Int( 42.into() ) );
 * 
 * // Resolves a simple variable
 * assert_eq!(
 *  v.resolve::<i64>( &earl::graphql_parser::query::Value::Variable( "a".into() ) ).unwrap(),
 *  &earl::graphql_parser::query::Value::Int( 42.into() )
 * );
 *
 * // Keeps everything else in place
 * assert_eq!(
 *  v.resolve::<i64>( &earl::graphql_parser::query::Value::Int( 13.into() ) ).unwrap(),
 *  &earl::graphql_parser::query::Value::Int( 13.into() )
 * );
 *
 * // Uses FromValue::is_assignable_from for type checking
 * assert_eq!(
 *  v.resolve::<Option<i64>>( &earl::graphql_parser::query::Value::Variable( "a".into() ) ).unwrap(),
 *  &earl::graphql_parser::query::Value::Int( 42.into() )
 *  // Works because an i64 can be assigned to an Option<i64>
 * );
 * assert_eq!(
 *  v.resolve::<f64>( &earl::graphql_parser::query::Value::Variable( "a".into() )
 *  ).unwrap_err().to_string(),
 *  // Fails because i64 cannot be assigned to an f64.
 *  "Type mismatch on variable $a and argument key (Int! / Float!)"
 * );
 *
 */
#[derive(Debug)]
pub struct Variables {
    variables: HashMap<String, Variable>
}

impl Variables {

    pub fn empty() -> Self {
        Variables{ variables: HashMap::new() }
    }

    pub(crate) fn new<S: Schema + ?Sized>(var_defs: &[internal::graphql_parser::query::VariableDefinition<'static,String>],values: &mut dyn ValueProvider) -> Result<Self,Error> {
        let mut vars = HashMap::with_capacity(var_defs.len());
        for def in var_defs.iter() {
            let t = S::input_type(&def.var_type)
                .ok_or_else(|| Error::new(ErrorKind::Query)
                    .with_fatal()
                    .with_pos(&def.position)
                    .with_source(Box::new(QueryError::InvalidVariableType{
                        variable: def.name.to_string(),
                        defined_type: type_to_string(&def.var_type)
                    })))?;
            let v = values.provide(&def.name, &def.var_type)?
                    .or(def.default_value.clone())
                    .ok_or_else(|| Error::new(ErrorKind::Query)
                        .with_fatal()
                        .with_source(
                        Box::new(QueryError::InvalidVariableValue{
                            variable: def.name.to_string(),
                            defined_type: type_to_string(&def.var_type)
                        }))
                    )?;
            t(&v).map_err(|e|
                e.with_pos(&def.position)
                 .with_fatal()
                 .or_with_source(|| Box::new(QueryError::InvalidVariableValue{
                        variable: def.name.to_string(),
                        defined_type: type_to_string(&def.var_type)
                    }))
                 .or_with_extension(|| crate::extensions::VariableValue(v.clone()))
                )?;

            vars.insert(def.name.to_string(),Variable{
                used: std::sync::atomic::AtomicBool::new(false),
                definition: def.clone(),
                value: v
            });
        }
        return Ok(Self{ variables: vars })
    }

    /**
     * Adds a fixed value to the Values. This is only used for testing.
     */
    pub fn insert<T: FromValue>(&mut self, name: String, value: crate::graphql_parser::query::Value<'static,String>) {
        let dname = name.clone();
        self.variables.insert(
            name,
            Variable{
                used: std::sync::atomic::AtomicBool::new(false),
                definition: crate::graphql_parser::query::VariableDefinition{
                    name: dname,
                    position: crate::graphql_parser::Pos{ line: 0, column: 0 },
                    var_type: T::graphql_type(),
                    default_value: None
                },
                value
            }
        );
    }

    /**
     * Resolves one level of variable indirection.
     */
    pub fn resolve<'a, T: FromValue>(&'a self,value: &'a internal::graphql_parser::query::Value<'static,String>) -> Result<&'a internal::graphql_parser::query::Value<'static,String>, Error> {
        match value {
            internal::graphql_parser::query::Value::Variable(var_name) => {
                if let Some(var) = self.variables.get::<str>(var_name.as_ref()) {
                    var.mark();
                    if T::is_assignable_from(&var.definition.var_type) {
                        Ok(&var.value)
                    } else {
                        Err(Error::new(ErrorKind::Query).with_fatal().with_source(Box::new(QueryError::IncompatibleVariableDefinition{
                            variable: var_name.to_string(),
                            defined_type: type_to_string(&var.definition.var_type),
                            expected_type: type_to_string(&T::graphql_type())
                        })))
                    }
                } else {
                    Err(Error::new(ErrorKind::Query).with_fatal().with_source(Box::new(QueryError::UnknownVariable{
                        variable: var_name.to_string()
                    })))
                }
            },
            _ => return Ok(value)
        }
    }
}
