use std::convert::Infallible;
use futures::prelude::*;

mod hello {
    #![allow(non_snake_case)]
    #![allow(non_camel_case_types)]
    #![allow(dead_code)]
    earl_macro::graphql_schema!(r#"
"""
An example input object
"""
input AnInputObject {
    with_default: String! = "z",
    nullable: String,
    required: String!
}

"""
A nested input object
"""
input ANestedInputObject {
    inner: AnInputObject!
}

"""
Output object for the example input object
"""
type EchoInputObject {
    with_default: String!
    nullable: String
    required: String!
}

"""
Output object for the nested input object
"""
type EchoNestedInputObject {
    inner: EchoInputObject!
}

enum EchoEnum {
    FOO
    BAR
}

type Query {
    echoList(s: [String!]!): [String!]!
    echoString(s: String!, lazy: Boolean! = false): String!
    echoInt(i: Int!, lazy: Boolean! = false): Int!
    echoFloat(f: Float!, lazy: Boolean! = false): Float!
    echoInputObject(o: AnInputObject!): EchoInputObject!
    echoNestedInputObject(o: ANestedInputObject!): EchoNestedInputObject!
    echoId(i: ID!): ID!
    echoEnum(e: EchoEnum!): EchoEnum!
    error(questionMark: Boolean! = false, fatal: Boolean! = false, lazy: Boolean! = false): Int
    errorList(questionMark: Boolean! = false, fatal: Boolean! = false, lazy: Boolean! = false): [EchoInputObject!]!
    this(lazy: Boolean! = false): Query!
}

interface Uargs {
    lol(x: String!): String!
}


type Bar implements Uargs {
    foo: String!
}

schema {
    query: Query
}
"#
);
}

#[derive(Debug)]
struct JustError {
    s: String
}

impl std::fmt::Display for JustError {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write![f, "{}", &self.s]
    }
}

impl std::error::Error for JustError {

}

struct EchoInputObject<'a>(&'a hello::AnInputObject);

impl<'a> hello::echo_input_object::Instance<()> for EchoInputObject<'a> {
    fn with_default(&self,
        responder: earl::string::Responder<()>
    ) -> earl::Ack {
        responder.ok(&self.0.with_default)
    }

    fn required(&self,
        responder: earl::string::Responder<()>
    ) -> earl::Ack {
        responder.ok(&self.0.required)
    }

    fn nullable(&self,
        responder: earl::string::OptionalResponder<()>
    ) -> earl::Ack {
        responder.ok(self.0.nullable.as_ref())
    }
}

struct EchoNestedInputObject(hello::ANestedInputObject);

impl hello::echo_nested_input_object::Instance<()> for EchoNestedInputObject {
    fn inner(&self,
        responder: hello::echo_input_object::Responder<()>
    ) -> earl::Ack {
        responder.ok(&EchoInputObject(&self.0.inner))
    }
}

impl hello::query::Instance<()> for () {

    fn echoEnum(&self,
        s: hello::EchoEnum,
        responder: hello::echo_enum::Responder<()>
    ) -> earl::Ack {
        responder.ok(&s) 
    }
    fn echoList(&self,
        s: Vec<String>,
        responder: earl::string::ListResponder<()>
    ) -> earl::Ack {
        responder.ok(&s) 
    }

    fn echoString(&self,
        s: String,
        lazy: bool,
        responder: earl::string::Responder<()>
        ) -> earl::Ack  {
        if lazy {
            responder.lazy(async move {
                Ok(s)
            })
        } else {
            responder.ok(&s)
        }
    }

    fn echoInt(&self,
        i: i64,
        lazy: bool,
        responder: earl::int::Responder<()>
        ) -> earl::Ack  {
        if lazy {
            responder.lazy(async move {
                Ok(i)
            })
        } else {
            responder.ok(i)
        }
    }

    fn echoFloat(&self,
        f: f64,
        lazy: bool,
        responder: earl::float::Responder<()>
        ) -> earl::Ack {
        if lazy {
            responder.lazy(async move {
                Ok(f)
            })
        } else {
            responder.ok(f)
        }
    }

    fn echoInputObject(&self,
        o: hello::AnInputObject,
        responder: hello::echo_input_object::Responder<()>
    ) -> earl::Ack {
        responder.ok(&EchoInputObject(&o))
    }

    fn echoNestedInputObject(&self,
        o: hello::ANestedInputObject,
        responder: hello::echo_nested_input_object::Responder<()>
    ) -> earl::Ack {
        responder.ok(&EchoNestedInputObject(o))
    }

    fn echoId(&self,
        i: earl::ID,
        responder: earl::id::Responder<()>
    ) -> earl::Ack {
        responder.ok(&i)
    }

    fn error(&self,
        questionMark: bool,
        fatal: bool,
        lazy: bool,
        responder: earl::int::OptionalResponder<()>
    ) -> earl::Ack {
        if lazy {
            responder.lazy(async move {
                if fatal {
                    Err(earl::Error::from(JustError{s: "No you don't!".to_string()}).with_fatal())?
                } else {
                    Err(JustError{s: "No you don't!".to_string()})?
                }
            })
        } else {
            if questionMark {
                if fatal {
                    Err(earl::Error::from(JustError{s: "No you don't!".to_string()}).with_fatal())?
                } else {
                    Err(JustError{s: "No you don't!".to_string()})?
                }
            } else {
                if fatal {
                    responder.err(
                        earl::Error::from(JustError{s: "No you don't!".to_string()}).with_fatal()
                    )
                } else {
                    responder.err(JustError{s: "No you don't!".to_string()})
                }
            }
        }
    }

    fn errorList(&self,
        _questionMark: bool,
        fatal: bool,
        lazy: bool,
        responder: hello::echo_input_object::ListResponder<()>
    ) -> earl::Ack {
        if lazy {
            responder.lazy::<EchoInputObject,_>(async move {
                if fatal {
                    Err(earl::Error::from(JustError{s: "No you don't!".to_string()}).with_fatal())?
                } else {
                    Err(JustError{s: "No you don't!".to_string()})?
                }
            })
        } else {
            if fatal {
                Err(earl::Error::from(JustError{s: "No you don't!".to_string()}).with_fatal())?
            } else {
                Err(JustError{s: "No you don't!".to_string()})?
            }
        }
    }

    fn this(&self,
        lazy: bool,
        responder: hello::query::Responder<()>
    ) -> earl::Ack {
        if lazy {
            responder.lazy(futures::future::ok(()))
        } else {
            responder.ok(&())
        }
    }
}

fn server() -> impl Future<Output=()> {
    hyper::Server::bind(&([127u8,0,0,1],12345u16).into()).serve(
        hyper::service::make_service_fn(move |_: &hyper::server::conn::AddrStream| async{
            Ok::<_,Infallible>(
                service()
            )
        })
    ).map(|r| r.unwrap() )
}

fn service() -> impl tower_service::Service<
        hyper::Request<hyper::Body>,
        Response=hyper::Response<hyper::Body>,
        Error=Infallible,
        Future=impl Future<Output=Result<hyper::Response<hyper::Body>,Infallible>> + Send> {
    hyper::service::service_fn(move |req| async{
        let res = earl::hyper::call::<hello::Schema,_>((), req).await;
        Ok::<_,Infallible>(res)
    })
}

#[tokio::main]
async fn main() {
    server().await;
}

#[cfg(test)]
mod tests {
    use serde::Serialize;
    use serde;
    use futures::prelude::*;
    use serde_json;
    use serde_json::json;
    use hyper::service::Service;
   

    fn uri() -> hyper::Uri {
        hyper::Uri::from_static("http://localhost:12345")
    }

    fn body(
        query: &str,
        operation_name: Option<&str>,
        variables: serde_json::Value
        ) -> String {
        #[derive(Serialize)]
        struct Body<'a> {
            query: &'a str,
            #[serde(rename = "operationName", skip_serializing_if = "Option::is_none")]
            operation_name: Option<&'a str>,
            #[serde(skip_serializing_if = "serde_json::Value::is_null")]
            variables: serde_json::Value
        }
        serde_json::to_string(&Body{
            query, operation_name, variables
        }).unwrap()
    }

    async fn round_trip(
        query: &str,
        operation_name: Option<&str>,
        variables: serde_json::Value
        ) -> hyper::Response<String> {
        let req = hyper::Request::builder()
            .method("POST")
            .uri(uri())
            .body(hyper::Body::from(body(
                query,
                operation_name,
                variables
            ))).unwrap();
        let res : hyper::Response<hyper::Body> =  super::service()
                .call(req).await.unwrap();
        let (head, mut body) = res.into_parts();
        let mut buf : Vec<u8> = Vec::new();
        loop {
            match body.next().await {
                Some(Ok(data)) => buf.extend_from_slice(&*data),
                Some(Err(e)) => panic!["{}", e],
                None => break
            }
        }

        hyper::Response::from_parts(head, std::str::from_utf8(&buf).expect("Invalid utf8").to_string())
    }

    #[tokio::test]
    async fn it_accepts_an_int_parameter() {
        let res = round_trip(
            r#"
{
    echoInt(i: 1)
}
"#,
            None,
            json!(null)
        ).await;
        assert_eq!(res.body(), r#"{"data":{"echoInt":1}}"#);
    }

    #[tokio::test]
    async fn it_accepts_an_int_variables() {
        let res = round_trip(
            r#"
query x($i: Int!){
    echoInt(i: $i)
}
"#,
            Some("x"),
            json!({
                "i": 2
            })
        ).await;
        assert_eq!(res.body(), r#"{"data":{"echoInt":2}}"#);
    }

    #[tokio::test]
    async fn it_accepts_the_full_i64_variables() {
        let res = round_trip(
            r#"
query x($max: Int!, $min: Int!){
    max: echoInt(i: $max)
    min: echoInt(i: $min)
}
"#,
            Some("x"),
            json!({
                "min": std::i64::MIN,
                "max": std::i64::MAX
            })
        ).await;
        assert_eq!(res.body(), r#"{"data":{"max":9223372036854775807,"min":-9223372036854775808}}"#);
    }

    #[tokio::test]
    async fn it_accepts_a_float_parameter() {
        let res = round_trip(
            r#"
{
    echoFloat(f: 1.1)
}
"#,
            None,
            json!(null)
        ).await;
        assert_eq!(res.body(), r#"{"data":{"echoFloat":1.1}}"#);
    }

    #[tokio::test]
    async fn it_accepts_an_float_variables() {
        let res = round_trip(
            r#"
query x($f: Float!){
    echoFloat(f: $f)
}
"#,
            Some("x"),
            json!({
                "f": 2.2
            })
        ).await;
        assert_eq!(res.body(), r#"{"data":{"echoFloat":2.2}}"#);
    }

    #[tokio::test]
    async fn it_accepts_a_string_parameter() {
        let res = round_trip(
            r#"
{
    echoString(s: "1")
}
"#,
            None,
            json!(null)
        ).await;
        assert_eq!(res.body(), r#"{"data":{"echoString":"1"}}"#);
    }

    #[tokio::test]
    async fn it_accepts_a_string_variable() {
        let res = round_trip(
            r#"
query x($s: String!){
    echoString(s: $s)
}
"#,
            Some("x"),
            json!({
                "s": "2"
            })
        ).await;
        assert_eq!(res.body(), r#"{"data":{"echoString":"2"}}"#);
    }

    #[tokio::test]
    async fn it_accepts_a_list_parameter() {
        let res = round_trip(
            r#"
{
    echoList(s: ["1","2","3"])
}
"#,
            None,
            json!(null)
        ).await;
        assert_eq!(res.body(), r#"{"data":{"echoList":["1","2","3"]}}"#);
    }

    #[tokio::test]
    async fn it_accepts_a_list_variable() {
        let res = round_trip(
            r#"
query x($s: [String!]!){
    echoList(s: $s)
}
"#,
            Some("x"),
            json!({
                "s": ["1","2","3"]
            })
        ).await;
        assert_eq!(res.body(), r#"{"data":{"echoList":["1","2","3"]}}"#);
    }

    #[tokio::test]
    async fn it_denies_a_list_variable_containing_null() {
        let res = round_trip(
            r#"
query x($s: [String!]!){
    echoList(s: $s)
}
"#,
            Some("x"),
            json!({
                "s": ["1",null,"3"]
            })
        ).await;
        assert_eq!(res.body(), r#"{"errors":[{"message":"Variable s of type [String!]! was provided invalid value","locations":[{"line":2,"column":9}],"path":[1],"extensions":{"value":null}}]}"#);
    }

    #[tokio::test]
    async fn it_accepts_an_id_parameter() {
        let res = round_trip(
            r#"
{
    echoId(i: "1")
}
"#,
            None,
            json!(null)
        ).await;
        assert_eq!(res.body(), r#"{"data":{"echoId":"1"}}"#);
    }

    #[tokio::test]
    async fn it_accepts_an_id_variable() {
        let res = round_trip(
            r#"
query x($s: ID!){
    echoId(i: $s)
}
"#,
            Some("x"),
            json!({
                "s": "2"
            })
        ).await;
        assert_eq!(res.body(), r#"{"data":{"echoId":"2"}}"#);
    }

    #[tokio::test]
    async fn it_accepts_an_enum_parameter() {
        let res = round_trip(
            r#"
{
    echoEnum(e: "FOO")
}
"#,
            None,
            json!(null)
        ).await;
        assert_eq!(res.body(), r#"{"data":{"echoEnum":"FOO"}}"#);
    }

    #[tokio::test]
    async fn it_accepts_an_enum_variable() {
        let res = round_trip(
            r#"
query x($e: EchoEnum!){
    echoEnum(e: $e)
}
"#,
            Some("x"),
            json!({"e": "FOO"})
        ).await;
        assert_eq!(res.body(), r#"{"data":{"echoEnum":"FOO"}}"#);
    }


    #[tokio::test]
    async fn it_rejects_an_enum_parameter_with_an_invalid_value() {
        let res = round_trip(
            r#"
{
    echoEnum(e: "FUU")
}
"#,
            None,
            json!(null)
        ).await;
        assert_eq!(res.body(), r#"{"errors":[{"message":"Argument 'e' on Field 'echoEnum' has an invalid value. Expected type 'EchoEnum!'.","locations":[{"line":3,"column":5}],"path":["echoEnum"],"extensions":{"value":"FUU"}}]}"#);
    }

    #[tokio::test]
    async fn it_rejects_an_enum_variable_with_an_invalid_value() {
        let res = round_trip(
            r#"
query x($e: EchoEnum!){
    echoEnum(e: $e)
}
"#,
            Some("x"),
            json!({"e": "FUU"})
        ).await;
        assert_eq!(res.body(), r#"{"errors":[{"message":"Variable e of type EchoEnum! was provided invalid value","locations":[{"line":2,"column":9}],"extensions":{"value":"FUU"}}]}"#);
    }

    #[tokio::test]
    async fn it_accepts_an_input_object_parameter1() {
        let res = round_trip(
            r#"
{
    echoInputObject(o: {required:"a"}){
        with_default
        nullable
        required
    }
}
"#,
            None,
            json!(null)
        ).await;
        assert_eq!(res.body(), r#"{"data":{"echoInputObject":{"with_default":"z","nullable":null,"required":"a"}}}"#);
    }

    #[tokio::test]
    async fn it_accepts_an_input_object_parameter2() {
        let res = round_trip(
            r#"
{
    echoInputObject(o: {required:"a",nullable:null}){
        with_default
        nullable
        required
    }
}
"#,
            None,
            json!(null)
        ).await;
        assert_eq!(res.body(), r#"{"data":{"echoInputObject":{"with_default":"z","nullable":null,"required":"a"}}}"#);
    }

    #[tokio::test]
    async fn it_accepts_an_input_object_parameter3() {
        let res = round_trip(
            r#"
{
    echoInputObject(o: {required:"a", with_default: "b"}){
        with_default
        nullable
        required
    }
}
"#,
            None,
            json!(null)
        ).await;
        assert_eq!(res.body(), r#"{"data":{"echoInputObject":{"with_default":"b","nullable":null,"required":"a"}}}"#);
    }

    #[tokio::test]
    async fn it_accepts_an_input_object_variable1() {
        let res = round_trip(
            r#"
query x($o: AnInputObject!){
    echoInputObject(o: $o){
        with_default
        nullable
        required
    }
}
"#,
            Some("x"),
            json!({
                "o": {
                    "required": "a"
                }
            })
        ).await;
        assert_eq!(res.body(), r#"{"data":{"echoInputObject":{"with_default":"z","nullable":null,"required":"a"}}}"#);
    }

    #[tokio::test]
    async fn it_accepts_an_input_object_variable2() {
        let res = round_trip(
            r#"
query x($o: AnInputObject!){
    echoInputObject(o: $o){
        with_default
        nullable
        required
    }
}
"#,
            Some("x"),
            json!({
                "o": {
                    "required": "a",
                    "nullable": null
                }
            })
        ).await;
        assert_eq!(res.body(), r#"{"data":{"echoInputObject":{"with_default":"z","nullable":null,"required":"a"}}}"#);
    }

    #[tokio::test]
    async fn it_accepts_an_input_object_variable3() {
        let res = round_trip(
            r#"
query x($o: AnInputObject!){
    echoInputObject(o: $o){
        with_default
        nullable
        required
    }
}
"#,
            Some("x"),
            json!({
                "o": {
                    "required": "a",
                    "with_default": "b"
                }
            })
        ).await;
        assert_eq!(res.body(), r#"{"data":{"echoInputObject":{"with_default":"b","nullable":null,"required":"a"}}}"#);
    }
    #[tokio::test]
    async fn it_denies_an_invalid_input_object_parameter_when_missing_an_argument() {
        let res = round_trip(
            r#"
{
    echoInputObject(o: {}){
        with_default
        nullable
        required
    }
}
"#,
            None,
            json!(null)
        ).await;
        assert_eq!(res.body(), r#"{"errors":[{"message":"Argument 'required' on InputObject 'AnInputObject' is required.","locations":[{"line":3,"column":5}],"path":["echoInputObject"],"extensions":{"value":{}}}]}"#);
    }

    #[tokio::test]
    async fn it_denies_an_invalid_input_object_parameter_when_missing_an_argument_deep() {
        let res = round_trip(
            r#"
{
    this{
        echoInputObject(o: {}){
            with_default
        }
    }
}
"#,
            None,
            json!(null)
        ).await;
        assert_eq!(res.body(), r#"{"errors":[{"message":"Argument 'required' on InputObject 'AnInputObject' is required.","locations":[{"line":4,"column":9}],"path":["this","echoInputObject"],"extensions":{"value":{}}}]}"#);
    }

    #[tokio::test]
    async fn it_denies_an_invalid_nested_input_object_parameter_when_missing_an_argument() {
        let res = round_trip(
            r#"
{
    echoNestedInputObject(o: {inner: {}}){
        inner {
            with_default
        }
    }
}
"#,
            None,
            json!(null)
        ).await;
        assert_eq!(res.body(), r#"{"errors":[{"message":"Argument 'required' on InputObject 'AnInputObject' is required.","locations":[{"line":3,"column":5}],"path":["echoNestedInputObject","inner"],"extensions":{"value":{}}}]}"#);
    }

    #[tokio::test]
    async fn it_denies_an_invalid_input_object_parameter_when_null_for_nonnull() {
        let res = round_trip(
            r#"
{
    echoInputObject(o: {required: null}){
        with_default
        nullable
        required
    }
}
"#,
            None,
            json!(null)
        ).await;
        assert_eq!(res.body(), r#"{"errors":[{"message":"Argument 'required' on InputObject 'AnInputObject' has an invalid value. Expected type 'String!'.","locations":[{"line":3,"column":5}],"path":["echoInputObject","required"],"extensions":{"value":null}}]}"#);
    }

    #[tokio::test]
    async fn it_denies_an_invalid_input_object_parameter_when_int_for_string() {
        let res = round_trip(
            r#"
{
    echoInputObject(o: {required: 1}){
        with_default
        nullable
        required
    }
}
"#,
            None,
            json!(null)
        ).await;
        assert_eq!(res.body(), r#"{"errors":[{"message":"Argument 'required' on InputObject 'AnInputObject' has an invalid value. Expected type 'String!'.","locations":[{"line":3,"column":5}],"path":["echoInputObject","required"],"extensions":{"value":1}}]}"#);
    }

    #[tokio::test]
    async fn it_denies_an_invalid_input_object_parameter_when_int_for_string_deep() {
        let res = round_trip(
            r#"
{
    this {
        echoInputObject(o: {required: 1}){
            with_default
            nullable
            required
        }
    }
}
"#,
            None,
            json!(null)
        ).await;
        assert_eq!(res.body(), r#"{"errors":[{"message":"Argument 'required' on InputObject 'AnInputObject' has an invalid value. Expected type 'String!'.","locations":[{"line":4,"column":9}],"path":["this","echoInputObject","required"],"extensions":{"value":1}}]}"#);
    }

    #[tokio::test]
    async fn it_denies_a_variable_with_wrong_type() {
        let res = round_trip(
            r#"
query x($o: String) {
    echoInputObject(o: $o) {
        nullable
    }
}
"#,
            Some("x"),
            json!({
                "o": "1"
            })
        ).await;
        assert_eq!(res.body(), r#"{"errors":[{"message":"Type mismatch on variable $o and argument key (String / AnInputObject!)","locations":[{"line":3,"column":5}],"path":["echoInputObject"]}]}"#);
    }

    #[tokio::test]
    async fn it_accepts_a_variable_with_varying_nullability() {
        let res = round_trip(
            r#"
query x($s: String!) {
    echoInputObject(o: {required: "1", nullable: $s}) {
        nullable
    }
}
"#,
            Some("x"),
            json!({
                "s": "1"
            })
        ).await;
        assert_eq!(res.body(), r#"{"data":{"echoInputObject":{"nullable":"1"}}}"#);
    }

    #[tokio::test]
    async fn it_denies_a_variable_with_varying_nullability() {
        let res = round_trip(
            r#"
query x($s: String) {
    echoInputObject(o: {required: $s}) {
        nullable
    }
}
"#,
            Some("x"),
            json!({
                "s": "1"
            })
        ).await;
        assert_eq!(res.body(), r#"{"errors":[{"message":"Type mismatch on variable $s and argument key (String / String!)","locations":[{"line":3,"column":5}],"path":["echoInputObject","required"]}]}"#);
    }

    #[tokio::test]
    async fn it_denies_an_unknown_variable() {
        let res = round_trip(
            r#"
query x($s: String) {
    echoInputObject(o: {required: $p}) {
        nullable
    }
}
"#,
            Some("x"),
            json!({
                "s": "1"
            })
        ).await;
        assert_eq!(res.body(), r#"{"errors":[{"message":"Unknown variable p","locations":[{"line":3,"column":5}],"path":["echoInputObject","required"]}]}"#);
    }

    #[tokio::test]
    async fn it_denies_an_unknown_variable_types() {
        let res = round_trip(
            r#"
query x($s: Blubber) {
    echoInputObject(o: {required: $s}) {
        nullable
    }
}
"#,
            Some("x"),
            json!({
                "s": "1"
            })
        ).await;
        assert_eq!(res.body(), r#"{"errors":[{"message":"Blubber isn't a defined input type (on $s)","locations":[{"line":2,"column":9}]}]}"#);
    }

    #[tokio::test]
    async fn it_allows_issuing_an_error1() {
        let res = round_trip(
            r#"
{
    error
}
"#,
            None,
            json!(null)
        ).await;
        assert_eq!(res.body(), r#"{"data":{"error":null},"errors":[{"message":"No you don't!","path":["error"]}]}"#);
    }

    #[tokio::test]
    async fn it_allows_issuing_an_error2() {
        let res = round_trip(
            r#"
{
    error(questionMark: true)
}
"#,
            None,
            json!(null)
        ).await;
        assert_eq!(res.body(), r#"{"data":{"error":null},"errors":[{"message":"No you don't!","path":["error"]}]}"#);
    }

    #[tokio::test]
    async fn it_allows_issuing_a_fatal_error1() {
        let res = round_trip(
            r#"
{
    error(fatal: true)
}
"#,
            None,
            json!(null)
        ).await;
        assert_eq!(res.body(), r#"{"errors":[{"message":"No you don't!","path":["error"]}]}"#);
    }

    #[tokio::test]
    async fn it_allows_issuing_a_fatal_error2() {
        let res = round_trip(
            r#"
{
    error(questionMark: true, fatal: true)
}
"#,
            None,
            json!(null)
        ).await;
        assert_eq!(res.body(), r#"{"errors":[{"message":"No you don't!","path":["error"]}]}"#);
    }

    #[tokio::test]
    async fn it_allows_issuing_a_lazy_error1() {
        let res = round_trip(
            r#"
{
    error(lazy: true)
}
"#,
            None,
            json!(null)
        ).await;
        assert_eq!(res.body(), r#"{"data":{"error":null},"errors":[{"message":"No you don't!","path":["error"]}]}"#);
    }

    #[tokio::test]
    async fn it_allows_issuing_a_lazy_error2() {
        let res = round_trip(
            r#"
{
    error(lazy: true, fatal: true)
}
"#,
            None,
            json!(null)
        ).await;
        assert_eq!(res.body(), r#"{"errors":[{"message":"No you don't!","path":["error"]}]}"#);
    }

    #[tokio::test]
    async fn it_allows_issuing_a_lazy_list_error1() {
        let res = round_trip(
            r#"
{
    errorList(lazy: true)
}
"#,
            None,
            json!(null)
        ).await;
        assert_eq!(res.body(), r#"{"errors":[{"message":"No you don't!","path":["errorList"]}]}"#);
    }

    #[tokio::test]
    async fn it_has_correct_nesting() {
        let res = round_trip(
            r#"
{
    a: this {
        b: this {
            c: this(lazy: true) {
                d: this {
                    e: this(lazy: true) {
                       f: this(lazy: false) {
                         error(questionMark: true)
                       }
                    }
                }
            }
        }
    }
}
"#,
            None,
            json!(null)
        ).await;
        assert_eq!(res.body(), r#"{"data":{"a":{"b":{"c":{"d":{"e":{"f":{"error":null}}}}}}},"errors":[{"message":"No you don't!","path":["a","b","c","d","e","f","error"]}]}"#);
    }

    #[tokio::test]
    async fn it_has_correct_nesting2() {
        let res = round_trip(
            r#"
{
    a: this {
        b: this {
            c: this(lazy: true) {
                d: this {
                    e: this(lazy: true) {
                       f: this(lazy: true) {
                         error(questionMark: true)
                       }
                    }
                }
            }
        }
    }
}
"#,
            None,
            json!(null)
        ).await;
        assert_eq!(res.body(), r#"{"data":{"a":{"b":{"c":{"d":{"e":{"f":{"error":null}}}}}}},"errors":[{"message":"No you don't!","path":["a","b","c","d","e","f","error"]}]}"#);
    }

    #[tokio::test]
    async fn it_has_correct_nesting3() {
        let res = round_trip(
            r#"
{
    a: this {
        b: this {
            c: this(lazy: true) {
                d: this {
                    e: this(lazy: true) {
                       f: this(lazy: false) {
                         error(questionMark: true, fatal: true)
                       }
                    }
                }
            }
        }
    }
}
"#,
            None,
            json!(null)
        ).await;
        assert_eq!(res.body(), r#"{"errors":[{"message":"No you don't!","path":["a","b","c","d","e","f","error"]}]}"#);
    }

    #[tokio::test]
    async fn it_has_correct_nesting4() {
        let res = round_trip(
            r#"
{
    a: this {
        b: this {
            c: this(lazy: true) {
                d: this {
                    e: this(lazy: true) {
                       f: this(lazy: true) {
                         error(questionMark: true, fatal: true)
                       }
                    }
                }
            }
        }
    }
}
"#,
            None,
            json!(null)
        ).await;
        assert_eq!(res.body(), r#"{"errors":[{"message":"No you don't!","path":["a","b","c","d","e","f","error"]}]}"#);
    }
}
