use std::fs::File;
use std::io::Read;

fn main() {
    let mut content = String::new();
    let mut file = File::open("schema.graphql").unwrap();
    file.read_to_string(&mut content).unwrap();
    print!["{}",earl_codegen::generate(&*content).unwrap()];
}
