use case::CaseExt;
use graphql_parser::{query, schema};
use std::collections::hash_map::HashMap;
//use bytes::BufMut;

#[derive(Debug)]
pub struct UnknownValueError {
    typ: graphql_parser::schema::Type<'static, String>,
}

impl UnknownValueError {
    fn of<'a,T>(typ: &schema::Type<'a,T>) -> Self
where T: schema::Text<'a> {
        UnknownValueError{
            typ: static_type(typ)
        }
    }
}

#[derive(Debug)]
pub struct InvalidValueError {
    value: query::Value<'static, String>,
}

fn static_type<'a,T>(typ: &schema::Type<'a,T>) -> schema::Type<'static,String>
where T: schema::Text<'a>
{
    match typ {
        schema::Type::NamedType(t) =>
            schema::Type::NamedType(t.as_ref().to_owned()),
        schema::Type::ListType(b) => 
            schema::Type::ListType(Box::new(static_type(b.as_ref()))),
        schema::Type::NonNullType(b) => 
            schema::Type::NonNullType(Box::new(static_type(b.as_ref()))),
    }
}

fn static_value<'a,T>(value: &query::Value<'a,T>) -> query::Value<'static,String>
where T: graphql_parser::schema::Text<'a> {
    match value {
        query::Value::Enum(t) => {
            query::Value::Enum(t.as_ref().to_owned())
        },
        query::Value::Variable(t) => {
            query::Value::Variable(t.as_ref().to_owned())
        },
        query::Value::List(l) => {
            query::Value::List(l.iter().map(static_value).collect())
        },
        query::Value::Object(o) => {
            query::Value::Object(o.iter().map(|(k,v)| (k.as_ref().to_owned(),static_value(v))).collect())
        },
        query::Value::Boolean(b) =>
            query::Value::Boolean(*b),
        query::Value::Float(f) =>
            query::Value::Float(*f),
        query::Value::Int(i) =>
            query::Value::Int(i.clone()),
        query::Value::String(s) =>
            query::Value::String(s.clone()),
        query::Value::Null =>
            query::Value::Null
    }
}

impl InvalidValueError {
    fn of<'a,T>(value: &query::Value<'a,T>) -> Self where T: graphql_parser::schema::Text<'a> {
        InvalidValueError{
            value: static_value(value)
        }
    }
}

#[derive(Debug)]
pub enum Error {
    ParseError(schema::ParseError),
    UnknownType {
        cause: UnknownValueError,
        pos: graphql_parser::Pos,
    },
    UnknownBaseType,
    CodeGenerationError(std::fmt::Error),
    InvalidValue {
        cause: InvalidValueError,
        pos: graphql_parser::Pos,
    },
    NotAnInputType {
        name: String,
    },
}

impl std::fmt::Display for Error {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self, f)
    }
}

impl std::error::Error for Error {}

impl From<schema::ParseError> for Error {
    fn from(other: schema::ParseError) -> Self {
        Error::ParseError(other)
    }
}

struct BaseType<'a, T: graphql_parser::query::Text<'a>> {
    pub name: T::Value,
    pub position: graphql_parser::Pos,
    pub module_name: String,
    pub rust_type: String,
    pub is_input: bool,
    has_projection: bool,
    definition: graphql_parser::schema::TypeDefinition<'a, T>,
}

struct Schema<'a, T: graphql_parser::query::Text<'a>>
    where T::Value: std::hash::Hash
{
    types: HashMap<T::Value, BaseType<'a,T>>,
    implementations: HashMap<T::Value, Vec<T::Value>>,
    interfaces: HashMap<T::Value, Vec<T::Value>>,
}

impl<'a, T> Schema<'a,T>
    where 
        T: graphql_parser::query::Text<'a> + std::clone::Clone,
        T::Value: std::hash::Hash + std::clone::Clone
    {
    fn new(doc: &schema::Document<'a,T>) -> Self {
        Schema {
            types: generate_types(doc),
            implementations: generate_implementations(doc),
            interfaces: generate_interfaces(doc),
        }
    }

    fn typ<'schema>(
        &'schema self,
        typ: &graphql_parser::query::Type<'a,T>,
    ) -> Result<Type<'schema,'a,T>, UnknownValueError> {
        match typ {
            query::Type::NamedType(type_name) => Ok(Type {
                base: self
                    .types
                    .get(type_name.as_ref())
                    .ok_or_else(|| UnknownValueError::of(typ))?,
                value_type: ValueType::Optional,
            }),
            query::Type::NonNullType(non_null_type) => match &**non_null_type {
                query::Type::NamedType(type_name) => Ok(Type {
                    base: self
                        .types
                        .get(type_name.as_ref())
                        .ok_or_else(|| UnknownValueError::of(typ))?,
                    value_type: ValueType::Default,
                }),
                query::Type::ListType(inner) => match &**inner {
                    query::Type::NamedType(type_name) => Ok(Type {
                        base: self
                            .types
                            .get(type_name.as_ref())
                            .ok_or_else(|| UnknownValueError::of(typ))?,
                        value_type: ValueType::ListOptional,
                    }),
                    query::Type::NonNullType(non_null_type) => match &**non_null_type {
                        query::Type::NamedType(type_name) => Ok(Type {
                            base: self
                                .types
                                .get(type_name.as_ref())
                                .ok_or_else(|| UnknownValueError::of(typ))?,
                            value_type: ValueType::List,
                        }),
                        _ => Err(UnknownValueError::of(typ)),
                    },
                    _ => Err(UnknownValueError::of(typ)),
                },
                _ => Err(UnknownValueError::of(typ)),
            },
            query::Type::ListType(inner) => match &**inner {
                query::Type::NamedType(type_name) => Ok(Type {
                    base: self
                        .types
                        .get(type_name.as_ref())
                        .ok_or_else(|| UnknownValueError::of(typ))?,
                    value_type: ValueType::OptionalListOptional,
                }),
                query::Type::NonNullType(non_null_type) => match &**non_null_type {
                    query::Type::NamedType(type_name) => Ok(Type {
                        base: self
                            .types
                            .get(type_name.as_ref())
                            .ok_or_else(|| UnknownValueError::of(typ))?,
                        value_type: ValueType::OptionalList,
                    }),
                    _ => Err(UnknownValueError::of(typ)),
                },
                _ => Err(UnknownValueError::of(typ)),
            },
        }
    }

    fn implementations<'schema>(&'schema self, typ: &str) -> Result<Vec<&'schema BaseType<'a,T>>, Error> {
        self.implementations
            .get(typ)
            .ok_or(Error::UnknownBaseType)?
            .iter()
            .map(|name| self.types.get(name.as_ref()).ok_or(Error::UnknownBaseType))
            .collect()
    }

    fn interfaces<'schema>(
        &'schema self,
        interfaces: &Vec<T::Value>,
    ) -> Result<Vec<(&'schema BaseType<'a,T>, &'schema schema::InterfaceType<'a,T>)>, Error> {
        interfaces
            .iter()
            .map(|iface| {
                let typ = &self.types[iface.as_ref()];
                if let schema::TypeDefinition::Interface(idef) = &typ.definition {
                    return Ok((typ, idef));
                } else {
                    return Err(Error::UnknownBaseType);
                }
            })
            .collect()
    }
}
fn generate_implementations<'a,T>(doc: &schema::Document<'a,T>) -> HashMap<T::Value, Vec<T::Value>>
    where T: graphql_parser::schema::Text<'a>,
          T::Value: std::hash::Hash
{
    let mut map: HashMap<T::Value, Vec<T::Value>> = HashMap::new();
    for def in doc.definitions.iter() {
        if let schema::Definition::TypeDefinition(schema::TypeDefinition::Object(ob)) = def {
            for name in ob.implements_interfaces.iter() {
                map.entry(name.clone())
                    .or_default()
                    .push(ob.name.clone());
            }
        }
    }
    return map;
}

fn generate_interfaces<'a,T>(doc: &schema::Document<'a,T>) -> HashMap<T::Value, Vec<T::Value>>
    where T: graphql_parser::schema::Text<'a>,
          T::Value: std::hash::Hash
{
    let mut map: HashMap<T::Value, Vec<T::Value>> = HashMap::new();
    for def in doc.definitions.iter() {
        if let schema::Definition::TypeDefinition(schema::TypeDefinition::Object(ob)) = def {
            let entry = map.entry(ob.name.clone()).or_default();
            entry.push(ob.name.clone());
            entry.extend(ob.implements_interfaces.iter().map(|ifs| ifs.clone()));
        }
    }
    return map;
}

fn generate_types<'a,T>(doc: &schema::Document<'a,T>) -> HashMap<T::Value, BaseType<'a,T>>
    where T: graphql_parser::schema::Text<'a>,
          T: std::clone::Clone,
          T::Value: std::hash::Hash + std::clone::Clone
{
    let mut map : HashMap<T::Value, BaseType<'a,T>> = HashMap::new();
    map.insert(
        T::Value::from("String"),
        BaseType {
            name: T::Value::from("String"),
            position: graphql_parser::Pos { column: 0, line: 0 },
            module_name: "::earl::string".to_owned(),
            has_projection: false,
            rust_type: "String".to_owned(),
            is_input: true,
            definition: graphql_parser::schema::TypeDefinition::Scalar(
                graphql_parser::schema::ScalarType {
                    position: graphql_parser::Pos { line: 0, column: 0 },
                    description: None,
                    name: T::Value::from("String"),
                    directives: Vec::new(),
                },
            ),
        },
    );
    map.insert(
        T::Value::from("Int"),
        BaseType {
            name: T::Value::from("Int"),
            position: graphql_parser::Pos { column: 0, line: 0 },
            module_name: "::earl::int".to_owned(),
            has_projection: false,
            rust_type: "i64".to_owned(),
            is_input: true,
            definition: graphql_parser::schema::TypeDefinition::Scalar(
                graphql_parser::schema::ScalarType {
                    position: graphql_parser::Pos { line: 0, column: 0 },
                    description: None,
                    name: T::Value::from("Int"),
                    directives: Vec::new(),
                },
            ),
        },
    );
    map.insert(
        T::Value::from("Boolean"),
        BaseType {
            name: T::Value::from("Boolean"),
            position: graphql_parser::Pos { column: 0, line: 0 },
            module_name: "::earl::boolean".to_owned(),
            has_projection: false,
            rust_type: "bool".to_owned(),
            is_input: true,
            definition: graphql_parser::schema::TypeDefinition::Scalar(
                graphql_parser::schema::ScalarType {
                    position: graphql_parser::Pos { line: 0, column: 0 },
                    description: None,
                    name: T::Value::from("Boolean"),
                    directives: Vec::new(),
                },
            ),
        },
    );
    map.insert(
        T::Value::from("Float"),
        BaseType {
            name: T::Value::from("Float"),
            position: graphql_parser::Pos { column: 0, line: 0 },
            module_name: "::earl::float".to_owned(),
            has_projection: false,
            rust_type: "f64".to_owned(),
            is_input: true,
            definition: graphql_parser::schema::TypeDefinition::Scalar(
                graphql_parser::schema::ScalarType {
                    position: graphql_parser::Pos { line: 0, column: 0 },
                    description: None,
                    name: T::Value::from("Float"),
                    directives: Vec::new(),
                },
            ),
        },
    );
    map.insert(
        T::Value::from("ID"),
        BaseType {
            name: T::Value::from("ID"),
            position: graphql_parser::Pos { column: 0, line: 0 },
            module_name: "::earl::id".to_owned(),
            has_projection: false,
            rust_type: "::earl::ID".to_owned(),
            is_input: true,
            definition: graphql_parser::schema::TypeDefinition::Scalar(
                graphql_parser::schema::ScalarType {
                    position: graphql_parser::Pos { line: 0, column: 0 },
                    description: None,
                    name: T::Value::from("ID"),
                    directives: Vec::new(),
                },
            ),
        },
    );
    for def in doc.definitions.iter() {
        match def {
            schema::Definition::TypeDefinition(td) => {
                match td {
                    schema::TypeDefinition::Scalar(sc) => {
                        map.insert(
                            sc.name.clone(),
                            BaseType {
                                name: sc.name.clone(),
                                position: sc.position,
                                module_name: format!["super::r#{}", sc.name.as_ref().to_snake()],
                                has_projection: false,
                                rust_type: format!["super::{}", sc.name.as_ref()],
                                is_input: true,
                                definition: td.clone(),
                            },
                        );
                    }
                    schema::TypeDefinition::Object(ob) => {
                        map.insert(
                            ob.name.clone(),
                            BaseType {
                                name: ob.name.clone(),
                                position: ob.position,
                                module_name: format!["super::r#{}", ob.name.as_ref().to_snake()],
                                has_projection: true,
                                rust_type: format![
                                    "dyn super::r#{}::Instance<Ctx>",
                                    ob.name.as_ref().to_snake()
                                ],
                                is_input: false,
                                definition: td.clone(),
                            },
                        );
                    }
                    schema::TypeDefinition::Interface(int) => {
                        map.insert(
                            int.name.clone(),
                            BaseType {
                                name: int.name.clone(),
                                position: int.position,
                                module_name: format!["super::r#{}", int.name.as_ref().to_snake()],
                                has_projection: true,
                                rust_type: format![
                                    "dyn super::r#{}::Instance<Ctx>",
                                    int.name.as_ref().to_snake()
                                ],
                                is_input: false,
                                definition: td.clone(),
                            },
                        );
                    }
                    /*
                    schema::TypeDefinition::Union(un) => &un.name,
                    */
                    schema::TypeDefinition::Enum(en) => {
                        map.insert(
                            en.name.clone(),
                            BaseType {
                                name: en.name.clone(),
                                position: en.position,
                                module_name: format!["super::r#{}", en.name.as_ref().to_snake()],
                                has_projection: false,
                                rust_type: format!["super::r#{}", en.name.as_ref()],
                                is_input: true,
                                definition: td.clone(),
                            },
                        );
                    }
                    schema::TypeDefinition::InputObject(ob) => {
                        map.insert(
                            ob.name.clone(),
                            BaseType {
                                name: ob.name.clone(),
                                position: ob.position,
                                module_name: "super".to_string(),
                                has_projection: true,
                                rust_type: format!["super::r#{}", ob.name.as_ref().to_string()],
                                is_input: true,
                                definition: td.clone(),
                            },
                        );
                    }
                    _ => panic!["unimplemented!"],
                };
            }
            _ => {}
        }
    }
    return map;
}

#[derive(Debug, Eq, PartialEq)]
enum ValueType {
    Default,
    Optional,
    OptionalList,
    OptionalListOptional,
    ListOptional,
    List,
}

macro_rules! push {
     ($buf:expr, $x:expr) => (
        $buf.extend_from_slice($x.as_bytes());
     );
     ($buf:expr, $x:expr, $($y:expr),+) => (
        $buf.extend_from_slice($x.as_bytes());
        push!($buf, $($y),+)
    )
}

struct Type<'schema,'a,T> where T: schema::Text<'a> {
    base: &'schema BaseType<'a,T>,
    value_type: ValueType,
}

impl<'schema,'a,T> Type<'schema,'a,T> where
    T: graphql_parser::schema::Text<'a>
{
    fn input(&self) -> String {
        match self.value_type {
            ValueType::Default => format!["{}", self.base.rust_type],
            ValueType::Optional => format!["Option<{}>", self.base.rust_type],
            ValueType::ListOptional => format!["Vec<Option<{}>>", self.base.rust_type],
            ValueType::List => format!["Vec<{}>", self.base.rust_type],
            ValueType::OptionalListOptional => {
                format!["Option<Vec<Option<{}>>>", self.base.rust_type]
            }
            ValueType::OptionalList => format!["Option<Vec<{}>>", self.base.rust_type],
        }
    }

    fn is_optional(&self) -> bool {
        match self.value_type {
            ValueType::Optional | ValueType::OptionalListOptional | ValueType::OptionalList => true,
            _ => false,
        }
    }

    fn responder(&self) -> String {
        format!["{}::{}", self.base.module_name, self.value_type.responder()]
    }

    fn responder_type(&self) -> String {
        format![
            "{}::{}<Ctx>",
            self.base.module_name,
            self.value_type.responder()
        ]
    }

    fn graphql_type(&self) -> String {
        match self.value_type {
            ValueType::Default => format!["{}!", self.base.name.as_ref()],
            ValueType::Optional => format!["{}", self.base.name.as_ref()],
            ValueType::ListOptional => format!["[{}]!", self.base.name.as_ref()],
            ValueType::List => format!["[{}!]!", self.base.name.as_ref()],
            ValueType::OptionalListOptional => format!["[{}]", self.base.name.as_ref()],
            ValueType::OptionalList => format!["[{}!]", self.base.name.as_ref()],
        }
    }

    fn print_value(&self, value: &query::Value<'a,T>) -> Result<String, InvalidValueError> {
        fn print_base_value<'a,T>(
            base: &BaseType<'a,T>,
            value: &query::Value<'a,T>,
        ) -> Result<String, InvalidValueError> 
            where T: graphql_parser::schema::Text<'a>
        {
            match base.name.as_ref() {
                "String" => match value {
                    query::Value::String(s) => Ok(format!["{:?}.to_string()", s]),
                    _ => Err(InvalidValueError::of(value)),
                },
                "Int" => match value {
                    query::Value::Int(i) => Ok(format![
                        "{:?}",
                        i.as_i64().ok_or_else(|| InvalidValueError::of(value))?
                    ]),
                    _ => Err(InvalidValueError::of(value)),
                },
                "Float" => match value {
                    query::Value::Float(i) => Ok(format!["{:?}", i]),
                    _ => Err(InvalidValueError::of(value)),
                },
                "Boolean" => match value {
                    query::Value::Boolean(b) => Ok(format!["{:?}", b]),
                    _ => Err(InvalidValueError::of(value)),
                },
                _ => Err(InvalidValueError::of(value)),
            }
        }
        if self.base.has_projection {
            return Err(InvalidValueError::of(value));
            //return Err(Error::NotAnInputType{ name: self.base.name.clone() })
        }
        return match self.value_type {
            ValueType::Default => print_base_value(&self.base, value),
            ValueType::Optional => match value {
                query::Value::Null => Ok("None".to_string()),
                _ => print_base_value(&self.base, value).map(|s| format!["Some({})", s]),
            },
            ValueType::List => match value {
                query::Value::List(list) => list
                    .iter()
                    .map(|elem| print_base_value(&self.base, elem))
                    .collect::<Result<Vec<_>, _>>()
                    .map(|v| format!["vec![{}]", v.join(",")]),
                _ => Err(InvalidValueError::of(value)),
            },
            ValueType::OptionalList => match value {
                query::Value::Null => Ok("None".to_string()),
                query::Value::List(list) => list
                    .iter()
                    .map(|elem| print_base_value(&self.base, elem))
                    .collect::<Result<Vec<_>, _>>()
                    .map(|v| format!["Some(vec![{}])", v.join(",")]),
                _ => Err(InvalidValueError::of(value)),
            },
            _ => panic!["unimplemented!"],
        };
    }
}

impl ValueType {
    fn responder(&self) -> &'static str {
        match self {
            ValueType::Default => "Responder",
            ValueType::Optional => "OptionalResponder",
            ValueType::ListOptional => "ListOptionalResponder",
            ValueType::List => "ListResponder",
            ValueType::OptionalListOptional => "OptionalListOptionalResponder",
            ValueType::OptionalList => "OptionalListResponder",
        }
    }

    fn is_optional(&self) -> bool {
        match self {
            ValueType::Optional | ValueType::OptionalListOptional | ValueType::OptionalList => true,
            _ => false,
        }
    }
}

pub fn generate(input: &str) -> Result<String, Error> {
    let doc = graphql_parser::parse_schema::<&str>(input)?;
    let schema = Schema::new(&doc);
    let mut output: Vec<u8> = Vec::new();
    //push!(output, "#![allow(non_snake_case)]\n#![allow(non_camel_case_types)]\n\n");
    push!(output, "#[derive(Hash,Eq,PartialEq,Copy,Clone,Debug)]\n");
    push!(output, "enum __Symbol {\n");
    push!(
        output,
        schema
            .types
            .keys()
            .map(|s| s.as_ref())
            .collect::<Vec<_>>()
            .join(",\n")
    );
    push!(output, "}\n");
    push!(output, "impl earl::Symbol for __Symbol {\n");
    push!(output, "  fn parse(name: &str) -> Option<Self> {\n");
    push!(output, "    match name {\n");
    for key in schema.types.keys() {
        push!(output, "    \"", key, "\" => Some(__Symbol::", key, "),\n");
    }
    push!(output, "    _ => None\n");
    push!(output, "    }\n");
    push!(output, "  }\n");
    push!(output, "  fn implementations(&self) -> &'static [Self] {\n");
    push!(output, "    match self {\n");
    for (key, value) in schema.implementations.iter() {
        push!(
            output,
            "    __Symbol::",
            key,
            " => &[",
            value
                .iter()
                .map(|imp| format!["__Symbol::{}", imp])
                .collect::<Vec<_>>()
                .join(","),
            "],\n"
        );
    }
    push!(output, "    _ => &[]\n");
    push!(output, "    }\n");
    push!(output, "  }\n");
    push!(output, "  fn interfaces(&self) -> &'static [Self] {\n");
    push!(output, "    match self {\n");
    for (key, value) in schema.interfaces.iter() {
        push!(
            output,
            "    __Symbol::",
            key,
            " => &[",
            value
                .iter()
                .map(|imp| format!["__Symbol::{}", imp])
                .collect::<Vec<_>>()
                .join(","),
            "],\n"
        );
    }
    push!(output, "    _ => &[]\n");
    push!(output, "    }\n");
    push!(output, "  }\n");
    push!(output, "}\n");
    for def in doc.definitions.iter() {
        match def {
            schema::Definition::TypeDefinition(td) => {
                match td {
                    schema::TypeDefinition::Scalar(scalar) => {
                        let name = &scalar.name;
                        push!(output, "pub mod ", &name.to_snake(), "{\n");
                        push!(output, "  pub struct Responder<'frame,Ctx> {\n");
                        push!(
                            output,
                            "    pub buf: &'frame mut ::earl::internal::ForkBuf,\n"
                        );
                        push!(output, "    pub context: &'frame Ctx,\n");
                        push!(output, "  }\n");
                        push!(output, "  pub struct OptionalResponder<'frame,Ctx> {\n");
                        push!(
                            output,
                            "    pub buf: &'frame mut ::earl::internal::ForkBuf,\n"
                        );
                        push!(output, "    pub context: &'frame Ctx,\n");
                        push!(output, "  }\n");
                        push!(output, "  pub struct ListResponder<'frame,Ctx> {\n");
                        push!(
                            output,
                            "    pub buf: &'frame mut ::earl::internal::ForkBuf,\n"
                        );
                        push!(output, "    pub context: &'frame Ctx,\n");
                        push!(output, "  }\n");
                        push!(output, "  impl<'frame,Ctx: 'static + Send + Clone> Responder<'frame,Ctx> where super::",name,": earl::Scalar {\n");
                        push!(
                            output,
                            "    pub fn ok(self, value: &super::",
                            name,
                            ") -> earl::Ack {\n"
                        );
                        push!(output, "      let Responder{ buf, .. } = self;\n");
                        push!(output, "      earl::Scalar::write_value(value, earl::ScalarWriter::new(buf))?;\n");
                        push!(output, "      earl::ack::new()\n");
                        push!(output, "    }\n");
                        push!(output, "    pub fn context(&self) -> &Ctx {\n");
                        push!(output, "      self.context\n");
                        push!(output, "    }\n");
                        push!(output, "  }\n");
                        push!(output, "  impl<'frame,Ctx: 'static + Send + Clone> OptionalResponder<'frame,Ctx> where super::",name,": earl::Scalar {\n");
                        push!(
                            output,
                            "    pub fn ok(self, value: Option<&super::",
                            name,
                            ">) -> earl::Ack {\n"
                        );
                        push!(output, "      let OptionalResponder{ buf, .. } = self;\n");
                        push!(output, "      if let Some(v) = value {\n");
                        push!(output, "        earl::Scalar::write_value(v, earl::ScalarWriter::new(buf))?;\n");
                        push!(output, "      } else {\n");
                        push!(output, "        buf.put(\"null\".as_bytes());\n");
                        push!(output, "      }\n");
                        push!(output, "      earl::ack::new()\n");
                        push!(output, "    }\n");
                        push!(output, "    pub fn context(&self) -> &Ctx {\n");
                        push!(output, "      self.context\n");
                        push!(output, "    }\n");
                        push!(output, "  }\n");
                        push!(output, "  impl<'frame,Ctx: 'static + Send + Clone> ListResponder<'frame,Ctx> where super::",name,": earl::Scalar {\n");
                        push!(
                            output,
                            "    pub fn ok<T>(self, value: &[super::",
                            name,
                            "]) -> earl::Ack where T: std::borrow::Borrow<super::",
                            name,
                            "> {\n"
                        );
                        push!(output, "      let ListResponder{ buf, .. } = self;\n");
                        push!(
                            output,
                            "      let mut first = true;
                buf.start_array();
                for inner in value.iter() {
                    if !first {
                        buf.next_element();
                    } else {
                        first = false;
                    }
                    let value = std::borrow::Borrow::borrow(inner);
                    earl::Scalar::write_value(value, earl::ScalarWriter::new(buf))?;
                }
                buf.end_array();\n"
                        );
                        push!(output, "      earl::ack::new()\n");
                        push!(output, "    }\n");
                        push!(output, "    pub fn context(&self) -> &Ctx {\n");
                        push!(output, "      self.context\n");
                        push!(output, "    }\n");
                        push!(output, "  }\n");
                        push!(output, "}\n");
                        push!(
                            output,
                            "impl earl::FromValue for ",
                            name,
                            " where ",
                            name,
                            ": earl::Scalar {\n"
                        );
                        push!(output, "  fn from_value(value: &earl::internal::graphql_parser::query::Value<'static,String>, variables: &earl::Variables) -> Result<Self,earl::Error> {\n");
                        push!(output, "    <Self as earl::Scalar>::read_value(variables.resolve::<Self>(value)?)\n");
                        push!(output, "  }\n");
                        push!(output, "  fn is_assignable_from(typ: &earl::internal::graphql_parser::query::Type) -> bool {\n");
                        push!(
                            output,
                            "        match typ {
            earl::internal::graphql_parser::query::Type::NonNullType(inner) => {
                match &**inner {
                    earl::internal::graphql_parser::query::Type::NamedType(name) => {
                        name == \"",
                            name,
                            "\"
                    },
                    _ => false
                }
            },
            _ => false
        }\n"
                        );
                        push!(output, "  }\n");
                        push!(output, "  fn graphql_type() -> earl::internal::graphql_parser::query::Type<'static,String> {\n");
                        push!(output, "    earl::internal::graphql_parser::query::Type::NonNullType(Box::new(earl::internal::graphql_parser::query::Type::NamedType(\"",name,"\".to_string())))\n");
                        push!(output, "  }\n");
                        push!(output, "}");
                    }
                    schema::TypeDefinition::InputObject(object) => {
                        let name = &object.name;
                        push!(output, "mod ", &object.name.to_snake(), "{\n");
                        push!(output, "#[derive(Clone,Debug,PartialEq)]\n");
                        push!(output, "pub struct r#", name, "{\n");
                        for field in object.fields.iter() {
                            let typ =
                                schema
                                    .typ(&field.value_type)
                                    .map_err(|e| Error::UnknownType {
                                        cause: e,
                                        pos: field.position.clone(),
                                    })?;
                            push!(output, "  pub ", &field.name, ": ", typ.input(), ",\n");
                        }
                        push!(output, "}\n");
                        push!(output, "impl earl::FromValue for ", name, "{\n");
                        push!(output, "  fn from_value(value: &earl::internal::graphql_parser::query::Value<'static,String>, variables: &earl::Variables) -> Result<Self,earl::Error> {\n");
                        push!(output, "    match variables.resolve::<Self>(value)? {\n");
                        push!(output, "      earl::internal::graphql_parser::query::Value::Object(object) => {\n");
                        // TODO: validate that there are no additional keys
                        push!(output, "        Ok(Self{\n");
                        for field in object.fields.iter() {
                            let typ =
                                schema
                                    .typ(&field.value_type)
                                    .map_err(|e| Error::UnknownType {
                                        cause: e,
                                        pos: field.position.clone(),
                                    })?;
                            push!(output, "          ", &field.name, ": ");
                            push!(output, "object.get(\"", &field.name,"\").map(|value| <", typ.input(), " as earl::FromValue>::from_value(value, variables).map_err(|err| err.with_fatal().or_with_source(|| Box::new(earl::QueryError::IncompatibledArgumentForInputObject{name: \"",&field.name,"\", on_type: \"",name,"\", expected_type: \"",typ.graphql_type(),"\"})).at_field(\"",&field.name,"\".to_string()) ) )");

                            if let Some(default) = &field.default_value {
                                push!(
                                    output,
                                    ".unwrap_or_else(|| Ok(",
                                    typ.print_value(&default).map_err(|e| Error::InvalidValue {
                                        cause: e,
                                        pos: field.position.clone()
                                    })?,
                                    ") )?,\n"
                                );
                            } else if typ.value_type.is_optional() {
                                push!(output, ".unwrap_or(Ok(None))?,\n");
                            } else {
                                push!(output, ".unwrap_or_else(|| Err(earl::Error::new(earl::ErrorKind::Query).with_fatal().with_source(Box::new(earl::QueryError::MissingArgumentForInputObject{name: \"",&field.name,"\", on_type: \"",name,"\"})).with_extension(earl::extensions::VariableValue(value.clone()))))?,\n");
                            }
                        }
                        push!(output, "        })\n");
                        push!(output, "      },\n");
                        push!(output, "      _ => Err(earl::Error::new(earl::ErrorKind::Query).with_fatal())\n");
                        push!(output, "    }\n");
                        push!(output, "  }\n");
                        push!(output, "  fn is_assignable_from(typ: &earl::internal::graphql_parser::query::Type<'static,String>) -> bool {\n");
                        push!(
                            output,
                            "        match typ {
            earl::internal::graphql_parser::query::Type::NonNullType(inner) => {
                match &**inner {
                    earl::internal::graphql_parser::query::Type::NamedType(name) => {
                        name == \"",
                            name,
                            "\"
                    },
                    _ => false
                }
            },
            _ => false
        }"
                        );
                        push!(output, "  }\n");
                        push!(output, "  fn graphql_type() -> earl::internal::graphql_parser::query::Type<'static,String> {\n");
                        push!(output, "    earl::internal::graphql_parser::query::Type::NonNullType(Box::new(earl::internal::graphql_parser::query::Type::NamedType(\"",name,"\".to_string())))\n");
                        push!(output, "  }\n");
                        push!(output, "}\n");
                        push!(output, "}\n");
                        push!(
                            output,
                            "pub use ",
                            &object.name.to_snake(),
                            "::",
                            name,
                            ";\n"
                        );
                    }
                    schema::TypeDefinition::Enum(en) => {
                        let name = &en.name.to_snake();
                        push!(output, "#[derive(Clone,PartialEq,Debug)]\n");
                        push!(output, "pub enum r#", &en.name, "{\n");
                        for value in en.values.iter() {
                            push!(output, "  r#", &value.name, ",\n");
                        }
                        push!(output, "}\n");
                        push!(output, "pub mod r#", name, " {\n");
                        push!(output, "  pub struct Responder<'frame,Ctx> {\n");
                        push!(
                            output,
                            "    pub buf: &'frame mut ::earl::internal::ForkBuf,\n"
                        );
                        push!(output, "    pub context: &'frame Ctx,\n");
                        push!(output, "  }\n");
                        push!(output, "  pub struct ListResponder<'frame,Ctx> {\n");
                        push!(
                            output,
                            "    pub buf: &'frame mut ::earl::internal::ForkBuf,\n"
                        );
                        push!(output, "    pub context: &'frame Ctx,\n");
                        push!(output, "  }\n");
                        push!(
                            output,
                            "  impl<'frame,Ctx: 'static + Send + Clone> Responder<'frame,Ctx> {\n"
                        );
                        push!(
                            output,
                            "    pub fn ok(self, value: &super::",
                            &en.name,
                            ") -> earl::Ack {\n"
                        );
                        push!(output, "      let Responder{ buf, .. } = self;\n");
                        push!(
                            output,
                            "      buf.put(format![\"\\\"{:?}\\\"\", value].as_bytes());\n"
                        );
                        push!(output, "      earl::ack::new()\n");
                        push!(output, "    }\n");
                        push!(output, "    pub fn context(&self) -> &Ctx {\n");
                        push!(output, "      self.context\n");
                        push!(output, "    }\n");
                        push!(output, "  }\n");
                        push!(output, "}\n");
                        push!(output, "impl earl::FromValue for r#", &en.name, "{\n");
                        push!(output, "  fn from_value(value: &earl::internal::graphql_parser::query::Value<'static,String>, variables: &earl::Variables) -> Result<Self, earl::Error> {\n");
                        push!(output, "    match variables.resolve::<Self>(value)? {\n");
                        push!(output, "      earl::internal::graphql_parser::query::Value::String(s) => match &**s {\n");
                        for value in en.values.iter() {
                            push!(
                                output,
                                "        \"",
                                &value.name,
                                "\" => Ok(r#",
                                &en.name,
                                "::r#",
                                &value.name,
                                "),\n"
                            );
                        }
                        push!(output, "        elze => Err(earl::Error::new(earl::ErrorKind::Query).with_fatal().with_extension(earl::extensions::VariableValue(earl::internal::graphql_parser::query::Value::String(elze.to_string()))))\n");

                        push!(output, "      },\n");
                        push!(output, "      elze => Err(earl::Error::new(earl::ErrorKind::Query).with_fatal().with_extension(earl::extensions::VariableValue(elze.clone())))
\n");
                        push!(output, "    }\n");
                        push!(output, "  }\n");
                        push!(output, "    fn is_assignable_from(typ: &earl::internal::graphql_parser::query::Type<'static,String>) -> bool {
        match typ {
            earl::internal::graphql_parser::query::Type::NonNullType(inner) => {
                match &**inner {
                    earl::internal::graphql_parser::query::Type::NamedType(name) => {
                        name == \"",&en.name,"\"
                    },
                    _ => false
                }
            },
            _ => false
        }
    }

    fn graphql_type() -> earl::internal::graphql_parser::query::Type<'static,String> {
        earl::internal::graphql_parser::query::Type::NonNullType(
            Box::new(earl::internal::graphql_parser::query::Type::NamedType(\"",&en.name,"\".to_string()))
        )
    }");
                        push!(output, "}\n");
                    }
                    schema::TypeDefinition::Object(object) => {
                        let name = &object.name.to_snake();
                        push!(output, "pub mod r#", name, " {\n");
                        push!(output, "  use earl::internal::graphql_parser;\n");
                        push!(output, "  use earl::FutureExt;\n");
                        for field in object.fields.iter() {
                            if !field.arguments.is_empty() {
                                push!(output, "  #[derive(Clone,PartialEq,Debug)]\n");
                                push!(output, "  pub struct ", field.name.to_camel(), "Args {\n");
                                for arg in field.arguments.iter() {
                                    let arg_type = schema.typ(&arg.value_type).map_err(|e| {
                                        Error::UnknownType {
                                            cause: e,
                                            pos: field.position.clone(),
                                        }
                                    })?;
                                    push!(
                                        output,
                                        "    pub ",
                                        &arg.name,
                                        ": ",
                                        arg_type.input(),
                                        ",\n"
                                    );
                                }
                                push!(output, "  }\n");
                            }
                        }
                        push!(output, "  #[derive(Clone,PartialEq,Debug)]\n");
                        push!(output, "  pub enum Field {\n");
                        for field in object.fields.iter() {
                            push!(output, "    ", field.name.to_camel());
                            let typ =
                                schema
                                    .typ(&field.field_type)
                                    .map_err(|e| Error::UnknownType {
                                        cause: e,
                                        pos: field.position.clone(),
                                    })?;
                            if typ.base.has_projection || !field.arguments.is_empty() {
                                push!(output, "{\n");
                                if typ.base.has_projection {
                                    push!(
                                        output,
                                        "      projection: ",
                                        &typ.base.module_name,
                                        "::Projection,\n"
                                    );
                                }
                                if !field.arguments.is_empty() {
                                    push!(output, "      args: ", field.name.to_camel(), "Args,");
                                }
                                push!(output, "    }");
                            }
                            push!(output, ",\n");
                        }
                        for (ityp, _iface) in schema.interfaces(&object.implements_interfaces)? {
                            push!(
                                output,
                                "    For",
                                &ityp.name.to_camel(),
                                "(",
                                &ityp.module_name,
                                "::Field),\n"
                            );
                        }
                        push!(output, "  }\n");
                        for iface in object.implements_interfaces.iter() {
                            let iface_type = &schema.types[iface];
                            push!(
                                output,
                                "  impl<Ctx, T> ",
                                &iface_type.module_name,
                                "::Upcast<Ctx> for T where T: Instance<Ctx>, T: ",
                                &iface_type.module_name,
                                "::Instance<Ctx>, Ctx: 'static + Send + Clone {\n"
                            );
                            push!(
                                output,
                                "     fn upcast<'frame>(&self, caster: ",
                                &iface_type.module_name,
                                "::Caster<'frame, Ctx>)  -> earl::Ack {\n"
                            );
                            push!(output, "       caster.", &object.name, "(self)\n");
                            push!(output, "    }\n");
                            push!(output, "  }\n");
                        }
                        push!(output, "  pub trait Instance<Ctx>");
                        if !object.implements_interfaces.is_empty() {
                            push!(output, " : ");
                        }
                        for (ityp, _iface) in schema.interfaces(&object.implements_interfaces)? {
                            push!(output, &ityp.module_name, "::Instance<Ctx>");
                        }
                        push!(output, " {\n");
                        for field in object.fields.iter() {
                            let typ =
                                schema
                                    .typ(&field.field_type)
                                    .map_err(|e| Error::UnknownType {
                                        cause: e,
                                        pos: field.position.clone(),
                                    })?;
                            push!(output, "    fn r#", &field.name, "(&self,\n");
                            for arg in field.arguments.iter() {
                                let arg_type = schema.typ(&arg.value_type).map_err(|e| {
                                    Error::UnknownType {
                                        cause: e,
                                        pos: field.position.clone(),
                                    }
                                })?;
                                push!(output, "      ", &arg.name, ": ", arg_type.input(), ",\n");
                            }
                            push!(
                                output,
                                "      responder: ",
                                typ.responder_type(),
                                " ) -> earl::Ack;\n"
                            );
                        }
                        push!(output, "  }\n");
                        push!(output, "  impl<Ctx,T> Instance<Ctx> for &T where T: Instance<Ctx> + Sized, Ctx: Clone + 'static");
                        /*for (ityp,_iface) in schema.interfaces(&object.implements_interfaces)? {
                            push!(output, ",\n    T: ",&ityp.module_name,"::Instance<Ctx>\n");
                        }*/
                        push!(output, " {\n");
                        for field in object.fields.iter() {
                            let typ =
                                schema
                                    .typ(&field.field_type)
                                    .map_err(|e| Error::UnknownType {
                                        cause: e,
                                        pos: field.position.clone(),
                                    })?;
                            push!(output, "    fn r#", &field.name, "(&self,\n");
                            for arg in field.arguments.iter() {
                                let arg_type = schema.typ(&arg.value_type).map_err(|e| {
                                    Error::UnknownType {
                                        cause: e,
                                        pos: field.position.clone(),
                                    }
                                })?;
                                push!(output, "      ", &arg.name, ": ", arg_type.input(), ",\n");
                            }
                            push!(
                                output,
                                "      responder: ",
                                typ.responder_type(),
                                " ) -> earl::Ack {\n"
                            );
                            push!(output, "      Instance::r#", &field.name, "(*self,\n");
                            for arg in field.arguments.iter() {
                                push!(output, "      ", &arg.name, ",\n");
                            }
                            push!(output, "      responder)\n");
                            push!(output, "    }\n");
                        }
                        push!(output, "  }\n");
                        push!(output, "  #[derive(PartialEq,Debug,Clone,Default)]\n");
                        push!(output, "  pub struct Projection {\n");
                        push!(output, "    pub fields: Vec<(String, Field)>\n");
                        push!(output, "  }\n");
                        push!(output, "  impl<Ctx: 'static + Send + Clone, T: Instance<Ctx>> ::earl::MakeRoot<Ctx,T> for Projection {\n");
                        push!(
                            output,
                            "    fn make_root(self, ctx: Ctx, t: &T) -> ::earl::internal::Root {\n"
                        );
                        push!(output, "      self.run(ctx,t)\n");
                        push!(output, "    }\n");
                        push!(output, "  }\n");
                        push!(output, "  impl Projection {\n");
                        push!(output, "    pub fn run<Ctx: 'static + Send + Clone, T: Instance<Ctx>>(self,ctx: Ctx, t: &T) -> earl::internal::Root {\n");
                        push!(
                            output,
                            "      let mut buf = earl::internal::ForkBuf::new();\n"
                        );
                        push!(output, "      let responder = Responder{ buf: &mut buf, context: &ctx, projection: self };\n");
                        push!(output, "      let ack = responder.ok(t);\n");
                        push!(
                            output,
                            "      return earl::internal::Root::from(ack, buf)\n"
                        );
                        push!(output, "    }\n");
                        push!(output, "    #[allow(unused)]\n");
                        push!(output, "    pub fn from_fields(flds: &Vec<&graphql_parser::query::Field<'static,String>>, doc: &graphql_parser::query::Document<'static,String>, variables: &earl::Variables) -> Result<Self, earl::Error> {\n");
                        push!(
                            output,
                            "      let mut fields : Vec<(String, Field)> = vec![];\n"
                        );
                        push!(output, "      for field in flds.iter() {\n");
                        push!(
                            output,
                            "           let name = field.alias.as_ref().unwrap_or(&field.name);\n"
                        );
                        // TOOD: check that name is not duplicate
                        push!(output, "           let pro = match &*field.name {\n");
                        for field in object.fields.iter() {
                            push!(
                                output,
                                "           \"",
                                &field.name,
                                "\" => Field::",
                                field.name.to_camel()
                            );
                            let typ =
                                schema
                                    .typ(&field.field_type)
                                    .map_err(|e| Error::UnknownType {
                                        cause: e,
                                        pos: field.position.clone(),
                                    })?;
                            if typ.base.has_projection || !field.arguments.is_empty() {
                                push!(output, "{\n");
                                if !field.arguments.is_empty() {
                                    push!(
                                        output,
                                        "             args: ",
                                        field.name.to_camel(),
                                        "Args{\n"
                                    );
                                    for arg in field.arguments.iter() {
                                        let arg_type =
                                            schema.typ(&arg.value_type).map_err(|e| {
                                                Error::UnknownType {
                                                    cause: e,
                                                    pos: field.position.clone(),
                                                }
                                            })?;
                                        push!(output, "               ", &arg.name, ":");
                                        push!(
                                            output,
                                            "field.arguments.iter().find(|(name,_)| name == \"",
                                            &arg.name,
                                            "\").map(|(_,value)| "
                                        );
                                        push!(output, "earl::FromValue::from_value(value, variables).map_err(|err| err.with_pos(&field.position).or_with_source(|| Box::new(earl::QueryError::IncompatibledArgumentForField{name: \"",&arg.name,"\", field: \"",&field.name,"\", expected_type: \"",arg_type.graphql_type(),"\"})).at_field(name.to_string()) ) )");
                                        if let Some(default) = &arg.default_value {
                                            push!(
                                                output,
                                                ".unwrap_or_else(|| Ok(",
                                                arg_type.print_value(&default).map_err(|e| {
                                                    Error::InvalidValue {
                                                        cause: e,
                                                        pos: field.position.clone(),
                                                    }
                                                })?,
                                                ") )?,\n"
                                            );
                                        } else if arg_type.value_type.is_optional() {
                                            push!(output, ".unwrap_or(Ok(None))?,\n");
                                        } else {
                                            push!(output, ".unwrap_or_else(|| Err(earl::Error::new(earl::ErrorKind::Query).with_fatal().with_pos(&field.position).with_source(Box::new(earl::QueryError::MissingArgumentForField{name:\"",&arg.name,"\", field: \"",&field.name,"\"})).with_extension(earl::extensions::VariableValue(earl::graphql_parser::query::Value::Null)).at_field(\"",&field.name,"\".to_string()).at_field(name.to_string()) ) )?,\n");
                                        }
                                    }
                                    push!(output, "               },\n");
                                }
                                if typ.base.has_projection {
                                    push!(output, "             projection: earl::FromSelectionSet::from_selection_set(&field.selection_set, doc, variables).map_err(|err| err.at_field(name.to_string()) )?\n");
                                }
                                push!(output, "}");
                            }
                            push!(output, ",\n");
                        }
                        for (ityp, iface) in schema.interfaces(&object.implements_interfaces)? {
                            for field in iface.fields.iter() {
                                push!(
                                    output,
                                    "           \"",
                                    &field.name,
                                    "\" => Field::For",
                                    ityp.name.to_camel(),
                                    "(",
                                    &ityp.module_name,
                                    "::Field::",
                                    field.name.to_camel()
                                );
                                let typ = schema.typ(&field.field_type).map_err(|e| {
                                    Error::UnknownType {
                                        cause: e,
                                        pos: field.position.clone(),
                                    }
                                })?;
                                if typ.base.has_projection || !field.arguments.is_empty() {
                                    push!(output, "{\n");
                                    if !field.arguments.is_empty() {
                                        push!(
                                            output,
                                            "             args: ",
                                            &ityp.module_name,
                                            "::",
                                            field.name.to_camel(),
                                            "Args{\n"
                                        );
                                        for arg in field.arguments.iter() {
                                            let arg_type =
                                                schema.typ(&arg.value_type).map_err(|e| {
                                                    Error::UnknownType {
                                                        cause: e,
                                                        pos: field.position.clone(),
                                                    }
                                                })?;
                                            push!(output, "               r#", &arg.name, ":");
                                            push!(
                                                output,
                                                "field.arguments.iter().find(|(name,_)| name == \"",
                                                &arg.name,
                                                "\").map(|(_,value)| "
                                            );
                                            push!(output, "earl::FromValue::from_value(value, variables).map_err(|err| err.with_pos(&field.position).or_with_source(|| Box::new(earl::QueryError::IncompatibledArgumentForField{name: \"",&arg.name,"\", field: \"",&field.name,"\", expected_type: \"",arg_type.graphql_type(),"\"})).at_field(\"",&field.name,"\".to_string()) ) )");
                                            if let Some(default) = &arg.default_value {
                                                push!(
                                                    output,
                                                    ".unwrap_or_else(|| Ok(",
                                                    arg_type.print_value(&default).map_err(
                                                        |e| Error::InvalidValue {
                                                            cause: e,
                                                            pos: field.position.clone()
                                                        }
                                                    )?,
                                                    ") )?,\n"
                                                );
                                            } else if arg_type.value_type.is_optional() {
                                                push!(output, ".unwrap_or(Ok(None))?,\n");
                                            } else {
                                                push!(output, ".unwrap_or_else(|| Err(earl::Error::new(earl::ErrorKind::Query).with_fatal().with_pos(&field.position).with_source(Box::new(earl::QueryError::MissingArgumentForField{name:\"",&arg.name,"\", field: \"",&field.name,"\"})).with_extension(earl::extensions::VariableValue(earl::graphql_parser::query::Value::Null)).at_field(\"",&field.name,"\".to_string()) ) )?,\n");
                                            }
                                        }
                                        push!(output, "               },\n");
                                    }
                                    if typ.base.has_projection {
                                        push!(output, "             projection: ", &typ.base.module_name ,"::Projection::from_selection_set(&field.selection_set, doc, variables).map_err(|err| err.at_field(\"",&field.name,"\".to_string()) )?\n");
                                    }
                                    push!(output, "}");
                                }
                                push!(output, "),\n");
                            }
                        }
                        push!(output, "             f => return Err(earl::Error::new(earl::ErrorKind::Query).with_fatal().with_source(Box::new(earl::QueryError::UnknownField{name: f.to_string(), on_type: \"",&object.name,"\" })).with_pos(&field.position))\n");
                        push!(output, "           };\n");
                        push!(output, "           fields.push((name.clone(), pro));\n");
                        push!(output, "      }\n");
                        push!(output, "      return Ok(Self{ fields })\n");
                        push!(output, "    }\n");
                        push!(output, "  }\n");
                        push!(output, "  impl earl::FromSelectionSet for Projection {\n");
                        push!(output, "    fn from_selection_set(selection : &graphql_parser::query::SelectionSet<'static,String>, doc: &graphql_parser::query::Document<'static,String>, variables: &earl::Variables) -> Result<Self, earl::Error> {\n");
                        push!(output, "      Projection::from_fields(&earl::normalize_object(selection,doc,super::__Symbol::",&object.name,")?, doc, variables)\n");
                        push!(output, "    }\n");
                        push!(output, "  }\n");
                        push!(output, "  pub struct Responder<'frame,Ctx> {\n");
                        push!(
                            output,
                            "    pub buf: &'frame mut ::earl::internal::ForkBuf,\n"
                        );
                        push!(output, "    pub context: &'frame Ctx,\n");
                        push!(output, "    pub projection: Projection\n");
                        push!(output, "  }\n");
                        push!(output, "  pub struct OptionalResponder<'frame,Ctx> {\n");
                        push!(
                            output,
                            "    pub buf: &'frame mut ::earl::internal::ForkBuf,\n"
                        );
                        push!(output, "    pub context: &'frame Ctx,\n");
                        push!(output, "    pub projection: Projection\n");
                        push!(output, "  }\n");
                        push!(output, "  pub struct ListResponder<'frame,Ctx> {\n");
                        push!(
                            output,
                            "    pub buf: &'frame mut ::earl::internal::ForkBuf,\n"
                        );
                        push!(output, "    pub context: &'frame Ctx,\n");
                        push!(output, "    pub projection: Projection\n");
                        push!(output, "  }\n");
                        push!(output, "  pub struct OptionalListResponder<'frame,Ctx> {\n");
                        push!(
                            output,
                            "    pub buf: &'frame mut ::earl::internal::ForkBuf,\n"
                        );
                        push!(output, "    pub context: &'frame Ctx,\n");
                        push!(output, "    pub projection: Projection\n");
                        push!(output, "  }\n");
                        push!(
                            output,
                            "  impl<'frame,Ctx: 'static + Send + Clone> Responder<'frame,Ctx> {\n"
                        );
                        push!(
                            output,
                            "    pub fn ok<T: Instance<Ctx>>(self, value: &T) -> earl::Ack {\n"
                        );
                        push!(
                            output,
                            "      let Responder{ mut buf, context, projection } = self;\n"
                        );
                        push!(output, "      let mut ack = earl::ack::new();\n");
                        push!(output, "      buf.start_object();\n");
                        push!(output, "      for (name, field) in projection.fields {\n");
                        push!(output, "        buf.field(&name);\n");
                        push!(output, "        let ans = Self::answer(&mut buf, context, value, field.clone());\n");
                        push!(
                            output,
                            "        if earl::ack::is_fatal(&ans) {
                            return ans;
                        } else if earl::ack::is_err(&ans) {
                            buf.ensure_field_is_closed();
                            buf.add_error(ans.unwrap_err());
                        } else {\n"
                        );
                        push!(output, "        earl::ack::merge(&mut ack, ans);}\n");
                        push!(output, "      }\n");
                        push!(output, "      buf.end_object();\n");
                        push!(output, "      return ack\n");
                        push!(output, "    }\n");
                        push!(output, "    pub fn lazy<T: Instance<Ctx> + 'static,F: ::std::future::Future<Output=Result<T,earl::Error>> + Send + 'static>(self, f: F) -> earl::Ack {\n");
                        push!(
                            output,
                            "      let Responder{ buf, context, projection } = self;\n"
                        );
                        push!(output, "      let mut t = buf.fork();\n");

                        push!(output, "      let context = Clone::clone(context);\n");
                        push!(output, "      earl::ack::of(f.map(move |result|{\n");
                        push!(output, "        match result {\n");
                        push!(output, "        Ok(value) => {\n");
                        push!(output, "          let responder = Responder{ buf: &mut t, context: &context, projection };\n");
                        push!(output, "          let r = responder.ok(&value);\n");
                        push!(output, "          earl::ack::into_inner(r, &mut t)\n");
                        push!(output, "        },\n");
                        push!(output, "        Err(err) => {\n");
                        push!(output, "          t.add_error(err);\n");
                        push!(output, "          earl::ack::new_inner()\n");
                        push!(output, "        }\n");
                        push!(output, "        }\n");
                        push!(output, "      }).boxed())\n");
                        push!(output, "    }\n");
                        push!(output, "    pub(super) fn answer<T: Instance<Ctx>>(buf: &mut ::earl::internal::ForkBuf, context: &Ctx, value: &T, field: Field) -> earl::Ack {\n");
                        push!(output, "      match field {\n");
                        for field in object.fields.iter() {
                            push!(output, "        Field::r#", field.name.to_camel());
                            let typ =
                                schema
                                    .typ(&field.field_type)
                                    .map_err(|e| Error::UnknownType {
                                        cause: e,
                                        pos: field.position.clone(),
                                    })?;
                            if typ.base.has_projection || !field.arguments.is_empty() {
                                push!(output, "{");
                                if typ.base.has_projection {
                                    push!(output, "projection,");
                                }
                                if !field.arguments.is_empty() {
                                    push!(output, "args");
                                }
                                push!(output, "}");
                            }
                            push!(output, " => {\n");

                            push!(output, "          let responder = ", typ.responder(), "{\n");
                            push!(output, "            buf: buf,\n");
                            push!(output, "            context: context,\n");
                            if typ.base.has_projection {
                                push!(output, "            projection: projection,\n");
                            }
                            push!(output, "          };\n");
                            push!(output, "          Instance::r#", &field.name, "( value,\n");
                            for arg in field.arguments.iter() {
                                push!(output, "            args.", &arg.name, ",\n");
                            }
                            push!(output, "            responder )\n");
                            if !typ.is_optional() {
                                push!(output, "            .map_err(|e| e.with_fatal() )\n");
                            }
                            push!(output, "        }\n");
                        }
                        for (ityp, _iface) in schema.interfaces(&object.implements_interfaces)? {
                            push!(
                                output,
                                "    Field::For",
                                ityp.name.to_camel(),
                                "(sub) => {\n"
                            );
                            push!(
                                output,
                                "        ",
                                &ityp.module_name,
                                "::Responder::answer(buf, context, value, sub)\n"
                            );
                            push!(output, "    }\n");
                        }
                        push!(output, "      }\n");
                        push!(output, "    }\n");
                        push!(output, "    pub fn context(&self) -> &Ctx {\n");
                        push!(output, "      self.context\n");
                        push!(output, "    }\n");
                        push!(output, "  }\n");
                        push!(output, "  impl<'frame,Ctx: 'static + Send + Clone> ListResponder<'frame,Ctx> {\n");
                        push!(
                            output,
                            "    pub fn ok<T: Instance<Ctx>>(self, value: &[T]) -> earl::Ack {\n"
                        );
                        push!(
                            output,
                            "      let ListResponder{ buf, context, projection } = self;\n"
                        );
                        push!(output, "      let mut collected = earl::ack::new();\n");
                        push!(output, "      let mut first = true;\n");
                        push!(output, "      buf.start_array();\n");
                        push!(output, "      for elem in value.iter() {\n");
                        push!(output, "        if !first {\n");
                        push!(output, "          buf.next_element();\n");
                        push!(output, "        } else { first = false; }\n");
                        push!(output, "        let responder = Responder{ buf, context, projection: projection.clone() };\n");
                        push!(
                            output,
                            "        earl::ack::merge(&mut collected, responder.ok(elem));\n"
                        );
                        push!(output, "      }\n");
                        push!(output, "      buf.end_array();\n");
                        push!(output, "      collected\n");
                        push!(output, "    }\n");
                        push!(output, "    pub fn empty(self) -> earl::Ack {\n");
                        push!(output, "      let ListResponder{ buf,.. } = self;\n");
                        push!(output, "      buf.put(\"[]\".as_bytes());\n");
                        push!(output, "      earl::ack::new()\n");
                        push!(output, "    }\n");
                        push!(output, "    pub fn lazy<T: Instance<Ctx> + 'static,F: ::std::future::Future<Output=Result<Vec<T>,earl::Error>> + Send + 'static>(self, f: F) -> earl::Ack {\n");
                        push!(
                            output,
                            "      let ListResponder{ buf, context, projection } = self;\n"
                        );
                        push!(output, "      let mut t = buf.fork();\n");
                        push!(output, "      let context = Clone::clone(context);\n");
                        push!(output, "      earl::ack::of(f.map(move |result|{\n");
                        push!(output, "        match result {\n");
                        push!(output, "        Ok(value) => {\n");
                        push!(output, "          let responder = ListResponder{ buf: &mut t, context: &context, projection };\n");
                        push!(output, "          let r = responder.ok(&*value);\n");
                        push!(output, "          earl::ack::into_inner(r, &mut t)\n");
                        push!(output, "        },\n");
                        push!(output, "        Err(err) => {\n");
                        push!(output, "          t.add_error(err.with_fatal());\n");
                        push!(output, "          earl::ack::new_inner()\n");
                        push!(output, "        }\n");
                        push!(output, "        }\n");
                        push!(output, "      }).boxed())\n");
                        push!(output, "    }\n");
                        push!(output, "    pub fn context(&self) -> &Ctx {\n");
                        push!(output, "      self.context\n");
                        push!(output, "    }\n");
                        push!(output, "  }\n");
                        push!(output, "  impl<'frame,Ctx: 'static + Send + Clone> OptionalListResponder<'frame,Ctx> {\n");
                        push!(output, "    pub fn ok<T: Instance<Ctx>>(self, value: Option<&[T]>) -> earl::Ack {\n");
                        push!(
                            output,
                            "      let OptionalListResponder{ buf, context, projection } = self;\n"
                        );
                        push!(output, "      if let Some(inner) = value {\n");
                        push!(output, "        let responder = ListResponder{ buf, context, projection: projection };\n");
                        push!(output, "        responder.ok(inner)\n");
                        push!(output, "       } else {\n");
                        push!(output, "        buf.put(\"null\".as_bytes());\n");
                        push!(output, "        earl::ack::new()\n");
                        push!(output, "      }\n");
                        push!(output, "    }\n");
                        push!(
                            output,
                            "    pub fn some<T: Instance<Ctx>>(self, value: &[T]) -> earl::Ack {\n"
                        );
                        push!(output, "      self.ok(Some(value))\n");
                        push!(output, "    }\n");
                        push!(output, "    pub fn none(self) -> earl::Ack {\n");
                        push!(
                            output,
                            "      let OptionalListResponder{ buf, ..} = self;\n"
                        );
                        push!(output, "      buf.put(\"null\".as_bytes());\n");
                        push!(output, "      earl::ack::new()\n");
                        push!(output, "    }\n");
                        push!(output, "    pub fn empty(self) -> earl::Ack {\n");
                        push!(
                            output,
                            "      let OptionalListResponder{ buf, ..} = self;\n"
                        );
                        push!(output, "      buf.put(\"[]\".as_bytes());\n");
                        push!(output, "      earl::ack::new()\n");
                        push!(output, "    }\n");
                        push!(output, "    pub fn context(&self) -> &Ctx {\n");
                        push!(output, "      self.context\n");
                        push!(output, "    }\n");
                        push!(output, "  }\n");
                        push!(output, "  impl<'frame,Ctx: 'static + Send + Clone> OptionalResponder<'frame,Ctx> {\n");
                        push!(output, "    pub fn ok<T: Instance<Ctx>>(self, value: Option<&T>) -> earl::Ack {\n");
                        push!(
                            output,
                            "      let OptionalResponder{ buf, context, projection } = self;\n"
                        );
                        push!(output, "      if let Some(inner) = value {\n");
                        push!(
                            output,
                            "        let responder = Responder{ buf, context, projection };\n"
                        );
                        push!(output, "        responder.ok(inner)\n");
                        push!(output, "       } else {\n");
                        push!(output, "        buf.put(\"null\".as_bytes());\n");
                        push!(output, "        earl::ack::new()\n");
                        push!(output, "      }\n");
                        push!(output, "    }\n");
                        push!(
                            output,
                            "    pub fn some<T: Instance<Ctx>>(self, value: &T) -> earl::Ack {\n"
                        );
                        push!(
                            output,
                            "      let OptionalResponder{ buf, context, projection } = self;\n"
                        );
                        push!(
                            output,
                            "      let responder = Responder{ buf, context, projection };\n"
                        );
                        push!(output, "      responder.ok(value)\n");
                        push!(output, "    }\n");
                        push!(output, "    pub fn none(self) -> earl::Ack {\n");
                        push!(output, "      let OptionalResponder{ buf, .. } = self;\n");
                        push!(output, "      buf.put(\"null\".as_bytes());\n");
                        push!(output, "      earl::ack::new()\n");
                        push!(output, "    }\n");
                        push!(
                            output,
                            "    pub fn err<T: Into<earl::Error>>(self, err: T) -> earl::Ack {\n"
                        );
                        push!(output, "      earl::Ack::Err(err.into())\n");
                        push!(output, "    }\n");
                        push!(output, "    pub fn lazy<T: Instance<Ctx> + 'static,F: ::std::future::Future<Output=Result<Option<T>,earl::Error>> + Send + 'static>(self, f: F) -> earl::Ack {\n");
                        push!(
                            output,
                            "      let OptionalResponder{ buf, context, projection } = self;\n"
                        );
                        push!(output, "      let mut t = buf.fork();\n");
                        push!(output, "      let context = Clone::clone(context);\n");
                        push!(output, "      earl::ack::of(f.map(move |result|{\n");
                        push!(output, "        match result {\n");
                        push!(output, "        Ok(value) => {\n");
                        push!(output, "          let responder = OptionalResponder{ buf: &mut t, context: &context, projection };\n");
                        push!(output, "          let r = responder.ok(value.as_ref());\n");
                        push!(output, "          earl::ack::into_inner(r, &mut t)\n");
                        push!(output, "        },\n");
                        push!(output, "        Err(err) => {\n");
                        push!(output, "          t.ensure_field_is_closed();\n");
                        push!(output, "          t.add_error(err);\n");
                        push!(output, "          earl::ack::new_inner()\n");
                        push!(output, "        }\n");
                        push!(output, "        }\n");
                        push!(output, "      }).boxed())\n");
                        push!(output, "    }\n");
                        push!(output, "    pub fn context(&self) -> &Ctx {\n");
                        push!(output, "      self.context\n");
                        push!(output, "    }\n");
                        push!(output, "  }\n");
                        push!(output, "}\n");
                    }
                    schema::TypeDefinition::Interface(object) => {
                        let name = &object.name.to_snake();
                        push!(output, "pub mod r#", name, " {\n");
                        push!(output, "  use earl::internal::graphql_parser;\n");
                        push!(output, "  use earl::FutureExt;\n");
                        for field in object.fields.iter() {
                            if !field.arguments.is_empty() {
                                push!(output, "  #[derive(Clone,PartialEq,Debug)]\n");
                                push!(output, "  pub struct ", field.name.to_camel(), "Args {\n");
                                for arg in field.arguments.iter() {
                                    let arg_type = schema.typ(&arg.value_type).map_err(|e| {
                                        Error::UnknownType {
                                            cause: e,
                                            pos: field.position.clone(),
                                        }
                                    })?;
                                    push!(
                                        output,
                                        "    pub ",
                                        &arg.name,
                                        ": ",
                                        arg_type.input(),
                                        ",\n"
                                    );
                                }
                                push!(output, "  }\n");
                            }
                        }
                        push!(output, "  #[derive(Clone,PartialEq,Debug)]\n");
                        push!(output, "  pub enum Field {\n");
                        for field in object.fields.iter() {
                            push!(output, "    ", field.name.to_camel());
                            let typ =
                                schema
                                    .typ(&field.field_type)
                                    .map_err(|e| Error::UnknownType {
                                        cause: e,
                                        pos: field.position.clone(),
                                    })?;
                            if typ.base.has_projection || !field.arguments.is_empty() {
                                push!(output, "{\n");
                                if typ.base.has_projection {
                                    push!(
                                        output,
                                        "      projection: ",
                                        &typ.base.module_name,
                                        "::Projection,\n"
                                    );
                                }
                                if !field.arguments.is_empty() {
                                    push!(output, "      args: ", field.name.to_camel(), "Args,");
                                }
                                push!(output, "    }");
                            }
                            push!(output, ",\n");
                        }
                        push!(output, "  }\n");
                        push!(output, "  pub trait Instance<Ctx> {\n");
                        for field in object.fields.iter() {
                            let typ =
                                schema
                                    .typ(&field.field_type)
                                    .map_err(|e| Error::UnknownType {
                                        cause: e,
                                        pos: field.position.clone(),
                                    })?;
                            push!(output, "    fn r#", &field.name, "(&self,\n");
                            for arg in field.arguments.iter() {
                                let arg_type = schema.typ(&arg.value_type).map_err(|e| {
                                    Error::UnknownType {
                                        cause: e,
                                        pos: field.position.clone(),
                                    }
                                })?;
                                push!(output, "      ", &arg.name, ": ", arg_type.input(), ",\n");
                            }
                            push!(
                                output,
                                "      responder: ",
                                typ.responder_type(),
                                " ) -> earl::Ack;\n"
                            );
                        }
                        push!(output, "  }\n");
                        push!(output, "  impl<Ctx,T> Instance<Ctx> for &T where T: Instance<Ctx>, Ctx: Clone + 'static {\n");
                        for field in object.fields.iter() {
                            let typ =
                                schema
                                    .typ(&field.field_type)
                                    .map_err(|e| Error::UnknownType {
                                        cause: e,
                                        pos: field.position.clone(),
                                    })?;
                            push!(output, "    fn r#", &field.name, "(&self,\n");
                            for arg in field.arguments.iter() {
                                let arg_type = schema.typ(&arg.value_type).map_err(|e| {
                                    Error::UnknownType {
                                        cause: e,
                                        pos: field.position.clone(),
                                    }
                                })?;
                                push!(output, "      ", &arg.name, ": ", arg_type.input(), ",\n");
                            }
                            push!(
                                output,
                                "      responder: ",
                                typ.responder_type(),
                                " ) -> earl::Ack {\n"
                            );
                            push!(output, "      Instance::r#", &field.name, "(*self,\n");
                            for arg in field.arguments.iter() {
                                push!(output, "      ", &arg.name, ",\n");
                            }
                            push!(output, "     responder)\n");
                            push!(output, "    }\n");
                        }
                        push!(output, "  }\n");
                        push!(output, "  pub trait Upcast<Ctx> {\n");
                        push!(output, "    fn upcast<'frame>(&self, caster: Caster<'frame,Ctx>) -> earl::Ack;\n");
                        push!(output, "  }\n");
                        /*push!(output, "  impl<T,Ctx> Upcast<Ctx> for &T where T: Upcast<Ctx> {\n");
                        push!(output, "    fn upcast<'frame>(&self, caster: Caster<'frame,Ctx>) -> earl::Ack {\n");
                        push!(output, "      Upcast::upcast(*self, caster)\n");
                        push!(output, "    }\n");
                        push!(output, "  }\n");*/
                        push!(output, "  pub struct Caster<'frame, Ctx> { projection: Projection, buf: &'frame mut earl::internal::ForkBuf, context: &'frame Ctx }\n");
                        push!(
                            output,
                            "  impl<'frame, Ctx: 'static + Send + Clone> Caster<'frame, Ctx> {\n"
                        );
                        for implementation in schema.implementations(&*object.name)?.iter() {
                            push!(
                                output,
                                "    pub fn ",
                                &implementation.name,
                                "<T: ",
                                &implementation.module_name,
                                "::Instance<Ctx>>(self, value: &T) -> ::earl::Ack {\n"
                            );
                            push!(
                                output,
                                "      let Self{ projection, mut buf, context } = self;\n"
                            );
                            push!(
                                output,
                                "      let rsp = ",
                                &implementation.module_name,
                                "::Responder{ projection: projection.",
                                &implementation.name,
                                "_fields, buf: &mut buf, context };\n"
                            );
                            push!(output, "      return rsp.ok(value)\n");
                            push!(output, "    }\n");
                        }
                        push!(output, "  }\n");
                        push!(output, "  #[derive(PartialEq,Debug,Clone,Default)]\n");
                        push!(output, "  pub struct Projection {\n");
                        for implementation in schema.implementations(&*object.name)?.iter() {
                            push!(
                                output,
                                "    pub ",
                                &implementation.name,
                                "_fields: ",
                                &implementation.module_name,
                                "::Projection,\n"
                            );
                        }
                        push!(output, "  }\n");
                        push!(output, "  impl Projection {\n");
                        push!(output, "    pub fn run<Ctx: 'static + Send + Clone, T: Upcast<Ctx>>(self,ctx: Ctx, t: &T) -> earl::internal::Root {\n");
                        push!(
                            output,
                            "      let mut buf = earl::internal::ForkBuf::new();\n"
                        );
                        push!(output, "      let responder = Responder{ buf: &mut buf, context: &ctx, projection: self };\n");
                        push!(output, "      let ack = responder.ok(t);\n");
                        push!(
                            output,
                            "      return earl::internal::Root::from(ack, buf)\n"
                        );
                        push!(output, "    }\n");
                        push!(output, "  }\n");
                        push!(output, "  impl earl::FromSelectionSet for Projection {\n");
                        push!(output, "    fn from_selection_set(selection : &graphql_parser::query::SelectionSet<'static,String>, doc: &graphql_parser::query::Document<'static,String>, variables: &earl::Variables) -> Result<Self, earl::Error> {\n");

                        push!(output, "      let fields = earl::normalize_interface(selection,doc,super::__Symbol::",&object.name,")?;\n");
                        push!(output, "      return Ok(Projection{\n");
                        for implementation in schema.implementations(&*object.name)?.iter() {
                            push!(
                                output,
                                "    ",
                                &implementation.name,
                                "_fields: ",
                                &implementation.module_name,
                                "::Projection::from_fields(&fields[&super::__Symbol::",
                                &implementation.name,
                                "], doc, variables)?,\n"
                            );
                        }
                        push!(output, "      })\n");
                        push!(output, "    }\n");
                        push!(output, "  }\n");
                        push!(output, "  pub struct Responder<'frame,Ctx> {\n");
                        push!(
                            output,
                            "    pub buf: &'frame mut ::earl::internal::ForkBuf,\n"
                        );
                        push!(output, "    pub context: &'frame Ctx,\n");
                        push!(output, "    pub projection: Projection\n");
                        push!(output, "  }\n");
                        push!(output, "  pub struct ListResponder<'frame,Ctx> {\n");
                        push!(
                            output,
                            "    pub buf: &'frame mut ::earl::internal::ForkBuf,\n"
                        );
                        push!(output, "    pub context: &'frame Ctx,\n");
                        push!(output, "    pub projection: Projection\n");
                        push!(output, "  }\n");
                        push!(
                            output,
                            "  impl<'frame,Ctx: 'static + Send + Clone> Responder<'frame,Ctx> {\n"
                        );
                        push!(
                            output,
                            "    pub fn ok<T: Upcast<Ctx>>(self, value: &T) -> earl::Ack {\n"
                        );
                        push!(
                            output,
                            "      let Responder{ buf, context, projection } = self;\n"
                        );
                        push!(
                            output,
                            "      let caster = Caster{ buf, context, projection };\n"
                        );
                        push!(output, "      return Upcast::upcast(value, caster);\n");
                        push!(output, "    }\n");
                        /*push!(output, "    pub fn lazy<T: Upcast<Ctx> + 'static,F: ::std::future::Future<Output=Result<T,earl::Error>> + Send + 'static>(self, f: F) -> earl::Ack {\n");
                        push!(output, "      let Responder{ buf, context, projection } = self;\n");
                        push!(output, "      let mut t = buf.fork();\n");
                        push!(output, "      let context = Clone::clone(context);\n");
                        push!(output, "      earl::ack::of(f.map_ok(move |value|{\n");
                        push!(output, "        let responder = Responder{ buf: &mut t, context: &context, projection };\n");
                        push!(output, "        earl::ack::into_inner(responder.ok(&value))\n");
                        push!(output, "      }).boxed())\n");
                        push!(output, "    }\n");*/
                        push!(output, "    pub(super) fn answer<T: Instance<Ctx>>(buf: &mut ::earl::internal::ForkBuf, context: &Ctx, value: &T, field: Field) -> earl::Ack {\n");
                        push!(output, "      match field {\n");
                        for field in object.fields.iter() {
                            push!(output, "        Field::r#", field.name.to_camel());
                            let typ =
                                schema
                                    .typ(&field.field_type)
                                    .map_err(|e| Error::UnknownType {
                                        cause: e,
                                        pos: field.position.clone(),
                                    })?;
                            if typ.base.has_projection || !field.arguments.is_empty() {
                                push!(output, "{");
                                if typ.base.has_projection {
                                    push!(output, "projection,");
                                }
                                if !field.arguments.is_empty() {
                                    push!(output, "args");
                                }
                                push!(output, "}");
                            }
                            push!(output, " => {\n");

                            push!(output, "          let responder = ", typ.responder(), "{\n");
                            push!(output, "            buf: buf,\n");
                            push!(output, "            context: context,\n");
                            if typ.base.has_projection {
                                push!(output, "            projection: projection,\n");
                            }
                            push!(output, "          };\n");
                            push!(output, "          Instance::r#", &field.name, "( value,\n");
                            for arg in field.arguments.iter() {
                                push!(output, "            args.", &arg.name, ",\n");
                            }
                            push!(output, "            responder )\n");
                            push!(output, "        }\n");
                        }
                        push!(output, "      }\n");
                        push!(output, "    }\n");
                        push!(output, "  }\n");
                        push!(output, "  impl<'frame,Ctx: 'static + Send + Clone> ListResponder<'frame,Ctx> {\n");
                        push!(
                            output,
                            "    pub fn ok<T: Upcast<Ctx>>(self, value: &[T]) -> earl::Ack {\n"
                        );
                        push!(
                            output,
                            "      let ListResponder{ buf, context, projection } = self;\n"
                        );
                        push!(output, "      let mut collected = earl::ack::new();\n");
                        push!(output, "      let mut first = true;\n");
                        push!(output, "      buf.start_array();\n");
                        push!(output, "      for elem in value.iter() {\n");
                        push!(output, "        if !first {\n");
                        push!(output, "          buf.next_element();\n");
                        push!(output, "        } else { first = false; }\n");
                        push!(output, "        let responder = Responder{ buf, context, projection: projection.clone() };\n");
                        push!(
                            output,
                            "        earl::ack::merge( &mut collected, responder.ok(elem));\n"
                        );
                        push!(output, "      }\n");
                        push!(output, "      buf.end_array();\n");
                        push!(output, "      collected\n");
                        push!(output, "    }\n");
                        /*push!(output, "    pub fn lazy<T: Upcast<Ctx> + 'static,F: ::std::future::Future<Output=Result<Vec<T>,earl::Error>> + Send + 'static>(self, f: F) -> earl::Ack {\n");
                        push!(output, "      let ListResponder{ buf, context, projection } = self;\n");
                        push!(output, "      let mut t = buf.fork();\n");
                        push!(output, "      let context = Clone::clone(context);\n");
                        push!(output, "      earl::ack::of(f.map_ok(move |value|{\n");
                        push!(output, "        let responder = ListResponder{ buf: &mut t, context: &context, projection };\n");
                        push!(output, "        earl::ack::into_inner(responder.ok(&*value))\n");
                        push!(output, "      }).boxed())\n");
                        push!(output, "    }\n");*/
                        push!(output, "  }\n");
                        push!(output, "}\n");
                    }
                    schema::TypeDefinition::Union(_un) => {}
                }
            }
            schema::Definition::SchemaDefinition(sch) => {
                push!(output, "pub struct Schema {}\n");
                push!(output, "impl earl::Schema for Schema {\n");
                if let Some(t) = sch.query.as_ref() {
                    push!(output, "  type Query = ", t.to_snake(), "::Projection;\n");
                } else {
                    push!(output, "  type Query = earl::NotImplemented;\n");
                }
                if let Some(t) = sch.mutation.as_ref() {
                    push!(
                        output,
                        "  type Mutation = ",
                        t.to_snake(),
                        "::Projection;\n"
                    );
                } else {
                    push!(output, "  type Mutation = earl::NotImplemented;\n");
                }
                push!(output, "  type Subscription = earl::NotImplemented;\n");
                push!(
                    output,
                    "  fn source() -> &'static str { ",
                    format!["{:?}", input],
                    " }\n"
                );
                push!(output, "  fn input_type(typ: &earl::internal::graphql_parser::query::Type<'static,String>) -> Option<&'static dyn Fn(&earl::internal::graphql_parser::query::Value<'static,String>) -> Result<(),earl::Error>> {\n");
                push!(output, "    match typ {\n");
                push!(output, "      earl::internal::graphql_parser::query::Type::NamedType(name) => match &**name {\n");
                for i in schema.types.iter().map(|(_, t)| t).filter(|t| t.is_input) {
                    push!(
                        output,
                        "        \"",
                        &i.name,
                        "\" => Some(&<Option<",
                        i.rust_type.trim_start_matches("super::"),
                        "> as earl::FromValue>::accepts),\n"
                    );
                }
                push!(output, "        _ => None\n");
                push!(output, "        },\n");
                push!(output, "      earl::internal::graphql_parser::query::Type::NonNullType(inner) => match &**inner {\n");
                push!(output, "        earl::internal::graphql_parser::query::Type::NamedType(name) => match &**name {\n");
                for i in schema.types.iter().map(|(_, t)| t).filter(|t| t.is_input) {
                    push!(
                        output,
                        "          \"",
                        &i.name,
                        "\" => Some(&<",
                        i.rust_type.trim_start_matches("super::"),
                        " as earl::FromValue>::accepts),\n"
                    );
                }
                push!(output, "          _ => None\n");
                push!(output, "          },\n");
                push!(output, "        earl::internal::graphql_parser::query::Type::ListType(list) => match &**list {\n");
                push!(output, "          earl::internal::graphql_parser::query::Type::NamedType(name) => match &**name {\n");
                for i in schema.types.iter().map(|(_, t)| t).filter(|t| t.is_input) {
                    push!(
                        output,
                        "            \"",
                        &i.name,
                        "\" => Some(&<Vec<Option<",
                        i.rust_type.trim_start_matches("super::"),
                        ">> as earl::FromValue>::accepts),\n"
                    );
                }
                push!(output, "            _ => None\n");
                push!(output, "            },\n");
                push!(output, "          earl::internal::graphql_parser::query::Type::NonNullType(inner) => match &**inner {\n");
                push!(output, "            earl::internal::graphql_parser::query::Type::NamedType(name) => match &**name {\n");
                for i in schema.types.iter().map(|(_, t)| t).filter(|t| t.is_input) {
                    push!(
                        output,
                        "              \"",
                        &i.name,
                        "\" => Some(&<Vec<",
                        i.rust_type.trim_start_matches("super::"),
                        "> as earl::FromValue>::accepts),\n"
                    );
                }
                push!(output, "              _ => None\n");
                push!(output, "              },\n");
                push!(output, "            _ => None\n");
                push!(output, "            },\n");

                push!(output, "          _ => None\n");
                push!(output, "          },\n");
                push!(output, "        _ => None\n");
                push!(output, "        },\n");
                push!(output, "      _ => None\n");
                push!(output, "    }\n");
                push!(output, "  }\n");
                push!(output, "}\n");
            }
            _ => unimplemented![],
        };
    }
    return Ok(String::from_utf8(output).expect("Invalid encoding"));
}
