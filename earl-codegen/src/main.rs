use std::io::{self, Read, Write};

fn main() -> io::Result<()> {
    let mut buffer = String::new();
    io::stdin().read_to_string(&mut buffer)?;
    io::stdout().write_all(
        earl_codegen::generate(&*buffer)
            .map_err(|e| io::Error::new(io::ErrorKind::Other, Box::new(e)))?
            .as_bytes()
            )?;
    Ok(())
}
